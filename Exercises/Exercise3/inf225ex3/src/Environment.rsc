module Environment
import Simple;

import ParseTree;
import List;
import IO;

@doc{Prints the environment. Useful for debugging}
public void printenv(Env env) {
	for(i <- index(env)) {
		Scope scope = env[i];
		println ("<i>:[");
		for(k <- scope) {
			if(Fun(argName, expr, fenv) := scope[k]) {
				println("    <k>: Fun(<unparse(argName)>, <unparse(expr)>)");
			} else {
				println("    <k>: <scope[k]>");
			}
		}
		println ("  ]");
	}
}

@doc{The type of environments; maps from strings to values}
alias Scope = map[str, int];

@doc{The Environment. This is a stack of scopes}
alias Env = list[Scope];

public Env insertInto(Env source, Env target) {
	for(i <- index(source)) {
		if(i in index(target)) {
			target[i] = source[i]; 
		} else {
			return target;
		}
	}
	return target;
}

@doc{A lookup function. Will look up a variable starting from the innermost 
scope and work outwards}
public int lookup(ID name, Env env) {
	for(scope <- reverse(env)) {
		if(unparse(name) in scope) {
			return scope[unparse(name)];
		}
	}
	throw "Undefined variable \'<name>\'";
}

@doc{A function which will declare a variable in the innermost scope}
public Env declare(ID name, int pos, Env env) {
	int index = size(env) - 1;
	scope = env[index];
	if(unparse(name) in scope) {
		throw "Variable \'<unparse(name)>\' already declared";
	}
	scope[unparse(name)] = pos;
	env[index] = scope;
	return env;
}

@doc{Will assign to an existing variable. If it does not exists it will be
declared}
public Env assign(ID name, int pos, Env env) {
	for(scopeIndex <- reverse(index(env))) {
		scope = env[scopeIndex];
		if(unparse(name) in scope) {
			scope[unparse(name)] = pos;
			env[scopeIndex] = scope;
			return env;
		}
	}
	return declare(name, pos, env);
}

@doc{Returns a new scope at the start of the environment}
public Env enterScope(Env env) {
	env = env + [()];
	return env;
}

@doc{Will discard the innermost scope}
public Env exitScope(Env env) {
	return env[0..-1];
}

@doc{Will create a new completely blank environment}
public Env newEnv() {
	return [()];
}