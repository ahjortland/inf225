module Main
import Simple;
import Evaluator;
import Environment;
import Value;
import Store;

import List;
import IO;
import ParseTree;

public str toString(Value val) {
	if(Fun(args, expr, env) := val) {
		return "Fun(<unparse(args)>, <unparse(expr)>)";
	} else {
		return "<val>";
	}
}

public str toString(Store store) {
	str string = "";
	for(index <- index(store)) {
		string = "<string><index>: <toString(store[index])>\n";
	}
	return string;
}


public str toString(Env env, store) {
	str string = "";
	for(i <- index(env)) {
		scope = env[i];
		string += "[\n";
		for(k <- scope) {
			string += "  <k>:  <scope[k]>: <toString(getValue(scope[k], store))>\n";
		}
		string += "]\n";
	}
	return string;
}

public void run(Program p) {
	<val, env, store> = eval(p);
	println("\nStore:");
	println("<toString(store)>");
	println("Environment:");
	println("<toString(env, store)>");
	
	println("Value: <val>");
}