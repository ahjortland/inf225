module Simple

start syntax Program = Expr expr;

syntax Expr
	= ID var                 // variables
	| NUM num                // integers
	| STRING string			 // strings
	| Expr function "(" ExprList exprs ")"     // function call
	| ScopeExpr expr // scoping 
	| "(" Expr e ")"        // parentheses
	| "#" "(" Expr e ")"
	> left (Expr e1 "*" Expr e2 | Expr e1 "/" Expr e2)       // multiplication
	> left (Expr e1 "+" Expr e2 | Expr e1 "-" Expr e2)       // addition
	> left (Expr e1 "\>" Expr e2 | Expr e1 "\<" Expr e2 | Expr e1 "\>=" Expr e2 | Expr e1 "\<=" Expr e2 | Expr e1 "==" Expr e2)       // comparison
	> "let" ID var "=" Expr e1 "in" Expr e2 "end"
	| "if" cond Expr "then" ScopeExpr e1 "else" ScopeExpr e2 "end"
	// Loops
	| "while" cond Expr "do" ScopeExpr expr "done"
	| "do" ScopeExpr expr "while" Expr cond "done"
	| "for" ID var "=" Expr start ".." Expr end "do" ScopeExpr expr "done"
	> right ID var "=" Expr e
	| "function" "(" ArgList args ")" ScopeExpr body
	> left Expr e1 ";" Expr e2
	;

syntax ArgList
	= ID name
	| ID name "," ArgList rest
	| ""
	;
	
syntax ExprList
	= Expr expr
	| Expr expr "," ExprList rest
	| ""
	;

syntax ScopeExpr
	= "{" Expr "}"
	;

	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_] [a-zA-Z_0-9]* !>> [a-zA-Z_0-9];

lexical STRING = "\"" !<< ("\"" (![\\\"] | ("\\" [\\\"]))* "\"") !>> "\"";

// numbers
lexical NUM = [\-]? [0-9] !<< [0-9]+ !>> [0-9];

lexical LAYOUT = [\ \t\n\r\f]*;

layout SPC = [\ \t\n\r\f]*;

