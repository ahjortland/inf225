module Evaluator
import Simple;
import Environment;
import Value;
import Store;

import String;
import ParseTree;
import IO;
import List;

@doc{Evaluate a program.}
public tuple[Value, Env, Store] eval((Program)`<Expr e>`) {
	Env env = newEnv();
	Store store = newStore();
	return eval(e, env, store);
}

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment.}
public tuple[Value, Env, Store] eval((Expr)`function(<ArgList args>) <ScopeExpr body>`, Env env, Store store) {
	println("Defining function (<args>) <body> ");
	return <Fun(args, body, env), env, store>;
}

@doc{Evaluate a scoping expression}
public tuple[Value, Env, Store] eval((Expr)`<ScopeExpr e>`, Env env, Store store) {
	return eval(e, env, store);
}

@doc{Evaluate a ScopeExpr}
public tuple[Value, Env, Store] eval((ScopeExpr)`{ <Expr e> }`, Env env, Store store) {
	Env scopeEnv = enterScope(env);
	<eVal, eEnv, store> = eval(e, scopeEnv, store);
	return <eVal, exitScope(eEnv), store>;
}

@doc{The value of an integer literal is the value of integer itself.}
public tuple[Value, Env, Store] eval((Expr)`<NUM i>`, Env env, Store store) {
	return <Int(toInt(unparse(i))), env, store>;
}

@doc{The value of a string literal is the value of the string itself.}
public tuple[Value, Env, Store] eval((Expr)`<STRING s>`, Env env, Store store) {
	// Ignore surounding '"' characters.
	return <String(unparse(s)[1..-1]), env, store>;
}

@doc{The value of a variable is the value it's bound to in the environment.}
public tuple[Value, Env, Store] eval((Expr)`<ID x>`, Env env, Store store) {
	return <getValue(lookup(x, env), store), env, store>;
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in an environment where the
argument is bound.}
public tuple[Value, Env, Store] eval((Expr)`<Expr f> ( <ExprList e> )`, Env env, Store store) {
	<fun, env, store> = eval(f, env, store);
	<eVal, eEnv, store> = evalList(e, env, store);
	
	if(Fun(argNames, body, fenv) := fun) {
		list[tuple[ID, Value]] names = zip(argList(argNames), eVal);
		
		Env funEnv = enterScope(fenv);
		for(name <- names) {
			<i, store> = allocate(store);
			store = setValue(i, name[1], store);
			funEnv = declare(name[0], i, funEnv);
		}
		
		<returnVal, returnEnv, store> = eval(body, funEnv, store);
		return <returnVal, eEnv, store>;
	} else {
		throw "Expression should be a function";
	}
}

public tuple[list[Value], Env, Store] evalList((ExprList)``, env, store) {
	return <[], env, store>;
}

public tuple[list[Value], Env, Store] evalList((ExprList)`<Expr expr>`, env, store) {
	<eVal, eEnv, store> = eval(expr, env, store);
	return <[eVal], eEnv, store>;
}

public tuple[list[Value], Env, Store] evalList((ExprList)`<Expr expr>,<ExprList rest>`, env, store) {
	<eVal, eEnv, store> = eval(expr, env, store);
	<restVal, restEnv, store> = evalList(rest, eEnv, store);
	
	return <[eVal] + restVal, restEnv, store>;
}

public list[ID] argList((ArgList)``) {
	return [];
}

public list[ID] argList((ArgList)`<ID name>`) {
	return [name];
}

public list[ID] argList((ArgList)`<ID name>,<ArgList rest>`) {
	return [name] + argList(rest);
}

@doc{Expression wrapped in parantheses}
public tuple[Value, Env, Store] eval((Expr)`(<Expr e>)`, Env env, Store store) {
	return eval(e, env, store);
}

@doc{Sequencing expressions. First evaluate e1, then e2 in the environment
from e1}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>;<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);

	return eval(e2, env1, store);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>*<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue * e2Val.intValue), env2, store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>+<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);
	if(Int(num1) := e1Val) {
		if(Int(num2) := e2Val) {
			return <Int(e1Val.intValue + e2Val.intValue), env2, store>;
		}
		if(String(string2) := e2Val) {
			return <String("<num1><string2>"), env2, store>;
		}
	}
	if(String(string1) := e1Val) {
		if(Int(num2) := e2Val) {
			return <String("<string1><num2>"), env2, store>;
		} 
		if(String(string2) := e2Val) {
			return <String("<string1><string2>"), env2, store>;
		}
	}
	throw "Must be of type String or Int";
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>-<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue - e2Val.intValue), env2, store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>/<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue / e2Val.intValue), env2, store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1> == <Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue == e2Val.intValue ? 1 : 0), env2, store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>\<<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue < e2Val.intValue ? 1 : 0), env2, store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>\><Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue > e2Val.intValue ? 1 : 0), env2, store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>\<=<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue <= e2Val.intValue ? 1 : 0), env2, store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Env, Store] eval((Expr)`<Expr e1>\>=<Expr e2>`, Env env, Store store) {
	<e1Val, env1, store> = eval(e1, env, store);
	<e2Val, env2, store> = eval(e2, env1, store);

	return <Int(e1Val.intValue >= e2Val.intValue ? 1 : 0), env2, store>;
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public tuple[Value, Env, Store] eval((Expr)`if <Expr c> then <ScopeExpr e1> else <ScopeExpr e2> end`, Env env, Store store) {
	<exprVal, exprEnv, store> = eval(c,env, store);
	if(exprVal.intValue != 0)
		return eval(e1, exprEnv, store);
	else
		return eval(e2, exprEnv, store);
}

public tuple[Value, Env, Store] eval((Expr)`if <Expr c> then <ScopeExpr e1> else <ScopeExpr e2> end`, Env env, Store store) {
	<exprVal, exprEnv, store> = eval(c,env, store);
	if(exprVal.intValue != 0)
		return eval(e1, exprEnv, store);
	else
		return eval(e2, exprEnv, store);
}

public tuple[Value, Env, Store] eval((Expr)`while <Expr c> do <ScopeExpr e> done`, Env env, Store store) {
	<cVal, cEnv, store> = eval(c, env, store);
	eVal = cVal;
	// Would prefer a recursive definition on this.
	while(cVal.intValue != 0) {
		<eVal, eEnv, store> = eval(e, cEnv, store);
		<cVal, cEnv, store> = eval(c, eEnv, store);	
	}
	return <eVal, cEnv, store>;
}

public tuple[Value, Env, Store] eval((Expr)`do <ScopeExpr e> while <Expr c> done`, Env env, Store store) {
	<eVal, eEnv, store> = eval(e, env, store);
	<cVal, cEnv, store> = eval(c, eEnv, store);	
	while(cVal.intValue != 0) {
		<eVal, eEnv, store> = eval(e, cEnv, store);
		<cVal, cEnv, store> = eval(c, eEnv, store);	
	} 
	return <eVal, cEnv, store>;
}

public tuple[Value, Env, Store] eval((Expr)`for <ID var> = <Expr st> .. <Expr end> do <ScopeExpr e> done`, Env env, Store store) {
	int l = -1;
	try {
		l = lookup(var, env);
	} catch: {
		<l, store> = allocate(store);
	}
	forEnv = enterScope(env);
	forEnv = assign(var, l, forEnv);
	
	<val, forEnv, store> = eval(st, forEnv, store);
	store = setValue(l, val, store);
	<endVal, forEnv, store> = eval(end, forEnv, store);
	
	begin = val.intValue;
	stop = endVal.intValue;
	
	while(begin < stop) {
		<val, forEnv, store> = eval(e, forEnv, store);
		
		begin = getValue(l, store).intValue + 1;
		store = setValue(l, Int(begin), store);
	}
	return <val, env, store>;
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public tuple[Value, Env, Store] eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env, Store store) {
	<l, store> = allocate(store);

	<v, vEnv, store> = eval(e1, env, store);
	innerEnv = enterScope(vEnv);
	store = setValue(l, v, store);
	innerEnv = declare(x, l, innerEnv);
	<e2Val, e2Env, store > = eval(e2, innerEnv, store);
	
	return <e2Val, exitScope(e2Env), store>;
}

@doc{'Assignment expression. Evaluate the expression, then assign the result 
to the ID 'var'}
public tuple[Value, Env, Store] eval((Expr)`<ID var> = <Expr e>`, Env env, Store store) {
	int l = -1;
	try {
		l = lookup(var, env);
	} catch: {
		<l, store> = allocate(store);
	}
	
	env = assign(var, l, env);
	<val, env, store> = eval(e, env, store);
	store = setValue(l, val, store);
	 
	return <val, env, store>;
}

public tuple[Value, Env, Store] eval((Expr)`# ( <Expr e> )`, Env env, Store store) {
	<val, env, store> =  eval(e, env, store);
	println("<toString(val)>");
	return <val, env, store>;
}