module Tests::Tests
import Environment;
import Evaluator;
import Simple;
import Value;
import Store;

import IO;

test bool Numbers() {
	return eval((Program)`5`)[0] == Int(5);
}



test bool Arith1() { 
	return eval((Program)`5+5`)[0] == Int(10);
}

test bool Arith2() {
	return eval((Program)`1+2*3`)[0] == Int(7);
}

test bool Arith3() {
	return eval((Program)`10-2`)[0] == Int(8);
}

test bool Arith4() {
	return eval((Program)`10/2`)[0] == Int(5);
}

test bool Arith5() {
	return eval((Program)`(1+2)*3`)[0] == Int(9);
}

test bool Arith6() {
	return eval((Program)`((10)/(2))`)[0] == Int(5); 
}



test bool If1() {
	return eval((Program)`if 2*3 == 6 then { 42  } else { 69 } end`)[0] == Int(42);
}

test bool If2() {
	return eval((Program)`if 6 == 5 then { 42  } else { 69 } end`)[0] == Int(69);
}

test bool If3() {
	return eval((Program)`if 10 \< 5 then { 1 } else { 0 } end`)[0] == Int(0);
}



test bool Assign1() {
	tuple[Value, Env, Store] result = eval((Program)`a=3`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(3) 
		&& getValue(lookup((ID)`a`, env), store) == Int(3);
}

test bool Assign2() {
	tuple[Value, Env, Store] result = eval((Program)`a=3; a=5`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(5) 
		&& getValue(lookup((ID)`a`, env), store) == Int(5);
}

test bool Assign3() {
	tuple[Value, Env, Store] result = eval((Program)`a=3; a=a+3`);
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	
	return val == Int(6) 
		&& getValue(lookup((ID)`a`, env), store) == Int(6);
}



test bool Scope1() {
	tuple[Value, Env, Store] result = eval((Program)`{a=3}`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(3) 
		&& env  == [()];
}

test bool Scope2() {
	tuple[Value, Env, Store] result = eval((Program)`a=1;{a=3}`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(3)
		&& getValue(lookup((ID)`a`, env), store) == Int(3);
}



test bool Let1() {
	tuple[Value, Env, Store] result = eval((Program)`x=4; let x=5 in x=1 end`);
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(1) 
		&& getValue(lookup((ID)`x`, env), store) == Int(4);
}

test bool Let2() {
	tuple[Value, Env, Store] result = eval((Program)`let x=5 in x=1 end`);
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return env == [()]
		&& val == Int(1);
}

test bool Let3() {
	tuple[Value, Env, Store] result = eval((Program)`let x = 5 in let x = 7 in x = 9 end; x end`);
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return env == [()]
		&& val == Int(5);
}

test bool Let4() {
	tuple[Value, Env, Store] result = eval((Program)`let x = 5 in let y = 7 in x = 9 end; x end`);
	Value val = result[0];
	Env env = result[1];
	Store store = result[2]; 
	
	return env == [()]
		&& val == Int(9);
}



test bool Function1() {
	tuple[Value, Env, Store] result = eval((Program)`f = function(x) { x*x }; f(2)`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(4);
}

test bool Function2() {
	tuple[Value, Env, Store] result = eval((Program)`x=5;f = function(x) { x*x }; f(2)`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(4) 
		&& getValue(lookup((ID)`x`, env), store) == Int(5);
}

test bool Function3() {
	tuple[Value, Env, Store] result = eval((Program)`f = function(x) { x(3) }; f(function(x) { x*x })`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(9);
}

test bool Function4() {
	tuple[Value, Env, Store] result = eval((Program)`twice = function(f, x) { f(f(x)) }; twice(function(x) { x*x }, 2)`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(16); // (2*2)*(2*2) == 16
}

test bool Function4() {
	tuple[Value, Env, Store] result = eval((Program)`f = function(x) { if(x \> 1) then { x * f(x-1)} else { 1 } end}; f(5)`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(120);
}



test bool StaticScoping1() {
	tuple[Value, Env, Store] result = eval((Program)`g=5; f = function() { g }; h = function(g) {f()}; h(2)`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(5);
}



test bool String1() {
	tuple[Value, Env, Store] result = eval((Program)`"Hello, world!"`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return env == [()] 
		&& val == String("Hello, world!");
}

test bool String2() {
	tuple[Value, Env, Store] result = eval((Program)`"Hello," + " world!"`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return env == [()] 
		&& val == String("Hello, world!");
}



test bool while1() {
	tuple[Value, Env, Store] result = eval((Program)`x = 0; a = ""; while(x \< 10) do { a = a + "|"; x = x+1 } done`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(10) 
		&& getValue(lookup((ID)`x`, env), store) == Int(10)
		&& getValue(lookup((ID)`a`, env), store) == String("||||||||||");
}

test bool while2() {
	tuple[Value, Env, Store] result = eval((Program)`x = 0; a = ""; while(x == 10) do { a = a + "|"; x = x+1 } done`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(0) 
		&& getValue(lookup((ID)`x`, env), store) == Int(0)
		&& getValue(lookup((ID)`a`, env), store) == String("");
}



test bool do1() {
	tuple[Value, Env, Store] result = eval((Program)`x = 0; a = ""; do { a = a + "|"; x = x+1 } while(x \< 10) done`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(10) 
		&& getValue(lookup((ID)`x`, env), store) == Int(10)
		&& getValue(lookup((ID)`a`, env), store) == String("||||||||||");
}

test bool do2() {
	tuple[Value, Env, Store] result = eval((Program)`x = 0; a = ""; do { a = a + "|"; x = x+1 } while(x == 10) done`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(1) 
		&& getValue(lookup((ID)`x`, env), store) == Int(1)
		&& getValue(lookup((ID)`a`, env), store) == String("|");
}



test bool for1() {
	tuple[Value, Env, Store] result = eval((Program)`x=2; for i = 1..10 do { x = i; i+1 } done`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(10) 
		&& getValue(lookup((ID)`x`, env), store) == Int(9);
}

test bool for2() {
	tuple[Value, Env, Store] result = eval((Program)`x=2; for i=1..10 do { x = i; i+1 } done`); 
	Value val = result[0];
	Env env = result[1];
	Store store = result[2];
	
	return val == Int(10) 
		&& getValue(lookup((ID)`x`, env), store) == Int(9);
}