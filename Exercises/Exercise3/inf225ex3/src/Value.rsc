module Value
import Simple;
import Environment;

@doc{Values. Integers and functions are values}
data Value
	= Int(int intValue)
	| Fun(ArgList argNames, ScopeExpr expr, Env env)
	| String(str strValue)
	//| Array(Value* val)
	;
	
public str toString(Value val) {
	if(Fun(argName, expr, fenv) := val) {
		return "Fun(<unparse(argName)>, <unparse(expr)>)";
	}
	if(Int(number) := val) {
		return "<number>";
	}
	if(String(string) := val) {
		return "\"<string>\"";
	}
	return "<val>";
}