module Pretty
import Simple;

public str pretty(Program p) {
	return pretty(p.expr);
}

public default str pretty(Expr e) {
	return "[unknown expr: \"<e>\"]";
}

public str pretty((Expr)`function (<ID param>) <ScopeExpr body>`) {
	return "function(<param>) <pretty(body)>";
}

public str pretty((Expr)`<ID id>=<Expr e>`) {
	return "<id> = <pretty(e)>";
}

public str pretty((Expr)`<ScopeExpr e>`) {
	return "<pretty(e)>";
}

public str pretty((ScopeExpr)`{ <Expr e> }`) {
	return "{\n <pretty(e)> \n}";
}

// Loops
public str pretty((Expr)`while <Expr cond> do <ScopeExpr e> done`) {
	return "while(<pretty(cond)>) do\n<pretty(e)>\ndone";
}

public str pretty((Expr)`do <ScopeExpr e> while <Expr cond> done`) {
	return "do\n<pretty(e)>\nwhile(<pretty(cond)>) done";
}

public str pretty((Expr)`for <Expr init> ; <Expr cond> ; <Expr incr> do <ScopeExpr e> done`) {
	return "for(<pretty(init)>; <pretty(cond)>; <pretty(incr)>) do\n<pretty(e)>\ndone";
}
// End loops

// Sequencing
public str pretty((Expr)`<Expr e1>;<Expr e2>`) {
	return "<pretty(e1)>;\n<pretty(e2)>";
}
// End sequencing

// Comparison operators
public str pretty((Expr)`<Expr e1> \> <Expr e2>`) {
	return "(<pretty(e1)> \> <pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1> \< <Expr e2>`) {
	return "(<pretty(e1)> \< <pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1> \>= <Expr e2>`) {
	return "(<pretty(e1)> \>= <pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1> \<= <Expr e2>`) {
	return "(<pretty(e1)> \<= <pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1> == <Expr e2>`) {
	return "(<pretty(e1)> == <pretty(e2)>)";
}
// End comparison operators

public str pretty((Expr)`<ID i>`) {
	return "<i>";
}

public str pretty((Expr)`<NUM n>`) {
	return "<n>";
}

// Arithmetic operators
public str pretty((Expr)`<Expr e1>+<Expr e2>`) {
	return "(<pretty(e1)>+<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>-<Expr e2>`) {
	return "(<pretty(e1)>+<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>*<Expr e2>`) {
	return "(<pretty(e1)>*<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>/<Expr e2>`) {
	return "(<pretty(e1)>/<pretty(e2)>)";
}
// End arithmetic operators

public str pretty((Expr)`(<Expr e>)`) {
	return "(<pretty(e)>)";
}

public str pretty((Expr)`<ID f>(<Expr e>)`) {
	return "<f>(<pretty(e)>)";
}

public str pretty((Expr)`if <Expr c> then <ScopeExpr e1> else <ScopeExpr e2> end`) {
	return "if <pretty(c)> then <pretty(e1)> else <pretty(e2)> end";
}

public str pretty((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`) {
	return "let <x> = <pretty(e1)> in <pretty(e2)> end";
}
