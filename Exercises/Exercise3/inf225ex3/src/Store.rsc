module Store
import Value;

import List;

alias Store = list[Value];

public Store newStore() {
	return [];
}

public tuple[int, Store] allocate(Store store) {
	location = size(store);
	return <location, store + [Int(0)]>;
}

public Value getValue(int index, Store store) {
	return store[index];
}

public Store setValue(int index, Value val, Store store) {
	store[index] = val;
	return store;
}