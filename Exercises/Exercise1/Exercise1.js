$(document).ready(function() {
	$("form.ex1").each(function() {
		var $result = $(this).find(".result");
		var $input = $(this).find("input[type=text]");
		var $regex = $(this).find('.regex');

		var regex = new RegExp($regex.html(), "i");
		$regex.css("color", "black");

		$(this).on("submit", function(e) {
			e.preventDefault();
			
			var string = $input.val()
			string = string.replace(regex, function(str) {return '<strong>' + str + '</strong>'});

			$result.prepend("<li><span>" + string + "</span></li>");
		});
	});
});
