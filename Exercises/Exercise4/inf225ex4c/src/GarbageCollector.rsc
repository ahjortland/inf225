module GarbageCollector
import Set;
import List;

import Value;
import Environment;
import Store;

alias Location = int;

public Store runGC(Store store, Env[Location] env) {
	set[Location] used = collect(store, env, {});
	for(int val <- index(store)) {
		if(!(val in used)) {
			store[val] = Void();
		}
	}
	return store;
}

public set[Location] collect(Store store, Env[Location] env, set[Location] used) {
	for(Scope sc <- env.scopes) {
		for(str i <- sc) {
			Location lc = sc[i];
			if(!(lc in used)) {
				used = used + lc;
				Value val = get(lc, store); 
				if(val is Fun) {
					used = collect(store, val.env, used);
				}
			}
		}
	}
	if(!(env.chain has scopes)) {
		return used;
	}
	return collect(store, env.chain, used);
}
