module Store
import Value;
import List;

alias Store = list[Value];

public tuple[int, Store] allocate(Store store) {
	location = size(store);
	store = store + [Void()];
	return <location, store>;
}

public Store put(int location, Value val, Store store) {
	store[location] = val;
	return store;
}

public Value get(int location, Store store) {
	return store[location];
}
