module Environment
import ParseTree;
import List;
import IO;

@doc{A scope maps strings to ints.}
alias Scope[&T] = map[str, &T];

@doc{An environment is a list (stack) of scopes.}
data Env[&T] = Env(list[Scope[&T]] scopes, Env[&T] chain) | Env(type[&T]);

@doc{Look up an identifier in the environment.}
public &T lookup(Tree name, Env[&T] env) {
	for(sc <- env.scopes) {
		if(unparse(name) in sc)
			return sc[unparse(name)];
	}
	
	throw "Undefined variable \'<unparse(name)>\' at <name@\loc>";
}

@doc{Declare an identifier, giving it a int. It must not already have
been declared in the same scope.}
public Env[&T] declare(Tree name, &T val, Env[&T] env) {
	sc = env.scopes[0]; // get the current scope
	
	if(unparse(name) in sc) {
		throw "Variable \'<unparse(name)>\' already declared at <name@\loc>";
	}
	else {
		sc[unparse(name)] = val;
		env.scopes[0] = sc;
		return env;
	}
}

@doc{Update the int of a variable in the environment}
public Env[&T] assign(Tree name, int val, Env[&T] env) {
	for(i <- index(env)) {
		sc = env.scopes[i];
		
		if(unparse(name) in sc) { // find the scope in which the variable is declared
			sc[unparse(name)] = val;
			env.scopes[i] = sc;
			return env;
		}
	}
	
	throw "Undefined variable \'<unparse(name)>\' at <name@\loc>";
}

@doc{Make a new scope in the environment}
public Env[&T] enterScope(Env[&T] env) {
	env.scopes = [(), *env.scopes];
	return env;
}

@doc{Discard the current scope from the environment}
public Env[&T] exitScope(Env[&T] env) {
	env.scopes = env.scopes[1..];
	
	return env;
}

@doc{Make a new environment}
public Env[&T] newEnv(type[&T] typ) { 
	return Env([()], Env(typ));
}


@doc{For debugging.}
public void printEnv(Env[&T] env) {
	i = 0;
	for(sc <- env.scopes) {
		println("  Scope <i>:");
		i = i + 1;
		for(k <- sc) {	
//			if(Fun(argName, expr, fenv) := sc[k])
//				println("  <k>: Fun(<unparse(argName)>, <unparse(expr)>, <fenv>)");
//			else
				println("  <k>: <sc[k]>");
		}
	}
}

public Env[&T] link(Env[&T] env, Env[&T] chain) {
	chain.chain = env;
	return env;
}
