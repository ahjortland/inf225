module TestGarbageCollector
import Environment;
import Value;
import Store;
import GarbageCollector;
import Simple;

test bool BasicTest() {
	return runGC([Int(12), Str("Hello"), Int(3)], 
			exitScope(declare((ID)`g`,2, 
				enterScope(
					declare((ID)`f`,1, newEnv(#Location)))))) 
			== [Void(), Str("Hello"), Void()];
}