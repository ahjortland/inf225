module TypeChecker
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;
import List;

data Type
	= Int()
	| Str()
	| Fun(list[Type] paramTypes, Type returnType)
	// Hack to see if a function returns.
	| Returns()
	| NotReturns()
	;
	

private Type evalType(Tree typ) {
	switch(typ) {
		case (SimpleTypeExpr)`int`: return Int();
		case (SimpleTypeExpr)`str`: return Str();
		case (SimpleTypeExpr)`(<TypeExpr t>)`: return evalType(t);
		case (TypeExpr)`<SimpleTypeExpr t>`: return evalType(t);
		case (TypeExpr)`<{SimpleTypeExpr ","}* pTypes> -\> <TypeExpr rType>`:
			return Fun([evalType(p) | p <- pTypes], evalType(rType));
		default: throw "Unknown type <typ>: <[typ]>";
	}
}


@doc{Declare a function. The function is added to the environement, but the body
is left unchecked.}
public Env[Type] define((Decl)`<SimpleTypeExpr retType> <ID f>(<{ParamDecl ","}* paramList>) <BlockStat body>`, Env[Type] env) {
	list[Type] paramTypes = [];
	for((ParamDecl)`<TypeExpr pType> <ID pName>` <- paramList) {
		paramTypes += evalType(pType);
	}
	env = declare(f, Fun(paramTypes, evalType(retType)), env);
	
	return env;
}

@doc{Declare a global variable and assign the expression e to it}
public Env[Type] define((Decl)`<SimpleTypeExpr t> <ID name> = <Expr e>;`, Env[Type] env) {
	return declare(name, evalType(t), env);
}

@doc{Declare a global variable}
public Env[Type] define((Decl)`<SimpleTypeExpr t> <ID name>;`, Env[Type] env) {
	return declare(name, evalType(t), env);
}

@doc{Check a variable declaration}
public Env[Type] define((Decl)`<SimpleTypeExpr t> <ID name> = <Expr e>;`, Env[Type] env) {
	return declare(name, evalType(t), env);
}

@doc{Check a function declaration. The function is assumed to be already added to the
environment (via define()), and we now check the body.}
public Decl check((Decl)`<SimpleTypeExpr retType> <ID f>(<{ParamDecl ","}* paramList>) <BlockStat body>`, Env[Type] env) {
	list[Type] paramTypes = [];
	list[ID] paramNames = [];
	for((ParamDecl)`<TypeExpr pType> <ID pName>` <- paramList) {
		paramTypes += evalType(pType);
		paramNames += pName;
	}

	env = enterScope(env);
	for(<pType, pName> <- zip(paramTypes, paramNames)) {
		env = declare(pName, pType, env);
	}
	env = declare((ID)`__RETURN__`, evalType(retType), env);
	env = declare((ID)`__DOESRETURN__`, NotReturns(), env);
	<body, env> = check(body, env);
	if(lookup((ID)`__DOESRETURN__`, env) != Returns()) {
		throw "Function <unparse(f)> does not return anything";
	}
	env = exitScope(env);
	
	return (Decl)`<SimpleTypeExpr retType> <ID f>(<{ParamDecl ","}* paramList>) <BlockStat body>`;
}

public Decl check((Decl)`<SimpleTypeExpr t> <ID name> = <Expr e>;`, Env[Type] env) {
	<e, eType> = check(e, env);
	if(evalType(t) != eType) {
		throw "The type <evalType(t)> of declaration <name> does not match type <eType> of expression <e>";
	}
	
	return (Decl)`<SimpleTypeExpr t> <ID name> = <Expr e>;`;
}

public Decl check((Decl)`<SimpleTypeExpr t> <ID name>;`, Env[Type] env) {
	return (Decl)`<SimpleTypeExpr t> <ID name>;`;
}

@doc{Typecheck a program.}
public tuple[Program, Env[Type]] check((Program)`<Decl* decls>`) {
	//try {
		Env[Type] env = newEnv(#Type);
		for(Decl d <- decls) {
			env = define(d, env);
		}
		
		// rewrite the parse tree of the declaration list
		// to include the results of typechecking
		decls = top-down-break visit(decls) {
			case Decl d => check(d, env)
		}
		
		return <(Program)`<Decl* decls>`, env>;
	//}
	//catch ex: {
	//	println(ex);
	//	return (Program)`0`;
	//}
}

@doc{The type of an integer literal is int.}
public tuple[Expr, Type] check((Expr)`<NUM i>`, Env[Type] env) {
	return <(Expr)`<NUM i>`, Int()>;
}

@doc{The type of an string literal is str.}
public tuple[Expr, Type] check((Expr)`<STRING s>`, Env[Type] env) {
	return <(Expr)`<STRING s>`, Str()>;
}

@doc{The type of a variable is the type it's bound to in the environment.}
public tuple[Expr, Type] check((Expr)`<ID x>`, Env[Type] env) {
	typ = lookup(x, env);
	return <(Expr)`<ID x>`, typ>;
	
	// here, we could also change the name, if we wanted to, e.g., include the type in
	// the name:
	// return <parse(#Expr, "<x>_<mangleType(typ)>"), typ>;
}

@doc{Function calls.}
public tuple[Expr, Type] check((Expr)`<ID f>(<{Expr ","}* argList>)`, Env[Type] env) {
	funTyp = lookup(f, env);
	
	list[Type] argTypes = [];
	list[Expr] argChecked = [];
	
	for(a <- argList) { // type check arguments
		<a, aTyp> = check(a, env);
		argTypes += aTyp;
		argChecked += a;
	}
	argList = makeArgList(argChecked);
	
	if(Fun(paramTypes, returnTyp) := funTyp) {
		// check that actual argument types line up with formal parameter types
		for(<aTyp, pTyp> <- zip(argTypes, paramTypes)) {
			if(aTyp != pTyp)
				throw "Wrong argument type <aTyp> to function <f>, expected <pTyp>, at <expr@\loc>";
		}
		return <(Expr)`<ID f>(<ArgList argList>)`, returnTyp>;
	}
	else {
		throw "Variable <f> is not a function, at <f@\loc>";
	}
}

@doc{Parenthesis.}
public tuple[Expr, Type] check((Expr)`(<Expr e>)`, Env[Type] env) {
	<e, typ> = check(e, env);
	return <(Expr)`(<Expr e>)`, typ>;
}

@doc{Arithmetic. Check that both operands are ints.}
public tuple[Expr, Expr, Type] checkArithOp(Expr e1, Expr e2, Env[Type] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(<Int(), Int()> == <typ1, typ2>) {
		return <e1, e2, Int()>;
	}
	else {
		throw "Wrong operands \<<typ1>, <typ2>\> to operator";
	}
}

public tuple[Expr, Type] check((Expr)`<Expr e1>*<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>*<Expr e2>`, typ>;
}


public tuple[Expr, Type] check((Expr)`<Expr e1>+<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>+<Expr e2>`, typ>;
}


public tuple[Expr, Type] check((Expr)`<Expr e1>-<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>-<Expr e2>`, typ>;
}


public tuple[Expr, Type] check((Expr)`<Expr e1>/<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkArithOp(e1, e2, env);
	return <(Expr)`<Expr e1>/<Expr e2>`, typ>;
}

@doc{Helper for checking comparisons. We assume that things of the same type can be compared.
Always returns an int.}
public tuple[Expr, Expr, Type] checkCompOp(Expr e1, Expr e2, Env[Type] env) {
	<e1, typ1> = check(e1, env);
	<e2, typ2> = check(e2, env);
	
	if(typ1 == typ2) {
		return <e1, e2, Int()>;
	}
	else {
		throw "Can\'t compare <typ1> and <typ2>, at <expr@\loc>";
	}
}


public tuple[Expr, Type] check((Expr)`<Expr e1> == <Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>==<Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\<<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\<<Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\><Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\><Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\<=<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\<=<Expr e2>`, typ>;
}

public tuple[Expr, Type] check((Expr)`<Expr e1>\>=<Expr e2>`, Env[Type] env) {
	<e1, e2, typ> = checkCompOp(e1, e2, env);
	return <(Expr)`<Expr e1>\>=<Expr e2>`, typ>;
}

@doc{Assignment. Variable type must match the expression type}
public tuple[Expr, Type] check((Expr)`<ID x>=<Expr e>`, Env[Type] env) {
	xTyp = lookup(x, env);
	<e, eTyp> = check(e, env);
	
	if(xTyp != eTyp) {
		throw "Wrong type in assignment";
	}
	
	return <(Expr)`<ID x>=<Expr e>`, xTyp>;	
}

@doc{Check block Statements}
public tuple[BlockStat, Env[Type]] check((BlockStat)`{ <Stat* stats> }`, Env[Type] env) {
	env = enterScope(env);
	for(Stat s <- stats) {
		<s, env> = check(s, env);
	}
	env = exitScope(env);
	return <(BlockStat)`{ <Stat* stats> }`, env>;
}

@doc{'If' expression. Condition should be an int.}
public tuple[Stat, Env[Type]] check((Stat)`if ( <Expr c> ) <Stat s1> else <Stat s2>`, Env[Type] env) {
	<c, cTyp> = check(c, env);
	<s1, env> = check(s1, env);
	<s2, env> = check(s2, env);
	
	if(cTyp != Int()) { 
		throw "Condition must be an integer";
	}
	
	return <(Stat)`if ( <Expr c> ) <Stat s1> else <Stat s2>`, env>;
}

@doc{Type check an if statement without an else block. This will push a new 
scope onto the environment.}
public tuple[Stat, Env[Type]] check((Stat)`if ( <Expr c> ) <Stat s>`, Env[Type] env) {
	env = enterScope(env);
	<c, cTyp> = check(c, env);
	<s, env> = check(s, env);
	
	if(cTyp != Int()) { 
		throw "Condition must be an integer";
	}
	env = exitScope(env);
	
	return <(Stat)`if ( <Expr c> ) <Stat s>`, env>;
}

@doc{Typecheck a variable declaration.}
public tuple[Stat, Env[Type]] check((Stat)`<SimpleTypeExpr typ> <ID id>;`, Env[Type] env) {
	env = declare(id, evalType(typ), env);
	
	return <(Stat)`<SimpleTypeExpr typ> <ID id>;`, env>;
}

@doc{Typecheck a variable declaration with an initializer.} 
public tuple[Stat, Env[Type]] check((Stat)`<SimpleTypeExpr typ> <ID id> = <Expr e>;`, Env[Type] env) {
	env = declare(id, evalType(typ), env);
	<e, eType> = check(e, env);
	if(eType != evalType(typ))  {
		throw "The type <evalType(typ)> of declaration <id> does not match type <eType> of expression <e>"; 
	}
	
	return <(Stat)`<SimpleTypeExpr typ> <ID id>;`, env>;
}

@doc{Typecheck the return statement. This will lookup the type of the function,
and it will set a variable which declares that the function might return.}
public tuple[Stat, Env[Type]] check((Stat)`return <Expr e>;`, Env[Type] env) {
	<e, eTyp> = check(e, env);
	Type retType = lookup((ID)`__RETURN__`, env);
	if(retType != eTyp) {
		throw "Return type <retType> of function does not match type <eTyp> of return statement <e>"; 
	}
	env = assign((ID)`__DOESRETURN__`, Returns(), env);
	
	return <(Stat)`return <Expr e>;`, env>;
}

@doc{Typecheck a statement which is an expression.}
public tuple[Stat, Env[Type]] check((Stat)`<Expr e>;`, Env[Type] env) {
	<e, eType> = check(e, env);
	return <(Stat)`<Expr e>;`, env>;
}

@doc{Typecheck a statement which is a block statement.}
public tuple[Stat, Env[Type]] check((Stat)`<BlockStat stat>`, Env[Type] env) {
	<stat, env> = check(stat, env);
	return <(Stat)`<BlockStat stat>`, env>;
}

@doc{Typecheck a while loop. This will push a new scope onto the environment.}
public tuple[Stat, Env[Type]] check((Stat)`while ( <Expr c> ) <Stat s>`, Env[Type] env) {
	env = enterScope(env);
	<c, cTyp> = check(c, env);
	if(cTyp != Int()) { 
		throw "Condition must be an integer";
	}
	<s, env> = check(s, env);
	env = exitScope(env);

	return<(Stat)`while ( <Expr c> ) <Stat s>`, env>;
}

@doc{Typecheck a do-while loop. This will push a new scope onto the 
environment.}
public tuple[Stat, Env[Type]] check((Stat)`do <Stat s> while( <Expr c> )`, Env[Type] env) {
	env = enterScope(env);
	<c, cTyp> = check(c, env);
	if(cTyp != Int()) { 
		throw "Condition must be an integer";
	}
	<s, env> = check(s, env);
	env = exitScope(env);

	return<(Stat)`do <Stat s> while( <Expr c> )`, env>;
}

@doc{Typecheck a for loop. This will push a new scope onto the environment.}
public tuple[Stat, Env[Type]] check((Stat)`for ( <Expr init>, <Expr cond>, <Expr iter> ) <Stat s>`, Env[Type] env) {
	env = enterScope(env);
	<init, initType> = check(init, env);
	<cond, condType> = check(cond, env);
	if(condType != Int()) {
		throw "Condition must be an integer";
	}
	<iter, iterType> = check(iter, env);
	<s, env> = check(s, env);
	env = exitScope(env);
	
	return <(Stat)`for ( <Expr init>, <Expr cond>, <Expr iter> ) <Stat s>`, env>;
}

// this will convert a type into a string suitable for
// including in an identifier name
private str mangleType(Type t) {
	switch(t) {
		case Int(): return "int";
		case Str():	return "str";
		case Fun(list[Type] ps, Type rt):
		return "<intercalate("_", [mangleType(p) | p <- ps])>__<mangleType(rt)>";
	}
}

@doc{Pick the type from a check() return tuple.}
public Type getType(tuple[&_, Type] tup) {
	return tup[1];
}

public Type getType(tuple[&_, Env[Type], ID] tup) {
	return lookup(tup[3], tup[1]);
}
