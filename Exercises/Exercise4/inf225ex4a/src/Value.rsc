module Value
import Simple;
import Environment;

@doc{Values. Integers and functions are values}
data Value
	= Int(int intValue)
	| Str(str strValue)
	| Fun(list[ParamDecl] paramList, Expr body, Env env)
	;
