module Evaluator
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;
import List;

alias Location = int;
data Result = Result(Value val, Store store);

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public tuple[Env[Location], Store, Location] define((Decl)`<SimpleTypeExpr retType> <ID f>(<{ParamDecl ","}* paramList>) <BlockStat body>`, Env[Location] env, Store store) {
	<l, store> = allocate(store);
	env = declare(f,  l, env);
	
	store = put(l, Fun([p | p <- paramList], body, env), store);

	return <env, store, l>;
}

@doc{Declare a global variable.}
public tuple[Env[Location], Store, Location] define((Decl)`<SimpleTypeExpr retType> <ID x>;`, Env[Location] env, Store store) {
	<l, store> = allocate(store);
	env = declare(x,  l, env);

	return <env, store, l>;
}

@doc{Declare and initialize a global variable.}
public tuple[Env[Location], Store, Location] define((Decl)`<SimpleTypeExpr retType> <ID x> = <Expr e>;`, Env[Location] env, Store store) {
	<l, store> = allocate(store);
	env = declare(x,  l, env);
	<val, store> = eval(e, env, store);
	store = put(l, val, store);

	return <env, store, l>;
}

@doc{Evaluate a program.}
public tuple[Value, Store] eval((Program)`<Decl* decls>`) {
	Store store = [];
	Env[Location] env = newEnv(#Location);
	list[Location] locs = [];
	for(Decl d <- decls) {
		<env, store, l> = define(d, env, store);
		locs += l;
	}
	
	// update stored functions so that all functions have the
	// same top-level environment
	for(l <- locs) {
		if(Fun(funParams, funBody, _) <- get(l, store))
			store = put(l, Fun(funParams, funBody, env), store);
	}

	<val, store> = eval((Expr)`main()`, env, store);
	return <val, store>;
}

@doc{The value of an integer literal is the value of integer itself.}
public tuple[Value, Store] eval((Expr)`<NUM i>`, Env[Location] env, Store store) {
	return <Int(toInt(unparse(i))), store>;
}

public tuple[Value, Store] eval((Expr)`<STRING s>`, Env[Location] env, Store store) {
	str string = unparse(s);
	string = substring(string, 1, size(string)-1);
	string = replaceAll(string, "\\\\", "\\");
	string = replaceAll(string, "\\\"", "\"");
	
	return <Str(string), store>;
}

@doc{The value of a variable is the value it's bound to in the environment.}
public tuple[Value, Store] eval((Expr)`<ID x>`, Env[Location] env, Store store) {
	l = lookup(x, env);
	return <get(l, store), store>;
}

public tuple[Value, Store] eval((Expr)`(<Expr e>)`, Env[Location] env, Store store) {
	return eval(e, env, store);
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in the function's definition
environment, with the argument is bound to the value.}
public tuple[Value, Store] eval((Expr)`<ID f>(<{Expr ","}* argList>)`, Env[Location] env, Store store) {
	fun = get(lookup(f, env), store);
	
	if(fun is Fun) {
		funEnv = enterScope(fun.env); // this has an effect: allow shadowing of variables
		// ([a | a <- argList] makes a list out of a comma-separated parsetree list)
		for(<param, arg> <- zip(fun.paramList, [a | a <- argList])) {
			// first, evaluate argument
			<argVal, store> = eval(arg, env, store); // evaluate in *original env*
			
			// then, allocate storage and declare parameter
			<l, store> = allocate(store);
			store = put(l, argVal, store);
			funEnv = declare(param.paramName, l, funEnv); // declare in funEnv
		}
		// evaluate body
		try {
			eval(fun.body, funEnv, store);
		} catch Result(rVal, rStore):
			return <rVal, rStore>;
		throw "Function did not return a value";
	}
	else {
		throw "Expression should be a function";
	}
}

@doc{Parenthesis.}
public tuple[Value, Store] eval((Expr)`(<Expr e>)`, Env[Location] env, Store store) {
	return eval(e, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>*<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue * v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>+<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue + v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>-<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue - v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>/<Expr e2>`, Env[Location] env, Store store) {
		<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue / v2.intValue), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>==<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue == v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\<<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue < v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\><Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue > v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\<=<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue <= v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\>=<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue >= v2.intValue ? 1 : 0), store>;
}

public tuple[Value, Store] eval((Expr)`<ID x>=<Expr e>`, Env[Location] env, Store store) {
	<v, store> = eval(e, env, store);
	l = lookup(x, env);
	
	store = put(l, v, store);
	
	return <v, store>;	
}

@doc{Evaluate a block statement. This will push a scope onto the environment,
and evaluate a list of statements within this block.}
public tuple[Env[Location], Store] eval((BlockStat)`{ <Stat* s> }`, Env[Location] env, Store store) {
	env = enterScope(env);
	for(Stat st <- s) {
		<env, store> = eval(st, env, store);
	}
	env = exitScope(env);
	return <env, store>;
}

@doc{Evaluate a statement which is a block statement.}
public tuple[Env[Location], Store] eval((Stat)`<BlockStat bs>`, Env[Location] env, Store store) {
	return eval(bs, env, store);
}

@doc{Evaluate a return statement.}
public tuple[Env[Location], Store] eval((Stat)`return <Expr e>;`, Env[Location] env, Store store) {
	<val, store> = eval(e, env, store);
	throw Result(val, store);
}

@doc{Evaluate an expression statement.}
public tuple[Env[Location], Store] eval((Stat)`<Expr e>;`, Env[Location] env, Store store) {
	<val, store> = eval(e, env, store);
	return <env, store>;
}

@doc{Evaluate a declaration with an initialiser.}
public tuple[Env[Location], Store] eval((Stat)`<SimpleTypeExpr typ> <ID id> = <Expr e>;`, Env[Location] env, Store store) {
	<l, store> = allocate(store);
	env = declare(id,  l, env);
	
	<val, store> = eval(e, env, store);
	store = put(l, val, store);
	
	return <env, store>;
}

@doc{Evaluate a pure declaration.}
public tuple[Env[Location], Store] eval((Stat)`<SimpleTypeExpr typ> <ID id>;`, Env[Location] env, Store store) {
	<l, store> = allocate(store);
	env = declare(id,  l, env);
	
	return <env, store>;
}

@doc{Evaluate a for loop. This will push a new scope onto the stack.}
public tuple[Env[Location], Store] eval((Stat)`for (<Expr init>, <Expr cond>, <Expr iter>) <Stat s>`, Env[Location] env, Store store) {
	env = enterScope(env);
	<val, store> = eval(init, env, store);
	<val, store> = eval(cond, env, store);
	while(val != Int(0)) {
		<env, store> = eval(s, env, store);
		<val, store> = eval(iter, env, store);
		<val, store> = eval(c, env, store);
	}
	env = exitScope(env);
	
	return <env, store>;
}

@doc{Evaluate a while loop. This will push a new scope onto the stack.}
public tuple[Env[Location], Store] eval((Stat)`while (<Expr c>) <Stat s>`, Env[Location] env, Store store) {
	env = enterScope(env);
	<val, store> = eval(c, env, store);
	while(val != Int(0)) {
		<env, store> = eval(s, env, store);
		<val, store> = eval(c, env, store);
	}
	env = exitScope(env);
	
	return <env, store>;
}

@doc{Evaluate a do-while loop. This will push a new scope onto the stack.}
public tuple[Env[Location], Store] eval((Stat)`do <Stat s> while (<Expr c>)`, Env[Location] env, Store store) {
	env = enterScope(env);
	val = Int(1);
	while(val != Int(0)) {
		<env, store> = eval(s, env, store);
		<val, store> = eval(c, env, store);
	}
	env = exitScope(env);
	
	return <env, store>;
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public tuple[Env[Location], Store] eval((Stat)`if (<Expr c>) <Stat s1> else <Stat s2>`, Env[Location] env, Store store) {
	env = enterScope(env);
	<cVal, store> = eval(c, env, store);
	
	if(cVal != Int(0)) {
		<env, store> = eval(s1, env, store);
	} else {
		<env, store> = eval(s2, env, store);
	}
	env = exitScope(env);
	return <env, store>;
}

@doc{'If' expression without an else block.}
public tuple[Env[Location], Store] eval((Stat)`if (<Expr c>) <Stat s1>`, Env[Location] env, Store store) {
	env = enterScope(env);
	<cVal, store> = eval(c, env, store);
	
	if(cVal != Int(0)) {
		<env, store> = eval(s1, env, store);
	}
	env = exitScope(env);
	return <env, store>;
}

public default tuple[Value, Store] eval(Tree e, Env[Location] env, Store store) {
	throw "Unknown expression \'<e>\':\n<[e]>";
}
