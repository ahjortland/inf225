module TestTypeChecker
import Simple;
import Value;
import TypeChecker;
import ParseTree;
import Environment;
import IO;

test bool Numbers() { return getType(check((Expr)`5`, newEnv(#Type))) == Int(); }

test bool Arith1() { return getType(check((Expr)`5+5`, newEnv(#Type))) == Int(); }
test bool Arith2() { return getType(check((Expr)`1+2*3`, newEnv(#Type))) == Int(); }
test bool Arith3() { return getType(check((Expr)`10-2`, newEnv(#Type))) == Int(); }
test bool Arith4() { return getType(check((Expr)`10/2`, newEnv(#Type))) == Int(); }
test bool Arith5() { return getType(check((Expr)`(1+2)*3`, newEnv(#Type))) == Int(); }
test bool Arith6() { return getType(check((Expr)`((10)/(2))`, newEnv(#Type))) == Int(); }



test bool If1() {
	check((Stat)`if (2*3 == 6) { 42; } else { 69; }`, [()]);
	return true;
}
test bool If2() {
	check((Stat)`if (6 == 5) 42; else 69;`, [()]);
	return true;
}
test bool If3() {
	check((Stat)`if (10 \< 5) { 1; } else { 0; }`, [()]);
	return true;
}

test bool IfFail1() {
	try {
		// condition should be int, not a function
		check((Program)`int f() { return 2; } int main(){ if (f) return 1; else return 2; }`);
	}
	catch str _: return true;
	return false;
}
test bool IfFail2() {
	try {
		// branch types must be the same
		check((Program)`int f() { return 2; } int main() { if (1) 1; else f; }`);
	}
	catch str _: return true;
	return false;
}

test bool AssignFail1() {
	try {
		// wrong type in assignment
		check((Program)`int f(int x) { str a = x; }`);
	}
	catch str _: return true;
	return false;
}

test bool Fun1() {
	<p, env> = check((Program)`int f(int x) { int a = 5; return f(x+a); }`);
	
	return lookup((ID)`f`, env) == Fun([Int()], Int());
}
			
test bool FailFun1() {
	try {
		// wrong return type
		check((Program)`str g() { return 5; }`); 
	}
	catch str _: return true;
	return false;
}
test bool FailFun2() {
	try {
		// doesn't return anything
		check((Program)`str g() { 5; }`);
	}
	catch str _: return true;
	return false;
}
test bool FailFun3() {
	try {
		// type error in body
		check((Program)`int f(str x) { return x + 5; }`); 
	}
	catch str _: return true;
	return false;
}
