module Simple
import List;

start syntax Program = Decl* decls;

keyword Keywords = "return" | "if" | "else" | "while" | "do" | "for" | "int" | "str";

syntax Decl
	= SimpleTypeExpr ID "(" {ParamDecl ","}* paramList ")" BlockStat // Function declaration
	| SimpleTypeExpr ID ";"                                         // Variable declaration
	| SimpleTypeExpr ID "=" Expr ";"                                // Variable declaration and initialization
	;
	 
syntax ParamDecl
	= TypeExpr paramType ID paramName
	;
	
syntax TypeExpr
	= SimpleTypeExpr
	// hand-code right-associativity of ->, since Rascal doesn't handle it automatically
	// when there's a list on the left-hand side
	| {SimpleTypeExpr ","}* "-\>" TypeExpr 
	;

syntax SimpleTypeExpr = "int" | "str" | "(" TypeExpr ")"
	;
	
syntax BlockStat = "{" Stat* "}"
	;

syntax Stat
	= BlockStat
	| "if" "(" Expr cond ")" Stat "else" Stat
	| "if" "(" Expr cond ")" Stat
	| "while" "(" Expr cond ")" Stat
	| "do" Stat "while" "(" Expr cond ")"
	| "for" "(" Expr init "," Expr cond "," Expr iter ")" Stat
	| SimpleTypeExpr varType ID varName ";"
	| SimpleTypeExpr varType ID varName "=" Expr ";"
	| "return" Expr ";" 
	| Expr ";"
	;
	
syntax Expr
	= ID                  // variables
	| NUM                 // integers
	| STRING              // Strings
	| ID "(" ArgList argList ")"     // function call
	| "(" Expr ")"        // parentheses
	> left (Expr "*" Expr | Expr "/" Expr )       // multiplication
	> left (Expr "+" Expr | Expr "-" Expr )       // addition
	> left (Expr "\>" Expr | Expr "\<" Expr | Expr "\>=" Expr | Expr "\<=" Expr | Expr "==" Expr ) // comparison
	> right ID var "=" Expr e
	;
	
syntax ArgList = {Expr ","}*;

// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< ([a-zA-Z_] [a-zA-Z_0-9]*) !>> [a-zA-Z_0-9] \ Keywords;


// numbers
lexical NUM = [0-9] !<< [0-9]+ !>> [0-9];

lexical STRING = "\"" !<< "\"" (![\"\\] | "\\\"" | "\\\\")* "\"" !>> "\"";

layout LAYOUT = (SPC | COM)* !>> [\ \t\n\r\f] !>> "//";

lexical COM
	= @category="Comment" "//" ![\n\r]* $
	;

lexical SPC = [\ \t\n\r\f];

public ArgList makeArgList(list[Expr] as) {
	return parse(#ArgList, intercalate(", ", as));
} 
