module TestImprovementB
import Simple;
import Value;
import ParseTree;
import Environment;
import IO;
import Runner;


test bool LambdaCall() {
	return run((Program)`( int x, int y ) =\> { x * y }(42, 69)`)
		== run((Program)`fun f(int x, int y) : int  = x * y; f(42, 69)`);
}

test bool LambdaCurry() {
	return run((Program)`fun mul(int x) : int -\> int = ( int y ) =\> { x * y }; mul(42)(69)`)
		== run((Program)`fun mul(int x, int y) : int  = x * y; mul(42, 69)`);
}

test bool Lambda3() {
	return run((Program)`fun thrice(int -\> int f, int x) : int = f(f(f(x))); thrice((int x) =\> { x*3 }, 3)`)
		== Int(81);
}

// This one will only work if you've used added recursive lets.
/*
test bool LambdaFib() {
	return run((Program)`let fib = (int n) =\> {if n \<2 then 1 else fib(n-1)+fib(n-2) end } in fib(10) end`);
}
*/

test bool Example1() {
	return run(parse(#start[Program], |project://inf225ex4b/src/Example1.B|).top)
		== Int(21);
}

// this one is tricky
test bool Example2() {
	return run(parse(#start[Program], |project://inf225ex4b/src/Example2.B|).top)
		== Int(18);
}
