module Evaluator
import Simple;
import String;
import ParseTree;
import IO;
import Environment;
import Value;
import Store;
import List;

alias Location = int;


@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public tuple[Env[Location], Store, Location] define((Decl)`fun <ID f>(<{ParamDecl ","}* paramList>) : <TypeExpr retType> = <Expr body>;`, Env[Location] env, Store store) {
	<l, store> = allocate(store);	
	env = declare(f,  l, env);
	
	store = put(l, Fun([p | p <- paramList], body, env), store);

	return <env, store, l>;
}

@doc{Evaluate a program.}
public tuple[Value, Store] eval((Program)`<Decl* decls><Expr e>`) {
	Store store = [];
	Env[Location] env = newEnv(#Location);
	list[Location] locs = [];
	for(Decl d <- decls) {
		<env, store, l> = define(d, env, store);
		locs += l;
	}
	
	// update stored functions so that all functions have the
	// same top-level environment
	for(l <- locs) {
		if(Fun(funParams, funBody, _) <- get(l, store))
			store = put(l, Fun(funParams, funBody, env), store);
	}
				
	<val, store> = eval(e, env, store);
	return <val, store>;
}

@doc{The value of an integer literal is the value of integer itself.}
public tuple[Value, Store] eval((Expr)`<NUM i>`, Env[Location] env, Store store) {
	return <Int(toInt(unparse(i))), store>;
}

@doc{The value of a variable is the value it's bound to in the environment.}
public tuple[Value, Store] eval((Expr)`<ID x>`, Env[Location] env, Store store) {
	l = lookup(x, env);
	return <get(l, store), store>;
}

public tuple[Value, Store] eval((Expr)`(<Expr e>)`, Env[Location] env, Store store) {
	return eval(e, env, store);
}

public tuple[Value, Store] eval ((Expr)`(<{ParamDecl ","}* paramList> ) =\> { <Expr body> }`, Env[Location] env, Store store) {
	return <Fun([p | p <- paramList], body, env), store>;
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in the function's definition
environment, with the argument is bound to the value.}
public tuple[Value, Store] eval((Expr)`<Expr f>(<{Expr ","}* argList>)`, Env[Location] env, Store store) {
	<fun, store> = eval(f, env, store);
	
	if(fun is Fun) {
		funEnv = enterScope(fun.env); // this has an effect: allow shadowing of variables
		// ([a | a <- argList] makes a list out of a comma-separated parsetree list)
		for(<param, arg> <- zip(fun.paramList, [a | a <- argList])) {
			// first, evaluate argument
			<argVal, store> = eval(arg, env, store); // evaluate in *original env*
			
			// then, allocate storage and declare parameter
			<l, store> = allocate(store);
			store = put(l, argVal, store);
			funEnv = declare(param.paramName, l, funEnv); // declare in funEnv
		}
		// evaluate body
		<v, store> = eval(fun.body, funEnv, store);
		funEnv = exitScope(funEnv);  // no effect here, since funEnv is discarded anyway
		return <v, store>;
	}
	else {
		throw "Expression should be a function";
	}
}

@doc{Parenthesis.}
public tuple[Value, Store] eval((Expr)`(<Expr e>)`, Env[Location] env, Store store) {
	return eval(e, env);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>*<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue * v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>+<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue + v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>-<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue - v2.intValue), store>;
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public tuple[Value, Store] eval((Expr)`<Expr e1>/<Expr e2>`, Env[Location] env, Store store) {
		<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue / v2.intValue), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>==<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue == v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\<<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue < v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\><Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue > v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\<=<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue <= v2.intValue ? 1 : 0), store>;
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public tuple[Value, Store] eval((Expr)`<Expr e1>\>=<Expr e2>`, Env[Location] env, Store store) {
	<v1, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	return <Int(v1.intValue >= v2.intValue ? 1 : 0), store>;
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public tuple[Value, Store] eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env[Location] env, Store store) {
	<cVal, store> = eval(c, env, store);
	if(cVal != Int(0))
		return eval(e1, env, store);
	else
		return eval(e2, env, store);
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public tuple[Value, Store] eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env[Location] env, Store store) {
	<l, store> = allocate(store);
	innerEnv = enterScope(env); // this has an effect: allow shadowing of variables
	
	innerEnv = declare(x,  l, innerEnv);
	<v, store> = eval(e1, innerEnv, store);
	store = put(l, v, store);
	
	<v2, store> = eval(e2, innerEnv, store);
	
	env = exitScope(innerEnv); // no effect here, since innerEnv is discarded anyway
	
	return <v2, store>;
}

public tuple[Value, Store] eval((Expr)`<ID x>=<Expr e>`, Env[Location] env, Store store) {
	<v, store> = eval(e, env, store);
	l = lookup(x, env);
	
	store = put(l, v, store);
	
	return <v, store>;	
}

public tuple[Value, Store] eval((Expr)`<Expr e1>;<Expr e2>`, Env[Location] env, Store store) {
	<_, store> = eval(e1, env, store);
	<v2, store> = eval(e2, env, store);
	
	return <v2, store>;
}

public default tuple[Value, Store] eval(Tree e, Env[Location] env, Store store) {
	throw "Unknown expression \'<e>\':\n<[e]>";
}
