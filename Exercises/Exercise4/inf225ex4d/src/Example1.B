// We encode 'pairs' as functions that select a field based
// on the argument (0 => x, 1 => y).
fun pair(int x, int y) : int -> int =
  fun int i => if i then y else x end;
  
// getters just call the pair function with the appropriate
// field selector (0 or 1) 
fun getX(int -> int p) : int = p(0);
fun getY(int -> int p) : int = p(1);

// setters return a new pair
fun setX(int -> int p, int x) : int -> int = pair(x, getY(p));
fun setY(int -> int p, int y) : int -> int = pair(getX(p), y);

// a simple test
getY(setY(pair(17,19),21))