module Store
import Value;
import List;

data Store = Store(list[Value] mem, list[Value] stack);

public Store newStore() {
	return Store([], []);
}

public tuple[int, Store] allocate(Store store) {
	location = size(store.mem);
	store.mem = store.mem + [Int(0)];
	return <location, store>;
}

public tuple[int, Store] push(Store store) {
	store.stack = [*store.stack, Int(0)];
	return <size(store.stack) * -1, store>;
}

public Store pop(Store store, int count) {
	if(count == 0) {
		return store;
	}
	store.stack = store.stack[..-count];
	return store;
}

public Store put(int location, Value val, Store store) {
	if(location < 0) {
		store.stack[(-1)-location] = val; // since the store is 0 indexed, the stack is "1" indexed by negative numbers
	} else {
		store.mem[location] = val;
	}
	return store;
}

public Value get(int location, Store store) {
	if(location < 0) {
		return store.stack[(-1)-location];
	} else {
		return store.mem[location];
	}
}
