module TestLocalStack
import Evaluator;
import Simple;
import Store;
import List;

test bool BasicTest1() {
	Store s = eval((Program)`let x = 2 in x + x end`)[1];
	return size(s.mem) == 0 && size(s.stack) == 0;
}

test bool BasicTest2() {
	Store s = eval((Program)`fun f(int x, int y) : int = x+y; let x = 2 in f(x, 2) + x end`)[1];
	return size(s.mem) == 1 && size(s.stack) == 0;
}

test bool BasicTest3() {
	Store s = eval((Program)`fun g(int x) : int = x*x; fun f(int x, int y) : int = x+y; let x = 2 in f(x, 2) + g(x) end`)[1];
	return size(s.mem) == 2 && size(s.stack) == 0;
}
