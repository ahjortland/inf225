#!/bin/bash
if [ "${1,,}" == "arm" ]; then
	arm=true
	session=arm
else
	arm=false
	session=vhdl
fi

if tmux has-session -t "$session"; then
	tmux attach -t "$session" &>/dev/null
	exit 0
fi

#common variables
dir=$(dirname $(realpath $0))
ast="$dir/ast.tree"
srcDir="$dir/src"
src="$dir/loadlarge.cmin"
svg="$dir/ast.svg"
phpsyntaxtree="$dir/../phpsyntaxtree"

outlog="$dir/output.log"
errorlog="$dir/error.log"

#arm specific variables
arm="$dir/raspberry"
armhost="raspberry"
hostpath="assembly"

function watchloop {
	FILE="$1"
	RUNFIRST="$2"
	COMMAND="$3"
	shift 3
	if [ $RUNFIRST = true ]; then
		$COMMAND "$@"
	fi
	while true; do
		inotifywait -r -e close_write,moved_to,create "$FILE" &> /dev/null
		printf " \n"
		echo "--------------------------------------------------------------------------------"
		printf " \n"
		$COMMAND "$@"
	done
}

function prettyVHDL {
	vim -n -c "set ft=vhdl|set et" -c "normal! gg=G" -c "sav! $1-indent.vhd|q" "$1" #&> /dev/null
	clear
	highlight -s molokai -S vhd -O xterm256 "$1-indent.vhd" | cat -n
}

function prettyASM {
	clear
	highlight -S asm -O xterm256 "$1" | cat -n
}

NUM=1
function runVHDL {
	LOGCOLOR="setaf 3"
	ANALYZECOLOR="sgr0"
	ELABORATECOLOR=$ANALYZECOLOR
	RUNCOLOR=$ANALYZECOLOR
	if [ -d vhdl ]; then
		rm -r vhdl/
	fi
	mkdir -p vhdl/
	cp "$1" "vhdl/"
	pushd vhdl &> /dev/null
	clear
	echo "$(tput $LOGCOLOR)Running vhdl"
	echo " "
	echo "Analyzing:"
	echo "$(tput $ANALYZECOLOR)"
	ghdl -a "$1"
	echo "$(tput $LOGCOLOR)"
	echo "--------------------------------------------------------------------------------"
	echo "Elaborating:"
	echo "$(tput $ELABORATECOLOR)"
	ghdl -e main
	echo "$(tput $LOGCOLOR)"
	echo "--------------------------------------------------------------------------------"
	echo "Running:"
	echo "$(tput $RUNCOLOR)"
	ghdl -r main
	echo "$(tput $LOGCOLOR)"
	echo "--------------------------------------------------------------------------------"
	echo "Done!"
	echo "$(tput sgr0)"
	echo 
	popd &> /dev/null

}

function runASM {
	ssh $1 "bash -i -c \"
			export TERM=xterm
			cd assembly
			while true; do
				inotifywait -e close_write,moded_to,create '$2.asm' 2>/dev/null
				clear
				make '$2.out'
				./$2.out
				echo $?
			done
		\"
	"
}

function prettyCMin {
	clear
	cpp -P $1 | indent -st -bap -bli0 -i4 -l79 -ncs -brf -brs -br -npcs -npsl -fca -lc79 -fc1 -ts4 - | highlight -s molokai -S c -O xterm256 | cat -n
}

function ASTtoSVG {
	pushd "$1" &>/dev/null
	printf "Creating new syntax tree svg.."
	php main.php "$(cat "$2")" > "$3"; 
	printf ". Done\n"
	popd &> /dev/null
}

function compileCMIN {
	ant compile
	java -classpath "bin/:lib/*" "entry.Main" -v -s -l "$1" -a "$2" "$3" "$4" 1> "$5" 2> "$6"
}

function watchLog {
	NUMBERCOLOR=$2
	TEXTCOLOR=$3

	INDEX=1
	IFS=$'\n';
	clear; echo
	for i in $(cat "$1"); do 
		printf " %s%4d	%s%s%s\n" "$(tput setaf $NUMBERCOLOR)" $INDEX "$(tput setaf $TEXTCOLOR)" "$i" "$(tput setaf 7)"; 
		INDEX=$(( $INDEX + 1 ))
	done
}

export -f watchloop
export -f prettyVHDL
export -f prettyCMin
export -f ASTtoSVG
export -f compileCMIN
export -f watchLog
export -f prettyASM
export -f runASM
export -f runVHDL

tmux new -d -s     "$session"    "bash -i -c 'watchloop \"$ast\" true ASTtoSVG \"$phpsyntaxtree\" \"$ast\" \"$svg\"'"
if [ $arm = true ]; then
	output="test"

	mountpoint "$arm" &> /dev/null
	if [ $? -eq 1 ]; then
		rm -rf "$arm"
		mkdir -p "$arm"
		sshfs "$armhost":"$hostpath" "$arm"
	fi

	tmux split  -t     "$session" -h "bash -i -c 'watchloop \"$srcDir\" true compileCMIN \"arm\" \"$ast\" \"$src\" \"$arm/$output.asm\" \"$outlog\" \"$errorlog\"'"
	tmux split  -t     "$session" -h "bash -i -c 'watchloop \"$src\"    false compileCMIN \"arm\" \"$ast\" \"$src\" \"$arm/$output.asm\" \"$outlog\" \"$errorlog\"'"
	tmux new-window -t "$session"    "bash -i -c 'watchloop \"$arm/$output.asm\" true prettyASM \"$arm/$output.asm\"'"
else
	output="$dir/loadlarge.vhdl"
	tmux split  -t     "$session" -h "bash -i -c 'watchloop \"$srcDir\" true compileCMIN \"vhdl\" \"$ast\" \"$src\" \"$output\" \"$outlog\" \"$errorlog\"'"
	tmux split  -t     "$session" -h "bash -i -c 'watchloop \"$src\"    false compileCMIN \"vhdl\" \"$ast\" \"$src\" \"$output\" \"$outlog\" \"$errorlog\"'"
	tmux new-window -t "$session"    "bash -i -c 'watchloop \"$output\" true prettyVHDL \"$output\"'"
fi

tmux split  -t     "$session" -h "bash -i -c 'watchloop \"$src\" true prettyCMin \"$src\"'"

tmux setw -t "$session" main-pane-width 88 &>/dev/null
tmux select-layout main-vertical &>/dev/null

if [ $arm = true ]; then
	tmux split  -t     "$session" -h "bash -i -c 'runASM \"$armhost\" \"$output\"'"
else
	tmux split  -t     "$session" -h "bash -i -c 'watchloop \"$output\" true runVHDL \"$output\"'"
fi

tmux split  -t     "$session" -v "bash -i -c 'watchloop \"$errorlog\" true watchLog \"$errorlog\" 7 1'"
tmux select-pane -L -t "$session" 
tmux split  -t     "$session" -v "bash -i -c 'watchloop \"$outlog\" true watchLog \"$outlog\" 7 2'"

# Set focus on the running program
tmux select-pane -R -t "$session"
tmux select-pane -U -t "$session"

tmux attach -t     "$session" &>/dev/null

