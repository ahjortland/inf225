%        File: Project_description.tex
%     Created: on. okt. 30 11:00  2013 C
% Last Change: on. okt. 30 11:00  2013 C
%
\documentclass[a4paper]{article}

% Use utf-8 encoding for foreign characters
\usepackage[utf8]{inputenc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

\usepackage{listings}
\usepackage{color}
\usepackage{appendix}
\usepackage{hyperref}
\usepackage[stable]{footmisc}
\usepackage{syntax}
 
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{lgray}{rgb}{0.95,0.95,0.95}
\definecolor{mauve}{rgb}{0.58,0,0.82}
 
\lstset{%
  language=Java,                  % the language of the code
  basicstyle=\footnotesize,       % the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
  stepnumber=1,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{lgray},  % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  title=\lstname,                 % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},      % keyword style
  commentstyle=\color{dkgreen},   % comment style
  stringstyle=\color{mauve},      % string literal style
  escapeinside={\%*}{*)},         % if you want to add LaTeX within your code
  morekeywords={*,..}             % if you want to add more keywords to the set
}

% More symbols
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{latexsym}
\usepackage{stmaryrd}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% Setup for fullpage use
%\usepackage{fullpage}

% Surround parts of graphics with box
%\usepackage{boxedminipage}

% If you want to generate a toc for each chapter (use with book)
%\usepackage{minitoc}

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi
\title{The C- compiler (with rewriting rules) \\ Project description - INF225 term
project}
\author{Andreas Hjortland}
%\author{0}
\date{\today}
%\date{2013-10-30}

\begin{document}

\ifpdf
\DeclareGraphicsExtensions{.pdf,.jpg,.tif}
\else
\DeclareGraphicsExtensions{.eps,.jpg}
\fi

\maketitle
\section{Introduction}
This project will try to implement a complete compiler for the language \texttt{C-} which
Kenneth C. Louden describes in his book Compiler Construction - Principles and
Practice~[\cite{compiler}]. The language will also have a couple of
modifications (see appendix~\ref{bnf}). Most notably it will have support for basic I/O,
strings and a construct for the programmer to create rewriting rules in the
language. I will also try to create a code generation backend which will output
ARM11 assembly for the Raspberry Pi running linux.

This will all be done within regular Java using the standard library with one
exception. The reason for to do this in regular Java is that I believe that it
is not possible appreciate how much work a tool actually do for the programmer
until I have tried to do the job manually at least once. The only library or
tool I use beyond the standard  library is the Nuthatch~[\cite{nuthatchery}]
library for creating trees and rewrite rules.

The goal of this project is first to try implementing a complete compiler from
scratch. This is because I want to learn how the whole compiler chain works from
abstract code to machine code. Secondly I want to learn how to do rewriting and
transformations on the AST which is why there will be support to create
rewriting rules in the language.

\section{Development approach}
Since this is a rather large project, I will do it in several smaller
incrementations. This will ensure that the project will have at least some
functionality even if I am not able to complete the whole project before the
deadline.

The project will be split into the following phases:
\begin{enumerate}
	\item Create a scanner and parser which will output an AST for a subset of
		the grammar.\footnote{I am almost done with this part}
	\item Create the type checker
	\item Create the code generator (if this gets to hard, I will just create an
		evaluator, or a JVM code generator instead).
	\item Implement tree transformations based on the \texttt{rewrite} keyword.
	\item Extend the grammar to include strings, I/O and perhaps modules
	\item Extend the code generator to acommodate for these changes.
\end{enumerate}

If I manage to do all this with time to spare, I am also interested in
bootstrapping the compiler. However I believe that compiler bootstrapping will
be way beyond the intended scope of this project, so it will probably not be
realized.
\begin{appendices}
	\section{Tentative BNF for the language\footnote{There might come some
	changes in the language as I am developing it.}}
	\label{bnf}
	All the non-terminals in the grammar will be denoted $<identifier>$. The
	literal terminals will be denoted \texttt{literal}, whilst the terminals
	matching a regular expression will be denoted \texttt{/regexp/}. All
	whitespace between literals will be ignored.

	Note that the keyword \texttt{read} is an expression (in that it takes a
	string from stdin and returns a value), whilst \texttt{print} is a statement
	since it will only print an expression on stdout.

	\newdimen\grammarindent
		\grammarindent 13em
	\begin{grammar}
		<program> ::= <declaration-list>

		<declaration-list> ::= <declaration> <declaration-list>
		\alt <declaration>

		<declaration> ::= <variable-declaration>
		\alt <function-declaration>

		<variable-declaration> ::= <type-specifier> <variable> ";"
		\alt "str" <variable> "=" <string> ";"
		\alt "int" <variable> "=" <number> ";"
		\alt "bool" <variable> "=" <bool> ";"

		<type-specifier> ::= "int"
		\alt "bool"
		\alt "str"
		\alt "void"

		<variable> ::= <identifier>
		\alt <identifier> <index>

		<identifier> ::= "/[\_a-zA-Z][\_a-zA-Z0-9]*/"

		<index> ::= "[" <number> "]"

		<number> ::= "/[0-9]+/"

		<string> ::= "/\"[^\"]*\"/"

		<bool> ::= "/(true|false)/"

		<function-declaration> ::= <type-specifier> <identifier> "(" <params>
		")" <compound-statement>

		<params> ::= <param-list>
		\alt "void"
		\alt $\varepsilon$

		<param-list> ::= <param> "," <param-list>
		\alt <param> 

		<param> ::= <type-specifier> <identifier>
		\alt <type-specifier> <identifier> "[" "]"

		<compound-statement> ::= "{" <statement-list> "}"

		<statement-list> ::= <statement> <statement-list>
		\alt $\varepsilon$

		<statement> ::= <variable-declaration>
		\alt ";"
		\alt <compound-statement>
		\alt <if-statement>
		\alt <while-statement>
		\alt <expression> ";"
		\alt "return" <expression> ";"
		\alt "rewrite" <statement> "->" <statement>
		\alt "print" "(" <expression> ")" ";"

		<if-statement> ::= "if" "(" <expression> ")" <statement> "else" <statement>
		\alt "if" "(" <expression> ")" <statement>

		<while-statement> ::= "while" "(" <expression> ")" <statement>

		<expression> ::= <arithmetic-expression>

		<arithmetic-expression> ::= <add-expression> "==" <add-expression>
		\alt <add-expression> "!=" <add-expression>
		\alt <add-expression> "<=" <add-expression>
		\alt <add-expression> "<"  <add-expression>
		\alt <add-expression> "\>=" <add-expression>
		\alt <add-expression> "\>" <add-expression>
		\alt <add-expression>

		<add-expression> ::= <add-expression> "+" <mul-expression>
		\alt <add-expression> "-" <mul-expression>
		\alt <mul-expression>

		<mul-expression> ::= <mul-expression> "*" <variable-expression>
		\alt <mul-expression> "/" <variable-expression>
		\alt <mul-expression> "\%" <variable-expression>
		\alt <variable-expression>

		<variable-expression> ::= <function-call>
		\alt "read" "(" <type-specifier> ")"
		\alt <variable>
		\alt <variable> "=" <expression>
		\alt <factor>

		<function-call> ::= <identifier> "(" <args> ")"

		<args> ::= <arg\_list>
		\alt $\varepsilon$

		<arg-list> ::= <expression> "," <arg-list>
		\alt <expression>


		<factor> ::= "(" <expression> ")"
		\alt <number>
		\alt <string>
		\alt <bool>
	\end{grammar}

\end{appendices}

\begin{thebibliography}{9}
	\bibitem{compiler}
		Louden, Kenneth C.,
		\emph{Compiler Construction: Principles and Practice},
		0534939724,
		PWS Publishing Co.,
		Boston, MA, USA,
		1997.
	\bibitem{nuthatchery}
		\url{http://nuthatchery.org/}
\end{thebibliography}

\end{document}
