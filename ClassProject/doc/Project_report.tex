%        File: Project_description.tex
%     Created: on. okt. 30 11:00  2013 C
% Last Change: on. okt. 30 11:00  2013 C
%
\documentclass[a4paper]{article}

% Use utf-8 encoding for foreign characters
\usepackage[utf8]{inputenc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

\usepackage{listings}
\usepackage{color}
\usepackage{appendix}
\usepackage{hyperref}
\usepackage[stable]{footmisc}
\usepackage{syntax}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{lgray}{rgb}{0.95,0.95,0.95}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{%
	language=Java,                  % the language of the code
	basicstyle=\footnotesize,       % the size of the fonts that are used for the code
	numbers=left,                   % where to put the line-numbers
	numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
	stepnumber=1,                   % the step between two line-numbers. If it's 1, each line 
								  % will be numbered
	numbersep=5pt,                  % how far the line-numbers are from the code
	backgroundcolor=\color{lgray},  % choose the background color. You must add \usepackage{color}
	showspaces=false,               % show spaces adding particular underscores
	showstringspaces=false,         % underline spaces within strings
	showtabs=false,                 % show tabs within strings adding particular underscores
	frame=single,                   % adds a frame around the code
	rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	tabsize=2,                      % sets default tabsize to 2 spaces
	captionpos=b,                   % sets the caption-position to bottom
	breaklines=true,                % sets automatic line breaking
	breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
	title=\lstname,                 % show the filename of files included with \lstinputlisting;
								  % also try caption instead of title
	keywordstyle=\color{blue},      % keyword style
	commentstyle=\color{dkgreen},   % comment style
	stringstyle=\color{mauve},      % string literal style
	escapeinside={\%*}{*)},         % if you want to add LaTeX within your code
	morekeywords={*,..}             % if you want to add more keywords to the set
}

% More symbols
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{latexsym}
\usepackage{stmaryrd}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% Setup for fullpage use
\usepackage{fullpage}

% Surround parts of graphics with box
%\usepackage{boxedminipage}

% If you want to generate a toc for each chapter (use with book)
%\usepackage{minitoc}

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi
\title{The C- compiler \\
Project report - INF225 term project}
\author{Andreas Hjortland}
%\author{0}
\date{\today}
%\date{2013-10-30}

\begin{document}

\ifpdf
\DeclareGraphicsExtensions{.pdf,.jpg,.tif}
\else
\DeclareGraphicsExtensions{.eps,.jpg}
\fi

\maketitle
\section{The vision}
The vision of this project was to create a complete compiler for a language to
learn how the whole process from grammar to running assembly code worked. The
reason I wanted to do this was that the focus of the course was more on the
tools than how they worked behind the scenes, and I wanted to see how it was
done manually, both to learn how it is done manually, but also to better
understand how the tools works and how much they help me as a language
developer.

\section{The plan}
The plan was to implement a compiler for a superset of the language \texttt{C-}
which Kenneth C. Louden describes in his book Compiler Construction - Principles
and Practice~[\cite{compiler}]. The additional features in this version of the
language is that it can declare functions which is not available at
compile-time, but rather at linking time. This feature give the language access
to the complete C-library installed on the computer. It also means that one can
link multiple files which can be used as a custom library. The language also has
support for strings and a construct which will enable the programmer to create
custom rewrite rules within the language. Since I wanted to create a complete
compiler, I created a code backend which outputs ARM11 assembly. More
specifically for linux running on the Raspberry Pi. It is possible that the
compiler it works on other operating systems and other ARM cpus, but that
haven't been tested.

Since this is a rather large project, I did it in several smaller
incrementations. The reason was that it would ensure that I would have at least
something to show for, if I was not able to complete the whole project before
the deadline. 

\begin{enumerate}
	\item Create a scanner and parser which will output an AST for a subset of
		the grammar.
	\item Create the type checker
	\item Create the code generator (if this gets to hard, I will just create an
		evaluator, or a JVM code generator instead).
	\item Implement tree transformations based on the \texttt{rewrite} keyword.
	\item Extend the grammar to include strings, I/O and perhaps modules
	\item Extend the code generator to acommodate for these changes.
	\item If I have time to spare, maybe try to bootstrap the compiler.
\end{enumerate}

I didn't stick completely to the plan. I did not extend the language to account
for modules, and I rely on calling C-functions to do I/O. I also created the
grammar in one iteration instead of going back to it after I had a rudimentary
code generator working. 

\section{The technologies}
I almost didn't use any technologies aside from Java with the standard library
and a tree modification library named Nuthatch~\cite{nuthatchery}. The reason
for this is that I wanted to learn how it is done manually, but also to
understand how the tools will help a language developer when creating a new
language.

If we also take into account the developer tools I used, the most common was the
GNU debugger (gdb), which I used extensively when developing the code backend.

\section{The implementation}
The scanner uses the built in Java regex support and parses the tokens, choosing
the longest token if there is an ambiguity. In the first iteration it had
support for saving positions and going back to them since the first parser had
to backtrack since it didn't have any lookahead, but that functionality was
removed when I rewrote the parser.

The parser was first implemented as a naïve LL parser with no lookahead which
utilized backtracking when it got an error. This was a slow process
($O(n * m)$, where $n$ is the size of the grammar and $m$ is the size of the
program), which could take up to 8 seconds to parse the ``Hello, world!''
application on a modern 4 core cpu (Intel I7). Since that obviously didn't work
I revisited my parser when I had everything but the code generator working, and
reimplemented it as a recursive descent parser. This gave me two immediate
benefits. Firstly it gave my compiler a way better performance. Now it even
performed better than the java compiler on the raspberry pi. Secondly I managed
to create better error messages for syntax errors in the code.

The tree rewriter is rather simple. I wanted to do more with it, but ran out of
time. All it does is to walk the complete AST and try to match every subtree
with the tree generated by the first operand of the rewrite construction. It it
manages to match it will replace the subtree and continue the walk. It will not
walk a subtree which already has been replaced.

The type checker will first add every global variable to an environment, and
then it will type-check the program utilizing a bottom-up strategy. This is a
rather plain implementation which there isn't much to say.

Since we haven't done much in terms of compiler backend and code-generation in
the course, this was the hardest part of the project. I am pretty sure that this
is riddled with bad practices and antipatterns, but it does work, and it outputs
code which does what it is supposed to do (most of the time at least). The
algorithm in the code generator is a simple top-down, left-right walk of the
AST where it will collect all the assembly in a \texttt{StringBuilder} as it
walks. It abuses the stack extensively, but the programs seems to be sufficiently
effective. The performance for my test programs have been almost up to par with
programs compiled using GCC with no optimizations.

\section{How far did I get?}
I will say that I almost completed all the initial goals of my project. The only
parts I didn't manage to do at all was the compiler bootstrapping, but that was
something I didn't really thought I would have time to do anyway. I would've
liked to be able to extend the rewrite construction with parameters which would
be subtrees, so that I could do rewrites like \texttt{if(true) x -> x} which
would just remove the \texttt{if(true)} statement, but keep the inner statement,
but that would have required me to either fix the grammar, or do something with
the parser which would accomadate for something like that. Neither of those was
something I think I would've been able to finish a week before the project
deadline.

\section{My experiences}
In my experience, I learned a lot of the more gritty details of compiler
construction than what was taught in the course. I also learned that doing
this without the help of tools are a very tedious and somewhat hard task. Even
so, it was an interesting project which taught me quite a bit about how a
compiler works, and that I will never create a compiler manually again.


\clearpage
\begin{appendices}
	\section{Grammar of the language}
	\label{bnf}
	All the non-terminals in the grammar will be denoted $<identifier>$. The
	literal terminals will be denoted \texttt{literal}, whilst the terminals
	matching a regular expression will be denoted \texttt{/regexp/}. All
	whitespace between literals will be ignored.

	Note that the keyword \texttt{read} is an expression (in that it takes a
	string from stdin and returns a value), whilst \texttt{print} is a statement
	since it will only print an expression on stdout.

	Note that it might be minor differences between the grammar in this report
	and the implementation. This was to make it easier to convert the parse tree
	into an abstract syntax tree after parsing.

	\newdimen\grammarindent
	\grammarindent 13em
	\begin{grammar}
		<program> ::= <declaration-list>

		<declaration-list> ::= <declaration> <declaration-list>
		\alt <declaration>

		<declaration> ::= <variable-declaration>
		\alt <function-declaration>
		\alt <rewrite-rule>

		<variable-declaration> ::= <type-specifier> <variable> ";"
		\alt <type-specifier> <assign> ";"

		<type-specifier> ::= "int"
		\alt "bool"
		\alt "str"
		\alt "void"

		<variable> ::= <identifier>
		\alt <identifier> <array>

		<identifier> ::= "/[\_a-zA-Z][\_a-zA-Z0-9]*/"

		<array> ::= "[" <expression> "]"

		<assign> ::= <variable> "=" <expression>

		<number> ::= "/-?/" "/[0-9]+/"

		<string> ::= "/\"[^\"]*\"/"

		<bool> ::= "/(true|false)/"

		<function-declaration> ::= <type-specifier> <identifier> "(" <params>
		")" <compound-statement>
		\alt <type-specifier> <identifier> "(" <params> ")" ";"

		<params> ::= <param-list>
		\alt "void"
		\alt <var-args>
		\alt $\varepsilon$

		<param-list> ::= <param> "," <param-list>
		\alt <param> 

		<param> ::= <type-specifier> <identifier>
		\alt <type-specifier> <identifier> "[" "]"

		<var-args> ::= "\dots"

		<compound-statement> ::= "{" <statement-list> "}"

		<statement-list> ::= <statement> <statement-list>
		\alt $\varepsilon$

		<statement> ::= <variable-declaration>
		\alt ";"
		\alt <compound-statement>
		\alt <if-statement>
		\alt <while-statement>
		\alt <expression> ";"
		\alt "return" <expression> ";"
		\alt "print" "(" <expression> ")" ";"

		<if-statement> ::= "if" "(" <expression> ")" <statement> "else" <statement>
		\alt "if" "(" <expression> ")" <statement>

		<while-statement> ::= "while" "(" <expression> ")" <statement>

		<expression> ::= <arithmetic-expression>

		<arithmetic-expression> ::= <add-expression> "==" <add-expression>
		\alt <add-expression> "!=" <add-expression>
		\alt <add-expression> "<=" <add-expression>
		\alt <add-expression> "<"  <add-expression>
		\alt <add-expression> "\>=" <add-expression>
		\alt <add-expression> "\>" <add-expression>
		\alt <add-expression>

		<add-expression> ::= <mul-expression> "+" <add-expression>
		\alt <add-expression> "-" <mul-expression>
		\alt <mul-expression>

		<mul-expression> ::= <variable-expression> "*" <mul-expression>
		\alt <mul-expression> "/" <variable-expression>
		\alt <mul-expression> "\%" <variable-expression>
		\alt <variable-expression>

		<variable-expression> ::= <function-call>
		\alt "read" "(" <type-specifier> ")"
		\alt <variable>
		\alt <variable> "=" <expression>
		\alt <factor>

		<function-call> ::= <identifier> "(" <args> ")"

		<args> ::= <arg\_list>
		\alt $\varepsilon$

		<arg-list> ::= <expression> "," <arg-list>
		\alt <expression>


		<factor> ::= "(" <expression> ")"
		\alt <number>
		\alt <string>
		\alt <bool>

		<rewrite-rule> ::= "rewrite" <statement> "->" <statement>
	\end{grammar}

\end{appendices}

\begin{thebibliography}{9}
	\bibitem{compiler}
		Louden, Kenneth C.,
		\emph{Compiler Construction: Principles and Practice},
		0534939724,
		PWS Publishing Co.,
		Boston, MA, USA,
		1997.
	\bibitem{nuthatchery}
		\url{http://nuthatchery.org/}
\end{thebibliography}

\end{document}
