public class tester {
	public static int globalScratch;
	public static int[] globalArray = new int[3];
	public static int abc;

	public static void output(int i) {
		System.out.println(i);
	}

	public static void testCall() {
		output(2);
	}

	public static void mathTest() {
		output(1 + 2);
		output(5 - 1);
		output(20 / 4);
		output(2 * 3);
	}

	public static void setGlobal() {
		globalScratch = 7;
	}

	public static void outputGlobal() {
		output(globalScratch);
	}

	public static void outputLocal() {
		int n;
		n = 8;
		output(n);
	}

	public static void testWhile() {
		int i;
		i = 9;
		while (i < 13) {
			output(i);
			i = i + 1;
		}
	}

	public static void testIf() {
		int i;
		i = 13;
		while (i < 15) {
			if (i == 13) {
				output(i);
			} else {
				output(i);
			}

			if (i == 14)
				output(15);

			i = i + 1;
		}

		if (true) {
			output(16);
		}
	}

	public static void testCompare() {
		//output(17 + (5 < 4));
		//output(17 + (4 < 5));

		if (3 <= 4) output(19);
		if (4 <= 4) output(20);
		if (5 <= 4) ; else output(21);

		if (4 >= 3) output(22);
		if (4 >= 4) output(23);
		if (4 >= 5) ; else output(24);

		if (4 > 3) output(25);
		if (4 > 4) ; else output(26);
		if (4 > 5) ; else output(27);

		if (4 == 4) output(28);
		if (5 == 4) ; else output(29);

		if (4 != 5) output(30);
		if (4 != 4) ; else output(31);

	}

	public static int testReturn() {
		return 32;
	}

	public static void testCallParam(int a, int b, int c) {
		output(a);
		output(b);
		output(c);
	}

	public static void setGlobalArray() {
		int i;
		i = 0;

		while (i <= 2) {
			globalArray[i] = i + 36;
			i = i + 1;
		}
	}

	public static void outputGlobalArray() {
		int i;
		i = 0;

		while (i <= 2) {
			output(globalArray[i]);
			i = i + 1;
		}
	}

	public static void testLocalArray() {
		int i;
		int[] array = new int[3];

		i = 0;
		while (i <= 2) {
			array[i] = i + 39;
			i = i + 1;
		}

		i = 0;
		while (i <= 2) {
			output(array[i]);
			i = i + 1;
		}
	}

	public static void testLocalPassingFill(int array[]) {
		int i;

		i = 0;
		while (i <= 2) {
			array[i] = i + 42;
			i = i + 1;
		}
	}

	public static void testLocalPassing() {
		int i;
		int[] array = new int[3];

		testLocalPassingFill(array);

		i = 0;
		while (i <= 2) {
			output(array[i]);
			i = i + 1;
		}
	}

	public static void doRecursion(int currentDepth, int bottom) {
		int i;
		if (currentDepth == bottom)
			globalScratch = globalScratch + 1;
		else {
			i = 0;

			while (i < 3) {
				doRecursion(currentDepth + 1, bottom);
				i = i + 1;
			}
		}
	}

	public static void testRecursion() {
		globalScratch = 0;
		doRecursion(0, 4);
		if (globalScratch == 81) output(45);
	}

	/* Generates a shadowed function param warning, this is OK */
	/*public static void testShadowing(int abc) {
		int abc;
		abc = 47;

		{
			int abc;
			abc = 46;
			output(abc);
		}

		output(abc);
	}*/

	public static void testEmptyExpr() {
		int i;
		i = 0;

		while (i < 100000) {
			testReturn();

			i = i + 1;
		}

		output(49);
	}

	public static void testAdvancedExpressions() {
		int a;
		int b;
		int c;
		int[] d = new int[10];
		int[] e = new int[10];

		a = b = c = 50;
		output(a);

		d[2] = 5;
		d[d[1+1]] = 7;
		d[1] = d[5] * d[2];
		output(d[5/5] + 16);

		e = d;
		output(e[5/5] + 17);

	}

	public static void testEmptyReturn() {
		return;
	}

	public static void main(String[] args) {
		/* Test 1: output */
		output(1);

		/* Test 2: void function call */
		testCall();

		/* Test 3-6: math operators */
		mathTest();

		/* Test 7: global variable */
		setGlobal();
		outputGlobal();

		/* Test 8: local variable */
		outputLocal();

		/* Test 9-12: while() */
		testWhile();

		/* Test 13-16: if() */
		testIf();

		/* Test 17-31: compare operators */
		testCompare();

		/* Test 32: return statement */
		output(testReturn());

		/* Test 33-35: function call params */
		testCallParam(33, 34, 35);

		/* Test 36-38: global array */
		setGlobalArray();
		outputGlobalArray();

		/* Test 39-41: local array */
		testLocalArray();

		/* Test 42-44: local array passing */
		testLocalPassing();

		/* Test 45: recursion */
		testRecursion();

		/* Test 46-48: variable shadowing */
		abc = 48;
		//testShadowing(9999);
		output(abc);

		/* Test 49: unconsumed expressions, possible stack overflow */
		testEmptyExpr();

		/* Test 50-52: various advanced expressions */
		testAdvancedExpressions();

		/* Test 53: Empty return bug test */
		testEmptyReturn();
		output(53);

		System.exit(0);
	}
}
