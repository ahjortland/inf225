public class fib {
	public static void main(String[] args) {
		int i = 40;
		System.out.printf("Fibonacci number of %d is %d\n", i, fib(i));
	}

	private static int fib(int i) {
		if(i <= 2) {
			return i;
		}
		return fib(i-1) + fib(i-2);
	}
}
