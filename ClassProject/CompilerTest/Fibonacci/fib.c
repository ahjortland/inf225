#include<stdio.h>

int main() {
	int i = 40;
	printf("Fibonacci number of %d is %d\n", i, fib(i));
	return i;
}

int fib(int i) {
	if(i <= 2) {
		return i;
	}
	return fib(i-1) + fib(i-2);
}
