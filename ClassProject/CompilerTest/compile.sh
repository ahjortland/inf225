#!/bin/sh

for i in *.c; do
	echo "gcc '$i'"
	time gcc "$i"
done

for i in *.java; do
	echo "javac '$i'"
	time javac "$i"
done

for i in *.cmin; do
	echo "cmin '$i' $i.out"
	time cmin "$i" "$i.out"
done
