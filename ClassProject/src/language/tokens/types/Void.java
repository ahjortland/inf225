package language.tokens.types;

import language.TypeChecker.Type;

public class Void extends TypeToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Void.class);
	}

	@Override
	public Type getType() {
		return new language.TypeChecker.Type.Void(this.start);
	}
}
