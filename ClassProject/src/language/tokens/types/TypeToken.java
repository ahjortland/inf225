package language.tokens.types;

import frontend.tokens.Token;

public abstract class TypeToken extends Token {

	@Override
	public boolean showInAST() {
		return true;
	}

}
