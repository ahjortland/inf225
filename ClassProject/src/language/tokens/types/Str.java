package language.tokens.types;

import language.TypeChecker.Type;

public class Str extends TypeToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Str.class);
	}

	@Override
	public Type getType() {
		return new language.TypeChecker.Type.Str(this.start);
	}
}
