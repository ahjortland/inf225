package language.tokens.types;

import language.TypeChecker.Type;

public class Bool extends TypeToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Bool.class);
	}

	@Override
	public Type getType() {
		return new language.TypeChecker.Type.Bool(this.start);
	}
}
