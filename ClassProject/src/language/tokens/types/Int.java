package language.tokens.types;

import language.TypeChecker.Type;

public class Int extends TypeToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Int.class);
	}

	@Override
	public Type getType() {
		return new language.TypeChecker.Type.Int(this.start);
	}
}
