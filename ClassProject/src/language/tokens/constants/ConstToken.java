package language.tokens.constants;

import frontend.tokens.Token;

public class ConstToken extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(ConstToken.class);
	}

	@Override
	public boolean showInAST() {
		return true;
	}
}
