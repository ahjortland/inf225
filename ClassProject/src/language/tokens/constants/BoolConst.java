package language.tokens.constants;

public class BoolConst extends ConstToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(BoolConst.class);
	}
}
