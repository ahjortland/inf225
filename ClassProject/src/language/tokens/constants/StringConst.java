package language.tokens.constants;

public class StringConst extends ConstToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(StringConst.class);
	}
}
