package language.tokens.operators.arithmetic;

import frontend.tokens.Token;

public class ArithmeticToken extends Token {

	@Override
	public boolean showInAST() {
		return false;
	}
}
