package language.tokens.operators.arithmetic;

public class Add extends ArithmeticToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Add.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
