package language.tokens.operators.arithmetic;

public class Modulo extends ArithmeticToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Modulo.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
