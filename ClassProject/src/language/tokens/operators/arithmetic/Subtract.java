package language.tokens.operators.arithmetic;

public class Subtract extends ArithmeticToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Subtract.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
