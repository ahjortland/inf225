package language.tokens.operators.arithmetic;

public class Divide extends ArithmeticToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Divide.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
