package language.tokens.operators.arithmetic;

public class Multiply extends ArithmeticToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Multiply.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
