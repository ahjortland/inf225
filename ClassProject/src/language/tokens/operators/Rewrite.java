package language.tokens.operators;

import frontend.tokens.Token;

public class Rewrite extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Rewrite.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
