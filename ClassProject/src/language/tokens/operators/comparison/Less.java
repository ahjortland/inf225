package language.tokens.operators.comparison;


public class Less extends CompareToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Less.class);
	}
}
