package language.tokens.operators.comparison;

public class Equal extends CompareToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Equal.class);
	}
}
