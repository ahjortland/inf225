package language.tokens.operators.comparison;

public class GreaterEqual extends CompareToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(GreaterEqual.class);
	}
}
