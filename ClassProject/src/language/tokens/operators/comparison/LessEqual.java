package language.tokens.operators.comparison;

public class LessEqual extends CompareToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(LessEqual.class);
	}
}
