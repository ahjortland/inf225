package language.tokens.operators.comparison;

public class NotEqual extends CompareToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(NotEqual.class);
	}
}
