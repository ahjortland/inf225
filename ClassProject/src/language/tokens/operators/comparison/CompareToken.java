package language.tokens.operators.comparison;

import frontend.tokens.Token;

public class CompareToken extends Token {

	@Override
	public boolean showInAST() {
		return false;
	}
}
