package language.tokens.operators.comparison;

public class Greater extends CompareToken {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Greater.class);
	}
}
