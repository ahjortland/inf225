package language.tokens;

import frontend.tokens.Token;

public class Assign extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Assign.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
