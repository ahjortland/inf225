package language.tokens;

import frontend.tokens.Token;

public class VarArgs extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(VarArgs.class);
	}

	@Override
	public boolean showInAST() {
		return true;
	}
}
