package language.tokens;

import frontend.tokens.Token;

public class Semicolon extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Semicolon.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
