package language.tokens;

import language.TypeChecker.Type;
import frontend.tokens.Token;

public class Identifier extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Identifier.class);
	}

	@Override
	public boolean showInAST() {
		return true;
	}

	@Override
	public Type getType() {
		return new language.TypeChecker.Type.Identifier(this);
	}
}
