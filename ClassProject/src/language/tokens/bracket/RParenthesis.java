package language.tokens.bracket;

import frontend.tokens.Token;

public class RParenthesis extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(RParenthesis.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
