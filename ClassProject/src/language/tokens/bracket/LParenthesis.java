package language.tokens.bracket;

import frontend.tokens.Token;

public class LParenthesis extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(LParenthesis.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
