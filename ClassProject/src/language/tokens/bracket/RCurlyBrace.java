package language.tokens.bracket;

import frontend.tokens.Token;

public class RCurlyBrace extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(RCurlyBrace.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
