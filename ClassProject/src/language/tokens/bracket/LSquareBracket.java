package language.tokens.bracket;

import frontend.tokens.Token;

public class LSquareBracket extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(LSquareBracket.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
