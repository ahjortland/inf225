package language.tokens.bracket;

import frontend.tokens.Token;

public class RSquareBracket extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(RSquareBracket.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
