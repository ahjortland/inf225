package language.tokens.bracket;

import frontend.tokens.Token;

public class LCurlyBrace extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(LCurlyBrace.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
