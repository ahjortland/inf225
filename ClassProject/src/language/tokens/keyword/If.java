package language.tokens.keyword;

import frontend.tokens.Token;

public class If extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(If.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
