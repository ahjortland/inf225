package language.tokens.keyword;

import frontend.tokens.Token;

public class Return extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Return.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
