package language.tokens.keyword;

import frontend.tokens.Token;

public class Else extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Else.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
