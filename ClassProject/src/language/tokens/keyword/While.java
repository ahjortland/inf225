package language.tokens.keyword;

import frontend.tokens.Token;

public class While extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(While.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
