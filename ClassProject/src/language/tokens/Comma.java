package language.tokens;

import frontend.tokens.Token;

public class Comma extends Token {
	public static frontend.syntax.ParseRule getTerminal() {
		return new frontend.syntax.ParseRule(Comma.class);
	}

	@Override
	public boolean showInAST() {
		return false;
	}
}
