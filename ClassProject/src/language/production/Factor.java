package language.production;

import exceptions.SyntaxError;
import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Factor extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		Token t = sc.peekToken();
		if(t instanceof language.tokens.bracket.LParenthesis) {
			SyntaxTree lparen = getTerminalTree(sc, language.tokens.bracket.LParenthesis.class);
			SyntaxTree expr = new Expression().parse(sc);
			SyntaxTree rparen = getTerminalTree(sc, language.tokens.bracket.RParenthesis.class);
			return new SyntaxTree(this, lparen, expr, rparen);
		} else if(t instanceof language.tokens.constants.NumberConst
				|| t instanceof language.tokens.operators.arithmetic.Subtract) { // Negative numbers
			return new SyntaxTree(this, new Number().parse(sc));
		} else if(t instanceof language.tokens.constants.BoolConst) {
			return new SyntaxTree(this, new Bool().parse(sc));
		} else if(t instanceof language.tokens.constants.StringConst) {
			return new SyntaxTree(this, new String().parse(sc));
		}
		throw new SyntaxError("Expected a factor", t);
	}

}
