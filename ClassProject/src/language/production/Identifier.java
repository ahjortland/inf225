package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Identifier extends ShownProd {
	private final Token id;
	public Identifier(Token id) {
		this.id = id;
	}
	
	@Override
	public Position getPosition() {
		return id.start;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		return new SyntaxTree(this, getTerminalTree(sc, 
				language.tokens.Identifier.class, id));
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		Type.Identifier id = (language.TypeChecker.Type.Identifier) subtypes[0];
		if(environment.contains(id.id)) {
			id.setType(environment.get(id.id));
		}
		return new Type[] { id };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
	}
}
