package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Variable_Declaration extends ShownProd {
	private final Token type;
	private final Token id;
	
	@Override
	public Position getPosition() {
		return type.start;
	}
	
	
	public Variable_Declaration(Token type, Token id) {
		this.type = type;
		this.id = id;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		SyntaxTree type = new Type_Specifier(this.type).parse(sc);
		SyntaxTree variable = new Variable(id).parse(sc);
		Token t = sc.peekToken();
		if(t instanceof language.tokens.Semicolon) {
			return new SyntaxTree(this, 
					type,
					variable,
					getTerminalTree(sc, language.tokens.Semicolon.class));
		}
		SyntaxTree assign = new Assign(variable).parse(sc);
		return new SyntaxTree(this, 
				type,
				assign,
				getTerminalTree(sc, language.tokens.Semicolon.class)
				);
	}


	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return null;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		Type.Identifier id = new Type.Identifier(this.id);
		id.setType(type.getType());
		environment.declare(id.id, type.getType());
	}
}
