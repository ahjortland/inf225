package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Params extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}


	@Override
	public SyntaxTree parse(Scanner sc) {
		Token t = sc.peekToken();
		pos = t.start;
		if(t instanceof language.tokens.types.Void) {
			return new SyntaxTree(this, getTerminalTree(sc, language.tokens.types.Void.class));
		} else if(sc.peekToken() instanceof language.tokens.VarArgs) {
			return new SyntaxTree(this, new VarArg().parse(sc));
		} else if(t instanceof language.tokens.types.TypeToken) {
			return new SyntaxTree(this, new Param_List().parse(sc));
		}
		language.tokens.types.Void empty = new language.tokens.types.Void();
		empty.start = sc.peekToken().start;
		empty.end = sc.peekToken().end;
		empty.literal = "void";
		return new SyntaxTree(this, new SyntaxTree(empty));
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return null;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
