package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Declaration extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		if(sc.peekToken() instanceof language.tokens.keyword.Rewrite) {
			return new SyntaxTree(this, new Rewrite().parse(sc));
		}
		Token type = getTerminal(sc, language.tokens.types.TypeToken.class);
		Token id = getTerminal(sc, language.tokens.Identifier.class);
		
		if(sc.peekToken() instanceof language.tokens.bracket.LParenthesis) {
			return  new SyntaxTree(this, new Function_Declaration(type, id).parse(sc));
		}
		return new SyntaxTree(this, new Variable_Declaration(type, id).parse(sc)); 
	}

}

