package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Argument extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		return new SyntaxTree(this, new Expression().parse(sc));
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		if(subtypes.length > 1) {
			if(subtypes.length == 0) {
				throw new TypeError("Argument doesn't have a type", new Position(0, 0));
			}
			throw new TypeError("Argument have more than 1 type", subtypes[0].pos);
		}
		return new Type[] { subtypes[0] };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
