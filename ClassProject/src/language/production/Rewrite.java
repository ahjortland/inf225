package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Rewrite extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree syn = getTerminalTree(sc, language.tokens.keyword.Rewrite.class);
		SyntaxTree from = new Statement().parse(sc);
		SyntaxTree arrow = getTerminalTree(sc, language.tokens.operators.Rewrite.class);
		SyntaxTree to = new Statement().parse(sc);
		return new SyntaxTree(this, syn, from, arrow, to);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return null;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
