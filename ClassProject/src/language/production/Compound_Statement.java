package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Compound_Statement extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree lbrace = getTerminalTree(sc, language.tokens.bracket.LCurlyBrace.class);
		SyntaxTree statement_list = new Statement_List().parse(sc);
		SyntaxTree rbrace = getTerminalTree(sc, language.tokens.bracket.RCurlyBrace.class);
		
		return new SyntaxTree(this, lbrace, statement_list, rbrace);
	}

	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		environment.exitScope();
		return null;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		environment.enterScope();
	}
}
