package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public final class Expression extends ShownProd {
	public static boolean isExpression(Token tok) {
		return tok instanceof language.tokens.Identifier
				|| tok instanceof language.tokens.bracket.LParenthesis
				|| tok instanceof language.tokens.constants.ConstToken
				;
	}
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		return new SyntaxTree(this, new Arithmetic_Expression().parse(sc));
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return new Type[] { subtypes[0] };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
