package language.production;

import exceptions.SyntaxError;
import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Arithmetic_Expression extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree addExpr = new Add_Expression().parse(sc);
		Token t = sc.peekToken();
		if(t instanceof language.tokens.operators.comparison.CompareToken) {
			SyntaxTree compare;
			if(t instanceof language.tokens.operators.comparison.Equal) {
				compare = new Equal(addExpr).parse(sc);
			} else if(t instanceof language.tokens.operators.comparison.NotEqual) {
				compare = new NotEqual(addExpr).parse(sc);
			} else if(t instanceof language.tokens.operators.comparison.GreaterEqual) {
				compare = new GreaterEqual(addExpr).parse(sc);
			} else if(t instanceof language.tokens.operators.comparison.Greater) {
				compare = new Greater(addExpr).parse(sc);
			} else if(t instanceof language.tokens.operators.comparison.LessEqual) {
				compare = new LessEqual(addExpr).parse(sc);
			}  else if(t instanceof language.tokens.operators.comparison.Less) {
				compare = new Less(addExpr).parse(sc);
			} else {
				throw new SyntaxError("Expected comparison operator.", t);
			}
			return new SyntaxTree(this, compare);
		}
		return new SyntaxTree(this, addExpr);
	}
}
