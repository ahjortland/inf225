package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Statement_List extends HiddenProd {
	private static boolean isStatement(Token tok) {
		return tok instanceof language.tokens.Semicolon
				|| tok instanceof language.tokens.types.TypeToken
				|| tok instanceof language.tokens.bracket.LCurlyBrace
				|| tok instanceof language.tokens.keyword.If
				|| tok instanceof language.tokens.keyword.While
				|| tok instanceof language.tokens.keyword.Return
				|| Expression.isExpression(tok)
				;
	}
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		if(isStatement(sc.peekToken())) {
			SyntaxTree statement = new Statement().parse(sc);
			if(isStatement(sc.peekToken())) {
				return new SyntaxTree(this, statement, new Statement_List().parse(sc));
			}
			return new SyntaxTree(this, statement);
		}
		return new SyntaxTree(this);
	}
}
