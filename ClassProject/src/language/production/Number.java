package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Number extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		if(sc.peekToken() instanceof language.tokens.operators.arithmetic.Subtract) {
			return new SyntaxTree(this, 
					getTerminalTree(sc, language.tokens.operators.arithmetic.Subtract.class),
					getTerminalTree(sc, language.tokens.constants.NumberConst.class));
		}
		return new SyntaxTree(this, getTerminalTree(sc, 
				language.tokens.constants.NumberConst.class));
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return new Type[] { new Type.Int(pos) };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
