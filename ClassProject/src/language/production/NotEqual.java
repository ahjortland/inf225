package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class NotEqual extends ShownProd {
	private final SyntaxTree lparam;
	private Position pos;

	public NotEqual(SyntaxTree lparam) {
		this.lparam = lparam;
	}
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree cmpOp = getTerminalTree(sc, language.tokens.operators.comparison.NotEqual.class);
		SyntaxTree rparam = new Add_Expression().parse(sc);
		return new SyntaxTree(this, lparam, cmpOp, rparam);
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		Type myType = new Type.Bool(subtypes[1].pos); 
		if(subtypes[0].equals(subtypes[1])) {
			return new Type[] { myType };
		}
		throw new TypeError(java.lang.String.format("Cannot compare type %s with type %s", subtypes[0], subtypes[1]), subtypes[0].pos);
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
