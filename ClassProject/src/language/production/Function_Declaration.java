package language.production;

import language.TypeChecker.Type;
import language.TypeChecker.TypeChecker;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Function_Declaration extends ShownProd {
	private final Token type;
	private final Token id;
	
	public Function_Declaration(Token type, Token id) {
		this.type = type;
		this.id = id;
	}
	
	@Override
	public Position getPosition() {
		return type.start;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		SyntaxTree type = new Type_Specifier(this.type).parse(sc);
		SyntaxTree id = new Identifier(this.id).parse(sc);
		SyntaxTree lpar = getTerminalTree(sc, language.tokens.bracket.LParenthesis.class);
		SyntaxTree params = new Params().parse(sc);
		SyntaxTree rpar = getTerminalTree(sc, language.tokens.bracket.RParenthesis.class);
		SyntaxTree body;
		if(sc.peekToken() instanceof language.tokens.Semicolon) {
			body = getTerminalTree(sc, language.tokens.Semicolon.class);
		} else {
			body = new Compound_Statement().parse(sc);
		}
		return new SyntaxTree(this,
				type,
				id,
				lpar,
				params,
				rpar,
				body);
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		Type.Identifier id = (language.TypeChecker.Type.Identifier) subtypes[1];
		environment.assign(id.id, subtypes[0]);
		environment.exitScope();
		return null;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		environment.declare(((Type.Identifier) id. getType()).id, null);
		environment.enterScope();
		environment.declare(TypeChecker.retKey, type.getType());
	}
}
