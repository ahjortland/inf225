package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Mul_Expression extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree param1 = new Variable_Expression().parse(sc);
		Token t = sc.peekToken();
		if(t instanceof language.tokens.operators.arithmetic.Multiply) {
			SyntaxTree op = getTerminalTree(sc, language.tokens.operators.arithmetic.Multiply.class);
			SyntaxTree param2 = new Mul_Expression().parse(sc);
			return new SyntaxTree(this, new SyntaxTree(new Mul(), param1, op, param2));
			
		} else if(t instanceof language.tokens.operators.arithmetic.Divide) {
			SyntaxTree op = getTerminalTree(sc, language.tokens.operators.arithmetic.Divide.class);
			SyntaxTree param2 = new Mul_Expression().parse(sc);
			return new SyntaxTree(this, new SyntaxTree(new Div(), param1, op, param2));
			
		} else if(t instanceof language.tokens.operators.arithmetic.Modulo) {
			SyntaxTree op = getTerminalTree(sc, language.tokens.operators.arithmetic.Modulo.class);
			SyntaxTree param2 = new Mul_Expression().parse(sc);
			return new SyntaxTree(this, new SyntaxTree(new Mod(), param1, op, param2));
		}
		return new SyntaxTree(this, param1);
	}
}
