package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Mul extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		return null;
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		Type myType = new Type.Int(subtypes[1].pos); 
		for(Type t : subtypes) {
			if(!t.equals(myType)) {
				throw new TypeError(java.lang.String.format("The type %s is not an integer", t.toString(), myType.toString()), t.pos);
			}
		}
		return new Type[] { myType };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
