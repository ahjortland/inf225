package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Declaration_List extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree child = new Declaration().parse(sc);
		Token t = sc.peekToken();
		if (t instanceof language.tokens.types.TypeToken
				|| t instanceof language.tokens.keyword.Rewrite) {
			return new SyntaxTree(this, child, new Declaration_List().parse(sc));
		}

		return new SyntaxTree(this, child);
	}
}
