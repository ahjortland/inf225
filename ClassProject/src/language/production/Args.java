package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Args extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		Token t = sc.peekToken();
		if(Expression.isExpression(t)) {
			return new SyntaxTree(this, new Arg_List().parse(sc));
		}
		language.tokens.types.Void empty = new language.tokens.types.Void();
		empty.start = sc.peekToken().start;
		empty.end = sc.peekToken().end;
		empty.literal = "void";
		return new SyntaxTree(this, new SyntaxTree(empty));
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return subtypes;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}

}
