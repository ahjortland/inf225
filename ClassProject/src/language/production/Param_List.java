package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Param_List extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}


	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree param = new Param().parse(sc);
		if(sc.peekToken() instanceof language.tokens.Comma) {
			SyntaxTree comma = getTerminalTree(sc, language.tokens.Comma.class);
			return new SyntaxTree(this, param, comma, new Param_List().parse(sc));
		}
		return new SyntaxTree(this, param);
	}
}
