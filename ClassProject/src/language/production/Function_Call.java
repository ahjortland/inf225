package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Function_Call extends ShownProd {
	private final Token id;

	public Function_Call(Token id) {
		this.id = id;
	}
	
	@Override
	public Position getPosition() {
		return id.start;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		SyntaxTree identifier = new Identifier(id).parse(sc);
		SyntaxTree lparen = getTerminalTree(sc,
				language.tokens.bracket.LParenthesis.class);
		SyntaxTree args = new Args().parse(sc);
		SyntaxTree rparen = getTerminalTree(sc,
				language.tokens.bracket.RParenthesis.class);

		return new SyntaxTree(this, identifier, lparen, args, rparen);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return new Type[] { environment.get(((Type.Identifier) subtypes[0]).id) };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
