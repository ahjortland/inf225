package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Type_Specifier extends ShownProd {
	private final Token type;
	
	@Override
	public Position getPosition() {
		return type.start;
	}
	

	public Type_Specifier(Token type) {
		if(type instanceof language.tokens.types.TypeToken) {
			this.type = type;
		} else {
			throw new TypeError("The token " + type + " is not a type.", type.start);
		}
	}
	
	@Override
	public SyntaxTree parse(Scanner sc) {
		return new SyntaxTree(this, getTerminalTree(sc, language.tokens.types.TypeToken.class, type));
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return new Type[] { subtypes[0] };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
