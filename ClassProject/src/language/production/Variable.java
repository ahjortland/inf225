package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Variable extends ShownProd {
	private final Token id;
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}
	
	public Variable(Token id) {
		this.id = id;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		SyntaxTree id = new Identifier(this.id).parse(sc);
		pos = sc.peekToken().start;
		
		if(sc.peekToken() instanceof language.tokens.bracket.LSquareBracket) {
			return new SyntaxTree(this, 
					id,
					new Array().parse(sc));
		}
		return new SyntaxTree(this, id);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return new Type[] { subtypes[0] };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// Doesn't need to do anything
	}

}
