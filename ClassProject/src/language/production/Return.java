package language.production;

import language.TypeChecker.Type;
import language.TypeChecker.TypeChecker;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Return extends ShownProd {
	private Position pos;

	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree keyword = getTerminalTree(sc, language.tokens.keyword.Return.class);
		if(sc.peekToken() instanceof language.tokens.Semicolon) {
			return new SyntaxTree(this, keyword, getTerminalTree(sc, language.tokens.Semicolon.class));
		}
		SyntaxTree expr = new Expression().parse(sc);
		SyntaxTree semicolon = getTerminalTree(sc, language.tokens.Semicolon.class);

		return new SyntaxTree(this, keyword, expr, semicolon);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		Type s = subtypes[0];
		if(s instanceof Type.Identifier) {
			s = environment.get(((Type.Identifier) s).id);
		}
		if(s.equals(environment.get(TypeChecker.retKey))) {
			return new Type[] { s };
		}
		throw new TypeError("return statements has different return type", subtypes[0].pos);
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}

}
