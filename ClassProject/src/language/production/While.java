package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class While extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}
	
	@Override
	public SyntaxTree parse(Scanner sc) {
		SyntaxTree whileKeyword = getTerminalTree(sc, language.tokens.keyword.While.class);
		SyntaxTree lparen = getTerminalTree(sc, language.tokens.bracket.LParenthesis.class);
		SyntaxTree expr = new Expression().parse(sc);
		SyntaxTree rparen = getTerminalTree(sc, language.tokens.bracket.RParenthesis.class);
		SyntaxTree whileStat = new Statement().parse(sc);
		
		pos = sc.peekToken().start;
		return new SyntaxTree(this, whileKeyword, lparen, expr, rparen, whileStat);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		if(subtypes[0].equals(new Type.Bool(null))) {
			return null;
		}
		throw new TypeError("Condition block is not a boolean expression", subtypes[0].pos);
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
