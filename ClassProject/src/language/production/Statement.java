package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Statement extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}
	
	@Override
	public SyntaxTree parse(Scanner sc) {
		Token tok = sc.peekToken();
		this.pos = tok.start;
		if(tok instanceof language.tokens.Semicolon) {
			return new SyntaxTree(this, getTerminalTree(sc, language.tokens.Semicolon.class));
		} else if(tok instanceof language.tokens.types.TypeToken) {
			return new SyntaxTree(this, new Variable_Declaration(sc.popToken(), sc.popToken()).parse(sc));
		} else if(tok instanceof language.tokens.bracket.LCurlyBrace) {
			return new SyntaxTree(this, new Compound_Statement().parse(sc));
		} else if(tok instanceof language.tokens.keyword.If) {
			return new SyntaxTree(this, new If().parse(sc));
		} else if(tok instanceof language.tokens.keyword.While) {
			return new SyntaxTree(this, new While().parse(sc));
		} else if(tok instanceof language.tokens.keyword.Return) {
			return new SyntaxTree(this, new Return().parse(sc));
		} else if(Expression.isExpression(tok)) {
			return new SyntaxTree(this, new Expression().parse(sc), getTerminalTree(sc, language.tokens.Semicolon.class));
		}
		return null;
	}

}
