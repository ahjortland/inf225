package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Array extends ShownProd {
	private int length;
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree lbrack =getTerminalTree(sc, language.tokens.bracket.LSquareBracket.class);
		SyntaxTree index = new Expression().parse(sc);
		SyntaxTree rbrack =getTerminalTree(sc, language.tokens.bracket.RSquareBracket.class);
		
		length = Integer.parseInt(SyntaxTree.getFirstToken(index).literal);
		return new SyntaxTree(this, lbrack, index, rbrack);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		if(new Type.Int(null).equals(subtypes[0])) {
			return new Type[] { new Type.Arr(subtypes[0], length) };
		}
		throw new TypeError("Index is not an integer", subtypes[0].pos);
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
