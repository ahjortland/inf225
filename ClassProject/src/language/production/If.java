package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class If extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree ifKeyword = getTerminalTree(sc, language.tokens.keyword.If.class);
		SyntaxTree lparen = getTerminalTree(sc, language.tokens.bracket.LParenthesis.class);
		SyntaxTree expr = new Expression().parse(sc);
		SyntaxTree rparen = getTerminalTree(sc, language.tokens.bracket.RParenthesis.class);
		SyntaxTree ifStat = new Statement().parse(sc);
		if(sc.peekToken() instanceof language.tokens.keyword.Else) {
			SyntaxTree elseKyword = getTerminalTree(sc, language.tokens.keyword.Else.class);
			SyntaxTree elseStat = new Statement().parse(sc);
			return new SyntaxTree(this, ifKeyword, lparen, expr, rparen, ifStat, elseKyword, elseStat);
		}
		return new SyntaxTree(this, ifKeyword, lparen, expr, rparen, ifStat);
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		if(subtypes[0].equals(new Type.Bool(null))) {
			return null;
		}
		throw new TypeError("Condition block is not a boolean expression", subtypes[0].pos);
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
