package language.production;

import language.TypeChecker.Type;
import language.tokens.types.Str;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class String extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}
	
	@Override
	public SyntaxTree parse(Scanner sc) {
		SyntaxTree terminal = getTerminalTree(sc, 
				language.tokens.constants.StringConst.class);
		pos = sc.peekToken().start;
				
		return new SyntaxTree(this, terminal);
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		return new Type[] { new Type.Str(pos) };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
