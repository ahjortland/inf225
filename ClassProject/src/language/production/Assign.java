package language.production;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.Scanner; 
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Assign extends ShownProd {
	private final SyntaxTree variable;
	private Position pos;
	
	public Assign(SyntaxTree variable) {
		this.variable = variable;
	}
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree assignChar = getTerminalTree(sc, language.tokens.Assign.class);
		SyntaxTree result = new Expression().parse(sc);
		return new SyntaxTree(this, variable, assignChar, result);
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		Type s = environment.get(((Type.Identifier) subtypes[0]).id);
		if(s.equals(subtypes[1])) {
			return new Type[] { subtypes[1] }; 
		}
		throw new TypeError(java.lang.String.format("Assigning %s to %s is not allowed.", subtypes[1], s), subtypes[1].pos);
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}
}
