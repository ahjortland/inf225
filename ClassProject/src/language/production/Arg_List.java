package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Arg_List extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree expr = new Argument().parse(sc);
		
		Token t = sc.peekToken();
		if(t instanceof language.tokens.Comma) {
			return new SyntaxTree(this, expr, getTerminalTree(sc, language.tokens.Comma.class), new Arg_List().parse(sc));
		}
		return new SyntaxTree(this, expr);
	}

}
