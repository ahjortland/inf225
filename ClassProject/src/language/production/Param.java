package language.production;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;

public class Param extends ShownProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree type = new Type_Specifier(sc.popToken()).parse(sc);
		SyntaxTree identifier = new Identifier(sc.popToken()).parse(sc);
		if(sc.peekToken() instanceof language.tokens.bracket.LSquareBracket) {
			SyntaxTree arr = new Array().parse(sc);
			return new SyntaxTree(this, type, identifier, arr);
		}
		return new SyntaxTree(this, type, identifier);
	}
	
	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		environment.declare(((Type.Identifier) subtypes[1]).id, subtypes[0]);
		return new Type[] { subtypes[0] };
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		// No need to do anything
	}

}
