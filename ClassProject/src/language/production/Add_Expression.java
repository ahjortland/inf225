package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Add_Expression extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		pos = sc.peekToken().start;
		SyntaxTree param1 = new Mul_Expression().parse(sc);
		Token t = sc.peekToken();
		if(t instanceof language.tokens.operators.arithmetic.Add) {
			SyntaxTree op = getTerminalTree(sc, language.tokens.operators.arithmetic.Add.class);
			SyntaxTree param2 = new Add_Expression().parse(sc);
			return new SyntaxTree(this, new SyntaxTree(new Add(), param1, op, param2));
		} else if(t instanceof language.tokens.operators.arithmetic.Subtract) {
			SyntaxTree op = getTerminalTree(sc, language.tokens.operators.arithmetic.Subtract.class);
			SyntaxTree param2 = new Add_Expression().parse(sc);
			return new SyntaxTree(this, new SyntaxTree(new Sub(), param1, op, param2));
		}
		
		return new SyntaxTree(this, param1);
	}
}
