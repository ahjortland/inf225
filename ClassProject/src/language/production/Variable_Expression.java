package language.production;

import frontend.Scanner;
import frontend.syntax.HiddenProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public class Variable_Expression extends HiddenProd {
	private Position pos;
	
	@Override
	public Position getPosition() {
		return pos;
	}
	
	@Override
	public SyntaxTree parse(Scanner sc) {
		Token t = sc.peekToken();
		pos = sc.peekToken().start;

		if(t instanceof language.tokens.Identifier) {
			t = sc.popToken();
			if(sc.peekToken() instanceof language.tokens.bracket.LParenthesis) {
				return new SyntaxTree(this, new Function_Call(t).parse(sc));
			} else {
				SyntaxTree variable = new Variable(t).parse(sc);
				if(sc.peekToken() instanceof language.tokens.Assign) {
					SyntaxTree assign = new Assign(variable).parse(sc);
					
					return new SyntaxTree(this, assign);
				}
				return new SyntaxTree(this, variable);
			}
		} else {
			return new SyntaxTree(this, new Factor().parse(sc));
		}
	}

}
