package language.TypeChecker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;

import language.TypeChecker.walks.DeclarationWalk;
import nuthatch.library.BaseWalk;
import frontend.environment.Environment;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;
import static nuthatch.library.JoinPoints.*;

public class TypeChecker extends BaseWalk<SyntaxTreeWalker> {
	public final static String retKey = "-ret";

	//	private static boolean binOp(List<Type> check, Class<? extends Type> type) {
	//		if (check.size() != 2) {
	//			return false;
	//		}
	//
	//		Type var1 = check.get(0);
	//		Type var2 = check.get(1);
	//		if (!checkType(var1, type)) {
	//			return false;
	//		}
	//		if (!checkType(var2, type)) {
	//			return false;
	//		}
	//		return true;
	//	}
	//
	//	public static void checkProgram(SyntaxTree program) {
	//		new TypeChecker(program).check();
	//	}
	//
	//	private static boolean checkType(Class<? extends Type> t1,
	//			Class<? extends Type> t2) {
	//		boolean test = t1.equals(t2);
	//		return test;
	//	}
	//
	//	private static boolean checkType(Type t1, Class<? extends Type> t2) {
	//		return checkType(t1.getClass(), t2);
	//	}
	//
	//	private static boolean checkType(Type t1, Type t2) {
	//		return checkType(t1.getClass(), t2.getClass());
	//	}
	//
	private Environment<Type> environment;

	private final SyntaxTree program;

	private final Stack<List<Type>> lastType = new Stack<>();

	public TypeChecker(SyntaxTree program) {
		this.program = program;
		this.environment = new Environment<Type>();
	}

	public void check() {
		//environment = new DeclarationWalk(environment).walk(program);

		new SyntaxTreeWalker(new SyntaxTreeCursor(program), this).start();
	}

	@Override
	public int step(SyntaxTreeWalker w) {
		if(down(w)) {
			lastType.push(new ArrayList<Type>());
			if(w.getType() != null) {
				w.getType().typeCheckDown(environment);
			}
		}
		if(up(w)) {
			List<Type> arr = lastType.pop();
			Type[] types = null;
			if(w.getData() != null) {
				types = new Type[] { w.getData().getType() };
			} else if(w.getType() != null) {
				Type[] t2 = new Type[arr.size()];
				for(int i = 0; i < t2.length; i++) {
					t2[i] = arr.get(i);
				}
				types = w.getType().typeCheckUp(environment, t2);
			}
			if(types != null) {
				if(types.length > 0) {
					w.setMetadata("type", types[0]);
				}
				for(Type t : types) {
					lastType.peek().add(t);
				}
			}
		}
		return NEXT;
	}

	//
	//	private Type check(List<Type> check, language.production.Add i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Int(check.get(0).pos);
	//	}
	//
	//	private List<Type> check(List<Type> check, language.production.Args i) {
	//		return check;
	//	}
	//
	//	private Type check(List<Type> retType, Argument prod) {
	//		return retType.get(0);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Array i) {
	//		return check.get(0);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Assign i) {
	//		Type type = check.get(0);
	//		if (type instanceof Type.Arr) {
	//			throw new TypeError("Cannot assign to an array", check.get(1).pos);
	//		}
	//		Type assignTyp = check.get(1);
	//		if (type.equals(assignTyp)) {
	//			return assignTyp;
	//		}
	//		throw new TypeError("Assigned type " + assignTyp
	//				+ " to a variable of type " + type + ".", assignTyp.pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Function_Call i) {
	//		Type f = check.get(0);
	//		if (!(f instanceof Type.Function)) {
	//			throw new TypeError("Not a function", f.pos);
	//		}
	//		Type.Function fun = (Type.Function) f;
	//		if (fun.isVarArg) {
	//			return fun.returnType;
	//		}
	//		if (check.size() != 2 || !(check.get(1) instanceof Type.Void)) {
	//			if (check.size() - 1 != fun.paramTypes.length) {
	//				throw new TypeError(
	//						"Wrong amount of parameters. Function takes "
	//								+ fun.paramTypes.length
	//								+ ", but it was called with "
	//								+ (check.size() - 1) + " parameters", fun.pos);
	//			}
	//		}
	//
	//		for (int index = 0; index < fun.paramTypes.length; index++) {
	//			Type t1 = check.get(index + 1);
	//			Type t2 = fun.paramTypes[index];
	//			if (!(t1.equals(t2))) {
	//				throw new TypeError("Parameter " + (index + 1) + " is of type "
	//						+ t2 + ", but was called wit type " + t1 + ".", t1.pos);
	//			}
	//		}
	//
	//		return fun.returnType;
	//	}
	//
	//	private Type check(List<Type> check, language.production.Div i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Int(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Equal i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Bool(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Expression i) {
	//		return check.get(0);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Greater i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Bool(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.GreaterEqual i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Bool(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Identifier i) {
	//		return check.get(0);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Less i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Bool(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.LessEqual i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Bool(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Mod i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Int(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Mul i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Int(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.NotEqual i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Bool(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Number i) {
	//		Type number = check.get(0);
	//		if (!checkType(number, Type.Int.class)) {
	//			throw new TypeError("Not int", number.pos);
	//		}
	//		return number;
	//	}
	//
	//	private Type check(List<Type> check, Bool prod) {
	//		Type type = check.get(0);
	//		if (!checkType(type, Type.Bool.class)) {
	//			throw new TypeError("Not bool", type.pos);
	//		}
	//		return type;
	//	}
	//
	//	private Type check(List<Type> check, language.production.String prod) {
	//		Type type = check.get(0);
	//		if (!checkType(type, Type.Str.class)) {
	//			throw new TypeError("Not string", type.pos);
	//		}
	//		return type;
	//	}
	//
	//	private void check(List<Type> check, language.production.Return i) {
	//		if (check.size() == 0) {
	//			Type t = environment.get(retKey);
	//			if (!checkType(t, Type.Void.class)) {
	//				throw new TypeError("This function does not have a type", t.pos);
	//			}
	//		} else {
	//			Type type = check.get(0);
	//			if (!environment.contains(retKey)) {
	//				throw new TypeError("Return statement not in a function",
	//						type.pos);
	//			}
	//			if (!checkType(type, environment.get(retKey))) {
	//				throw new TypeError(
	//						"Return type not same as function declaration",
	//						type.pos);
	//			}
	//		}
	//	}
	//
	//	private Type check(List<Type> check, language.production.Sub i) {
	//		if (!binOp(check, Type.Int.class)) {
	//			throw new TypeError("Parameters for binary operator is not int",
	//					check.get(0).pos);
	//		}
	//		return new Type.Int(check.get(0).pos);
	//	}
	//
	//	private Type check(List<Type> check, language.production.Type_Specifier i) {
	//		return check.get(0);
	//	}
	//
	//	private Type check(List<Type> check, Variable prod) {
	//		Type t = check.get(0);
	//		if (t instanceof Type.Arr) {
	//			if (check.size() == 2) {
	//				return ((Type.Arr) t).arrType;
	//			}
	//		}
	//		return t;
	//	}
	//
	//	private Type check(List<Type> check, VarArg prod) {
	//		return null;
	//	}
	//
	//	private void check(List<Type> check, Variable_Declaration prod) {
	//		Type type = check.get(1);
	//		if (!(type instanceof Type.Arr)) {
	//			Type init = check.get(1);
	//			if (!type.equals(init)) {
	//				throw new TypeError("Wrong type. Expected " + type + ", got "
	//						+ init + ".", init.pos);
	//			}
	//		}
	//	}
	//
	//	public boolean isKnown(Production prod) {
	//		return !(prod instanceof language.production.Add
	//				|| prod instanceof language.production.Args
	//				|| prod instanceof language.production.Argument
	//				|| prod instanceof language.production.Array
	//				|| prod instanceof language.production.Assign
	//				|| prod instanceof language.production.Bool
	//				|| prod instanceof language.production.Compound_Statement
	//				|| prod instanceof language.production.Function_Call
	//				|| prod instanceof language.production.Div
	//				|| prod instanceof language.production.Equal
	//				|| prod instanceof language.production.Expression
	//				|| prod instanceof language.production.Function_Declaration
	//				|| prod instanceof language.production.Greater
	//				|| prod instanceof language.production.GreaterEqual
	//				|| prod instanceof language.production.Identifier
	//				|| prod instanceof language.production.If
	//				|| prod instanceof language.production.Less
	//				|| prod instanceof language.production.LessEqual
	//				|| prod instanceof language.production.Mod
	//				|| prod instanceof language.production.Mul
	//				|| prod instanceof language.production.NotEqual
	//				|| prod instanceof language.production.Number
	//				|| prod instanceof language.production.Param
	//				|| prod instanceof language.production.Params
	//				|| prod instanceof language.production.Program
	//				|| prod instanceof language.production.Return
	//				|| prod instanceof language.production.Rewrite
	//				|| prod instanceof language.production.Sub
	//				|| prod instanceof language.production.Type_Specifier
	//				|| prod instanceof language.production.String
	//				|| prod instanceof language.production.Statement
	//				|| prod instanceof language.production.Variable
	//				|| prod instanceof language.production.VarArg
	//				|| prod instanceof language.production.Variable_Declaration || prod instanceof language.production.While);
	//	}
	//
	//	private boolean isScope(Production prod) {
	//		return prod instanceof language.production.Compound_Statement
	//				|| prod instanceof language.production.Args
	//				|| prod instanceof language.production.Program;
	//	}
	//
	//	@Override
	//	public int step(SyntaxTreeWalker w) {
	//		Production prod = w.getType();
	//		if (prod != null) {
	//			if (isKnown(prod)) {
	//				throw new Error("Unknown production: " + prod.GetName());
	//			}
	//		}
	//		if(w.isAtRoot()) {
	//			types.push(new ArrayList<Type>());
	//			return NEXT;
	//		}
	//		if(prod instanceof Rewrite) {
	//			return PARENT;
	//		}
	//
	//		if (leaf(w)) {
	//			Token token = w.getData();
	//			if (token instanceof language.tokens.Identifier) {
	//				if (environment.contains(token.literal)) {
	//					types.peek().add(environment.get(token.literal));
	//					w.setMetadata("type", types.peek().get(types.peek().size() - 1));
	//				} else {
	//					throw new TypeError("Variable " + token.literal
	//							+ " does not exist", token.start);
	//				}
	//			} else {
	//				Type type = Type.getType(token);
	//				if (type != null) {
	//					types.peek().add(type);
	//					w.setMetadata("type", types.peek().get(types.peek().size() - 1));
	//				} else {
	//					throw new Error("Whyyy?");
	//				}
	//			}
	//		} else if (up(w)) {
	//			List<Type> childType = types.pop();
	//			Type type = null;
	//			if (prod instanceof language.production.Add) {
	//				type = check(childType, (language.production.Add) prod);
	//			} else if (prod instanceof language.production.Assign) {
	//				type = check(childType, (language.production.Assign) prod);
	//			} else if (prod instanceof language.production.Args) {
	//				types.peek().addAll(check(childType, (language.production.Args) prod));
	//			} else if (prod instanceof language.production.Argument) {
	//				type = check(childType, (language.production.Argument) prod);
	//			} else if (prod instanceof language.production.Array) {
	//				type = check(childType, (language.production.Array) prod);
	//			} else if (prod instanceof language.production.Function_Call) {
	//				type = check(childType, (language.production.Function_Call) prod);
	//			} else if (prod instanceof language.production.Div) {
	//				type = check(childType, (language.production.Div) prod);
	//			} else if (prod instanceof language.production.Equal) {
	//				type = check(childType, (language.production.Equal) prod);
	//			} else if (prod instanceof language.production.Greater) {
	//				type = check(childType, (language.production.Greater) prod);
	//			} else if (prod instanceof language.production.GreaterEqual) {
	//				type = check(childType, (language.production.GreaterEqual) prod);
	//			} else if (prod instanceof language.production.Identifier) {
	//				type = check(childType, (language.production.Identifier) prod);
	//			} else if (prod instanceof language.production.Array) {
	//				type = check(childType, (language.production.Array) prod);
	//			} else if (prod instanceof language.production.Less) {
	//				type = check(childType, (language.production.Less) prod);
	//			} else if (prod instanceof language.production.LessEqual) {
	//				type = check(childType, (language.production.LessEqual) prod);
	//			} else if (prod instanceof language.production.Mod) {
	//				type = check(childType, (language.production.Mod) prod);
	//			} else if (prod instanceof language.production.Mul) {
	//				type = check(childType, (language.production.Mul) prod);
	//			} else if (prod instanceof language.production.Variable) {
	//				type = check(childType, (language.production.Variable) prod);
	//			} else if (prod instanceof language.production.NotEqual) {
	//				type = check(childType, (language.production.NotEqual) prod);
	//			} else if (prod instanceof language.production.Number) {
	//				type = check(childType, (language.production.Number) prod);
	//			} else if (prod instanceof language.production.String) {
	//				type = check(childType, (language.production.String) prod);
	//			} else if (prod instanceof language.production.Bool) {
	//				type = check(childType, (language.production.Bool) prod);
	//			} else if (prod instanceof language.production.Sub) {
	//				type = check(childType, (language.production.Sub) prod);
	//			} else if (prod instanceof language.production.Expression) {
	//				type = check(childType, (language.production.Expression) prod);
	//			} else if (prod instanceof language.production.Args) {
	//				types.peek().addAll(check(childType, (language.production.Args) prod));
	//			} else if (prod instanceof language.production.Return) {
	//				check(childType, (language.production.Return) prod);
	//			} else if (prod instanceof language.production.Variable_Declaration) {
	//				check(childType, (language.production.Variable_Declaration) prod);
	//			} else if (prod instanceof language.production.Type_Specifier) {
	//				type = check(childType, (language.production.Type_Specifier) prod);
	//			} else if (prod instanceof language.production.VarArg) {
	//				type = check(childType, (language.production.VarArg) prod);
	//			} else if (prod instanceof language.production.Function_Declaration) {
	//				environment.exitScope();
	//			}
	//			if (isScope(prod)) {
	//				environment.exitScope();
	//			}
	//			if (type != null) {
	//				types.peek().add(type);
	//				w.setMetadata("type", type);
	//			}
	//			System.out.print("");
	//		} else if (down(w)) {
	//			if (prod instanceof language.production.Variable_Declaration) {
	//				Type t = Type.getType(QueryFirst(w,
	//						language.production.Type_Specifier.class)
	//						.getBranch(1).getData());
	//
	//				SyntaxTreeCursor variable = QueryFirst(w,
	//						language.production.Variable.class);
	//				if (variable == null) { // Assignment
	//					variable = QueryFirst(w,
	//							language.production.Assign.class,
	//							language.production.Variable.class);
	//				}
	//				String id = getFirstToken(QueryFirst(variable, language.production.Identifier.class)).literal;
	//				SyntaxTreeCursor array = QueryFirst(variable, language.production.Array.class);
	//				if (array != null) {
	//					SyntaxTreeCursor arrSize = QueryFirst(array,
	//							language.production.Expression.class,
	//							language.production.Number.class);
	//					if (arrSize == null) {
	//						throw new TypeError("Cannot declare dynamically sized arrays", t.pos);
	//					}
	//					Type arrType = new Type.Arr(t, Integer.parseInt(getFirstToken(arrSize).literal));
	//					//w.setMetadata("type", arrType);
	//					environment.declare(id, arrType);
	//				} else {
	//					//w.setMetadata("type", t);
	//					environment.declare(id, t);
	//				}
	//			}
	//			if (prod instanceof language.production.Function_Declaration) {
	//				Type.Identifier id = (Identifier) Type.getType(w.getBranch(2).getBranch(1).getData());
	//				Function fun = ((Type.Function) environment.get(id.id));
	//				environment.enterScope();
	//				environment.declare(retKey, fun.returnType);
	//				for (int i = 0; i < fun.paramID.length; i++) {
	//					environment.declare(fun.paramID[i], fun.paramTypes[i]);
	//				}
	//				SyntaxTreeCursor s = QueryFirst(w, language.production.Compound_Statement.class);
	//				if (s == null) {
	//					return PARENT;
	//				}
	//				int next = s.getLastPathElement();
	//				return next;
	//			}
	//			if (isScope(prod)) {
	//				environment.enterScope();
	//			}
	//			types.push(new ArrayList<Type>());
	//		}
	//		return NEXT;
	//	}
}
