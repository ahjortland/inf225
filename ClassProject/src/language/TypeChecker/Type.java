package language.TypeChecker;

import frontend.tokens.Position;
import frontend.tokens.Token;

public class Type {
	public static Type getType(Token t) {
		if(t instanceof language.tokens.Identifier) {
			return new Identifier(t);
		} else if (t instanceof language.tokens.constants.BoolConst
				|| t instanceof language.tokens.types.Bool) {
			return new Type.Bool(t.start);
		} else if (t instanceof language.tokens.constants.StringConst
				|| t instanceof language.tokens.types.Str) {
			return new Type.Str(t.start);
		} else if (t instanceof language.tokens.constants.NumberConst
				|| t instanceof language.tokens.types.Int) {
			return new Type.Int(t.start);
		} else if (t instanceof language.tokens.types.Void) {
			return new Type.Void(t.start);
		}
		throw new Error("Whut?");
		//return null;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		return getClass().equals(obj.getClass());	
	};
	
	public final Position pos;
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	private Type(Position pos) {
		this.pos = pos;
	}

	public static class Index extends Type {
		public Index(Position t) {
			super(t);
		}
	}
	
	public static class Identifier extends Type {
		public final String id;
		private Type type;
		public void setType(Type t) {
			this.type = t;
		}
		public Identifier(Token t) {
			super(t.start);
			id = t.literal;
		}
		
		@Override
		public boolean equals(Object obj) {
			return super.equals(obj) || type.equals(obj);
		}
		
		@Override
		public String toString() {
			return super.toString() + "(" + type.toString() + ")";
		}
	}

	public static class Int extends Type {
		public Int(Position t) {
			super(t);
		}
	}

	public static class Str extends Type {
		public Str(Position t) {
			super(t);
		}
	}

	public static class Bool extends Type {
		public Bool(Position t) {
			super(t);
		}
	}

	public static class Void extends Type {
		public Void(Position t) {
			super(t);
		}
	}

	public static class Arr extends Type {
		public final Type arrType;
		public final int size;

		public Arr(Type arrType, int size) {
			super(arrType != null ? arrType.pos : new Position(0, 0));
			this.arrType = arrType;
			this.size = size;
		}
		
		@Override
		public String toString() {
			return arrType.toString() + "[]";
		}
	}

	public static class Function extends Type {
		public final Type returnType;
		public final Type[] paramTypes;
		public final String[] paramID;
		public final boolean isVarArg;
		
		public Function(final Type returnType) {
			super(returnType.pos);
			
			this.returnType = returnType;
			this.paramTypes = new Type[0];
			this.paramID = new String[0];
			isVarArg = true;
		}

		public Function(final Type returnType, String[] paramID, Type[] paramTypes) {
			super(returnType.pos);
			
			this.returnType = returnType;
			this.paramTypes = paramTypes;
			this.paramID = paramID;
			isVarArg = false;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("(");
			for(int i = 0; i < paramTypes.length; i++) {
				if(i > 0) {
					sb.append(", ");
				}
				sb.append(paramTypes[i].toString());
			}
			sb.append(") -> ");
			sb.append(returnType.toString());
			
			return sb.toString();
		}
	}
}