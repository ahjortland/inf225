package language.TypeChecker.walks;

import static frontend.syntax.nuthatch.SyntaxTree.*;
import static nuthatch.library.JoinPoints.down;

import java.util.ArrayList;
import java.util.List;

import nuthatch.library.BaseWalk;
import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.environment.Environment;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;

public class DeclarationWalk extends BaseWalk<SyntaxTreeWalker> {
	private Environment<Type> environment;
	private boolean hasWalked = false;

	public DeclarationWalk(Environment<Type> env) {
		this.environment = env;
	}

	public Environment<Type> walk(SyntaxTree root) {
		if(!hasWalked) {
			hasWalked = true;
			SyntaxTreeCursor cursor = new SyntaxTreeCursor(root);
			SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor , this);
			walker.start();
		}
		return environment;
	}

	@Override
	public int step(SyntaxTreeWalker w) {
		if(w.isAtRoot()) {
			return NEXT;
		}
		if (down(w)) {
			if(w.getType() instanceof language.production.Rewrite) {
				return PARENT;
			}
			Type t = Type.getType(getFirstToken(QueryFirst(w, 
					language.production.Type_Specifier.class)));
			
			if(w.getType() instanceof language.production.Function_Declaration) {
				String id = getFirstToken(QueryFirst(w, language.production.Identifier.class)).literal;
				SyntaxTreeCursor varArgs = QueryFirst(w, language.production.Params.class, language.production.VarArg.class);
				if(varArgs != null) {
					Type.Function fun = new Type.Function(t);
					w.setMetadata("type", fun);
					environment.declare(id, fun);
				} else {
					List<SyntaxTreeCursor> paramNode = Query(w, language.production.Params.class, language.production.Param.class);
					ParamDecl params = new ParamDecl();
					for(int i = 0; i < paramNode.size(); i++) {
						SyntaxTreeCursor vertex = paramNode.get(i);
						if(!vertex.isAtLeaf() && !(vertex.getData() instanceof language.tokens.types.Void)) {
							params.ids.add(getFirstToken(QueryFirst(vertex, language.production.Identifier.class)).literal);

							params.params.add(Type.getType(QueryFirst(vertex, 
									language.production.Type_Specifier.class).getBranch(1).getData()));
						}
					}
					Type.Function fun = new Type.Function(t, params.getID(), params.getParam());
					w.setMetadata("type", fun);
					environment.declare(id, fun);
				}
			} else {
				SyntaxTreeCursor variable = QueryFirst(w, language.production.Variable.class);
				if(variable == null) { // Assignment
					variable = QueryFirst(w, language.production.Assign.class,
							language.production.Variable.class);
				}
				String id = getFirstToken(QueryFirst(variable, language.production.Identifier.class)).literal;
				SyntaxTreeCursor array = QueryFirst(variable, language.production.Array.class);
				if(array != null) {
					SyntaxTreeCursor assign = QueryFirst(w, language.production.Assign.class);
					if(assign != null) {
						throw new TypeError("Cannot assign to an array declaration", getFirstToken(assign).start);
					}
					SyntaxTreeCursor size = QueryFirst(array, language.production.Expression.class,
							language.production.Number.class);
					if(size == null) {
						throw new TypeError("Cannot declare dynamically sized array", getFirstToken(w).start);
					}
					Type arrType = new Type.Arr(t, Integer.parseInt(getFirstToken(size).literal));
					w.setMetadata("type", arrType);
					environment.declare(id, arrType);
				} else {
					w.setMetadata("type", t);
					environment.declare(id, t);
				}
			}
			return PARENT;
		}
		return NEXT;
	}



	private class ParamDecl {
		public final ArrayList<String> ids;
		public final ArrayList<Type> params;
		public ParamDecl() {
			this.params = new ArrayList<>();
			this.ids = new ArrayList<>();
		}

		public Type[] getParam() {
			Type[] param = new Type[params.size()];
			for(int i = 0; i < param.length; i++) {
				param[i] = params.get(i);
			}
			return param;
		}
		public String[] getID() {
			String[] id = new String[ids.size()];
			for(int i = 0; i < id.length; i++) {
				id[i] = ids.get(i);
			}
			return id;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < params.size(); i++) {
				sb.append(ids.get(i) + ": " + params.get(i) + "\n");
			}
			return sb.toString();
		}
	}
}
