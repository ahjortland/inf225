package language.Rewrites;

import frontend.syntax.nuthatch.SyntaxTree;

public interface ASTRewriter {
	void rewrite(SyntaxTree input);
}
