package language.Rewrites;

import java.util.LinkedList;
import java.util.List;

import static nuthatch.library.JoinPoints.*;
import static frontend.syntax.nuthatch.SyntaxTree.*;
import frontend.syntax.nuthatch.ProductionPattern;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;
import nuthatch.library.BaseWalk;

public class RewriteEngine implements ASTRewriter {
	private SyntaxTree ast;
	private List<SyntaxTreeCursor> rewriteRules;

	public RewriteEngine() {
	}

	public void rewrite(SyntaxTree tree) {
		this.ast = tree;
		this.rewriteRules = new LinkedList<SyntaxTreeCursor>();
		rewriteRules.addAll(Query(ast, language.production.Rewrite.class));
		ProductionPattern pattern = new ProductionPattern(language.production.Rewrite.class).invertPattern();
		SyntaxTreeCursor cursor = pattern.build(new SyntaxTreeCursor(ast));
		
		RewriteWalker rewrite = new RewriteWalker();
		SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor, rewrite);
		walker.start();
	}

	private class RewriteWalker extends BaseWalk<SyntaxTreeWalker> {
		@Override
		public int step(SyntaxTreeWalker walker) {
			if(down(walker)) {
				for(SyntaxTreeCursor rule : rewriteRules) {
					SyntaxTreeCursor matchWith = (SyntaxTreeCursor) rule.getBranch(1);
					SyntaxTreeCursor replaceWith = (SyntaxTreeCursor) rule.getBranch(2);
					if(walker.subtreeEquals(matchWith)) {
						walker.replace(replaceWith);
						return PARENT;
					}
				}
			}
			return NEXT;
		}
	}
}
