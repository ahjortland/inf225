package language.CodeGeneration.ARMGenerator;

import static nuthatch.library.JoinPoints.*;
import static frontend.syntax.nuthatch.SyntaxTree.*;
import language.CodeGeneration.ARMGenerator.ARM.ARM;
import language.CodeGeneration.ARMGenerator.ARM.ARMStack;
import language.CodeGeneration.ARMGenerator.ARM.Address;
import language.CodeGeneration.ARMGenerator.ARM.Condition;
import language.CodeGeneration.ARMGenerator.ARM.Operand;
import language.CodeGeneration.ARMGenerator.ARM.Register;
import frontend.environment.Environment;
import frontend.syntax.Production;
import frontend.syntax.nuthatch.ProductionPattern;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;
import nuthatch.library.BaseWalk;

public class StatementGenerator implements frontend.CodeGenerator {
	private StringBuilder assembly;

	private final Environment<Address> environment;
	private final ARMStack stack;
	private final SyntaxTreeCursor cursor;
	private final Address endFun;

	public StatementGenerator(Environment<Address> environment, ARMStack stack, Address endFun, SyntaxTreeCursor cursor) {
		this.environment = environment;
		this.stack = stack;
		this.cursor = (SyntaxTreeCursor) cursor.copySubtree();
		this.endFun = endFun;
	}

	@Override
	public String generate() {
		assembly = new StringBuilder();
		SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor, new StatementWalk());
		walker.start();
		return assembly.toString();
	}

	private class StatementWalk extends BaseWalk<SyntaxTreeWalker> {
		@Override
		public int step(SyntaxTreeWalker w) {
			Production prod = w.getType();
			if(down(w)) {
				if(w.match(new ProductionPattern(language.production.Compound_Statement.class), null)) {
					assembly.append(ARM.Comment("Entering scope"));
					environment.enterScope();
					return NEXT;
				} else if(w.match(new ProductionPattern(language.production.Variable_Declaration.class), null)) {
					boolean assign = false;
					SyntaxTreeCursor variable = QueryFirst(w, language.production.Variable.class);
					if(variable == null) {
						assign = true;
						variable = QueryFirst(w, 
								language.production.Assign.class,
								language.production.Variable.class);
					}
					
					// Declare the variable
					String name = getFirstToken(QueryFirst(variable, language.production.Identifier.class)).literal;
					SyntaxTreeCursor arrSize = QueryFirst(variable, 
							language.production.Array.class, 
							language.production.Expression.class,
							language.production.Number.class);
					if(arrSize != null) {
						int size = Integer.parseInt(getFirstToken(arrSize).literal);
						
						assembly.append(stack.declare(size + 1, name, environment));
					} else {
						assembly.append(stack.declare(1, name, environment));
						ARM.MOV(Register.exprReg, new Operand(0));
					}
					// Assign the variable
					if(assign) {
						SyntaxTreeCursor expr = QueryFirst(w, language.production.Assign.class);
						frontend.CodeGenerator exprGen = new ExprGenerator(stack, environment, expr);
						assembly.append(exprGen.generate());
					}
				} else if(w.match(new ProductionPattern(language.production.Return.class), null)) {
					SyntaxTreeCursor expression = QueryFirst(w, language.production.Expression.class);
					if(expression != null) {
						frontend.CodeGenerator exprGenerator = new ExprGenerator(stack, environment, expression);
						assembly.append(exprGenerator.generate());
					}
					if(Register.returnReg != Register.exprReg) {
						assembly.append(ARM.MOV(Register.returnReg, Register.exprReg));
					}
					assembly.append(ARM.Branch(endFun));
				} else if(w.match(new ProductionPattern(language.production.While.class), null)) {
					Address startWhile = CodeGenerator.getNextLabel();
					Address endWhile = CodeGenerator.getNextLabel();
					SyntaxTreeCursor statementNode = (SyntaxTreeCursor) w.getBranch(2);
					frontend.CodeGenerator statement = new StatementGenerator(environment, stack, endFun, statementNode);
					SyntaxTreeCursor conditionNode = QueryFirst(w, language.production.Expression.class);
					frontend.CodeGenerator condition = new ExprGenerator(stack, environment, conditionNode);
					
					assembly.append(ARM.Branch(endWhile));
					assembly.append(ARM.label(startWhile));
					assembly.append(statement.generate());
					assembly.append(ARM.label(endWhile));
					assembly.append(condition.generate());
					assembly.append(ARM.CMP(Register.exprReg, CodeGenerator.TRUE));
					assembly.append(ARM.Branch(Condition.Equal, startWhile));
				} else if(w.match(new ProductionPattern(language.production.If.class), null)) {
					SyntaxTreeCursor conditionNode = QueryFirst(w, language.production.Expression.class);
					frontend.CodeGenerator condition = new ExprGenerator(stack, environment, conditionNode);
					assembly.append(condition.generate());
					assembly.append(ARM.CMP(Register.exprReg, CodeGenerator.TRUE));
					Address label = CodeGenerator.getNextLabel(); 
					assembly.append(ARM.Branch(Condition.NotEqual, label));

					SyntaxTreeCursor ifStat = (SyntaxTreeCursor) w.getBranch(2);
					frontend.CodeGenerator ifWalker = new StatementGenerator(environment, stack, endFun, ifStat);
					assembly.append(ifWalker.generate());
					if(w.getNumChildren() == 3) {
						// IF-else
						Address endIf = CodeGenerator.getNextLabel();
						assembly.append(ARM.Branch(endIf));
						assembly.append(ARM.label(label));
						SyntaxTreeCursor elseStat = (SyntaxTreeCursor) w.getBranch(3);
						frontend.CodeGenerator elseWalker = new StatementGenerator(environment, stack, endFun, elseStat);
						assembly.append(elseWalker.generate());
						assembly.append(ARM.label(endIf));
					} else {
						assembly.append(ARM.label(label));
					}
				} else if(prod instanceof language.production.Expression) {
					frontend.CodeGenerator expression = new ExprGenerator(stack, environment, (SyntaxTreeCursor) w.copy());
					assembly.append(expression.generate());
				}
				return PARENT;
			} else if(up(w)) {
				if(w.match(new ProductionPattern(language.production.Compound_Statement.class), null)) {
					assembly.append(stack.deallocate(environment.scopeSize()));
					assembly.append(ARM.Comment("Exited scope"));
					environment.exitScope();
					return NEXT;
				}
				throw new Error("You shall not get here" + w.getType());
			}
			return NEXT;
		}
	}
}
