package language.CodeGeneration.ARMGenerator;

import static nuthatch.library.JoinPoints.*;
import static frontend.syntax.nuthatch.SyntaxTree.*;

import java.util.List;

import language.CodeGeneration.ARMGenerator.ARM.ARM;
import language.CodeGeneration.ARMGenerator.ARM.ARMStack;
import language.CodeGeneration.ARMGenerator.ARM.Address;
import language.CodeGeneration.ARMGenerator.ARM.Operand;
import language.CodeGeneration.ARMGenerator.ARM.Register;
import frontend.environment.Environment;
import frontend.syntax.Production;
import frontend.syntax.nuthatch.ProductionPattern;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;
import nuthatch.library.BaseWalk;

public class CodeGenerator implements frontend.CodeGenerator {
	private final SyntaxTree ast;
	private final StringBuilder assembly;
	public static final Operand TRUE = new Operand(1);
	public static final Operand FALSE = new Operand(0);

	public final int alignment = 4;
	public Address endFun;
	public int padding;
	public final ARMStack stack;
	
	public Environment<Address> environment;

	private static int labels = 0;
	public static Address getNextLabel() {
		return new Address(".L" + labels++);
	}

	public CodeGenerator(SyntaxTree ast) {
		stack = new ARMStack(alignment);

		assembly = new StringBuilder();
		environment = new Environment<Address>();
		this.ast = ast;
	}

	public String generate() {
		// I cheated and implemented a read function myself. (since scanf uses pointers)
		assembly.append("    .balign 4\n");
		assembly.append(".number:\n");
		assembly.append("    .asciz  \"%d\"\n");
		assembly.append("    .text\n");
		assembly.append("    .balign  4\n");
		assembly.append("    .global read\n");
		assembly.append("read:\n");
		assembly.append("    stmfd   sp!, {fp, lr}\n");
		assembly.append("    add     fp, sp, #4\n");
		assembly.append("    sub     sp, sp, #8\n");
		assembly.append("    mov     r3, #0\n");
		assembly.append("    str     r3, [fp, #-8]\n");
		assembly.append("    sub     r3, fp, #8\n");
		assembly.append("    ldr     r0, .addr_number\n");
		assembly.append("    mov     r1, r3\n");
		assembly.append("    bl      scanf\n");
		assembly.append("    ldr     r3, [fp, #-8]\n");
		assembly.append("    mov     r0, r3\n");
		assembly.append("    sub     sp, fp, #4\n");
		assembly.append("    ldmfd   sp!, {fp, pc}\n");
		assembly.append("    .balign  4\n");
		assembly.append(".addr_number:\n");
		assembly.append("    .word   .number\n");
		
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(ast);
		SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor, new CodeWalker());
		walker.start();
		return assembly.toString();
	}

	private String globVarDecl(SyntaxTreeCursor w) {
		StringBuilder varDecl = new StringBuilder();
		SyntaxTreeCursor varVertex = QueryFirst(w,
				language.production.Variable.class);
		if(varVertex == null) {
			// Assignment
			varVertex = QueryFirst(w, language.production.Assign.class,
					language.production.Variable.class);
		}
		SyntaxTreeCursor value = QueryFirst(w, 
				language.production.Assign.class,
				language.production.Expression.class);
		SyntaxTreeCursor array = QueryFirst(varVertex, language.production.Array.class);
		String id = getFirstToken(QueryFirst(varVertex, language.production.Identifier.class)).literal;

		environment.declare(id, new Address(id));

		Address identifier = new Address(id);

		if(array != null) {
			int index = Integer.parseInt(getFirstToken(array).literal);
			varDecl.append(ARM.declareArr(identifier, index, alignment));
		} else if(value != null) {
			if(QueryFirst(value, language.production.Number.class) != null) {
				int number = Integer.parseInt(getFirstToken(value).literal);
				varDecl.append(ARM.declareVar(identifier, number, alignment));
			} else if(QueryFirst(value, language.production.String.class) != null) {
				String strval = getFirstToken(value).literal;
				varDecl.append(ARM.declareVar(identifier, strval, alignment));
			} else {
				String boolval = getFirstToken(value).literal;
				if(boolval.equals("true")) {
					varDecl.append(ARM.declareVar(identifier, Integer.parseInt(TRUE.GetOperand()), alignment));
				} else {
					varDecl.append(ARM.declareVar(identifier, Integer.parseInt(FALSE.GetOperand()), alignment));
				}
			}
		} else {
			varDecl.append(ARM.declareVar(identifier, 0, alignment));
		}
		return varDecl.toString();
	}

	private void getAllStrings() {
		BaseWalk<SyntaxTreeWalker> walk = new  BaseWalk<SyntaxTreeWalker>() {
			@Override
			public int step(SyntaxTreeWalker w) {
				if(w.getType() instanceof language.production.Variable_Declaration) {
					return PARENT;
				} else if(w.getType() instanceof language.production.String) {
					String key = getFirstToken(w).literal;
					Address label = getNextLabel();
					if(!environment.contains(key)) {
						environment.declare(key, label);
					}
					assembly.append(ARM.declareVar(label, key, alignment));
					return PARENT;
				}
				return NEXT;
			}
		};
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(ast);
		SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor, walk);

		walker.start();
	}

	private class CodeWalker extends BaseWalk<SyntaxTreeWalker> {
		@Override
		public int step(SyntaxTreeWalker w) {
			Production prod = w.getType();
			if(down(w)) {
				if(w.match(new ProductionPattern(language.production.Rewrite.class), null)) {
					return PARENT;
				}
				if(w.match(new ProductionPattern(language.production.Program.class), null)) {
					for(SyntaxTreeCursor tree : Query(w, language.production.Variable_Declaration.class)) {
						assembly.append(globVarDecl((SyntaxTreeCursor) tree.copy()));
					}
					getAllStrings();
					return NEXT;
				} else if(w.match(new ProductionPattern(language.production.Variable_Declaration.class), null)) {
					return PARENT;
				} else if(w.match(new ProductionPattern(language.production.Function_Declaration.class), null)) {
					String identifier = getFirstToken(QueryFirst(w, language.production.Identifier.class)).literal;
					if(QueryFirst(w, language.production.Compound_Statement.class) == null) {
						assembly.append(ARM.globalFun(identifier));
						return PARENT;
					}
					endFun = new Address(String.format(".end_%s", identifier));

					assembly.append(ARM.declareFun(identifier));
					Register[] pushRegs = {
							Register.framePointer,
							Register.linkRegister,
					};
					assembly.append(stack.push(pushRegs));
					assembly.append(ARM.ADD(Register.framePointer, Register.stackPointer, new Operand((pushRegs.length - 1) * alignment)));
					environment.enterScope();
					// Declare Variables
					List<SyntaxTreeCursor> parameters = Query(w, 
							language.production.Params.class,
							language.production.Param.class);
					// Go through in reverse and push the variable on the stack
					int index = 0;
					for(SyntaxTreeCursor subtree : parameters) {
						String paramID = getFirstToken(QueryFirst(subtree, language.production.Identifier.class)).literal;
						if(index < 4) {
							assembly.append(stack.declare(1, paramID, environment));
							assembly.append(ARM.Store(Register.registers[index], environment.get(paramID)));
						} else {
							int relIndex = index - 4;
							Address addr = new Address(Register.framePointer, (relIndex + 1) * alignment);
							environment.declare(paramID, addr);
						}
						index++;
					}

					// Find the block statement and go down that subtree
					SyntaxTreeCursor nextTree = QueryFirst(w, language.production.Compound_Statement.class);
					frontend.CodeGenerator statement = new StatementGenerator(environment, stack, endFun, nextTree);
					assembly.append(statement.generate());
					assembly.append(ARM.label(endFun));
					// stackSize == 0 after this
					assembly.append(ARM.SUB(Register.stackPointer, Register.framePointer, new Operand((pushRegs.length - 1) * alignment)));
					assembly.append(stack.pop(Register.framePointer, Register.programCounter));
					stack.resetStackSize();
					assembly.append("\n");
					environment.exitScope();
					return PARENT;
				} else {
					if(prod != null) {
						System.out.println(prod);
					} else {
						System.out.println("token: " + w.getData());
					}
				}
			}
			return NEXT;
		}
	}
}
