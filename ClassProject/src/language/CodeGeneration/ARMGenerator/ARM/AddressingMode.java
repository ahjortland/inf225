package language.CodeGeneration.ARMGenerator.ARM;

public class AddressingMode {
	public static AddressingMode IncrementAfter  = new AddressingMode("ia");
	public static AddressingMode IncrementBefore = new AddressingMode("ib");
	public static AddressingMode DecrementAfter  = new AddressingMode("da");
	public static AddressingMode DecrementBefore = new AddressingMode("db");
	public static AddressingMode FullDescending  = new AddressingMode("fd");
	public static AddressingMode EmptyDescending = new AddressingMode("ed");
	public static AddressingMode FullAscending   = new AddressingMode("fa");
	public static AddressingMode EmptyAscending  = new AddressingMode("ea");
	
	private final String mode;
	private AddressingMode(String mode) {
		this.mode = mode;
	}
	
	public String getMode() {
		return mode;
	}
	
	@Override
	public String toString() {
		return getMode();
	}
}
