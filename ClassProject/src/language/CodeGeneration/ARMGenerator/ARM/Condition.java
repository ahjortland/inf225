package language.CodeGeneration.ARMGenerator.ARM;

public class Condition {
	public static Condition Equal        = new Condition("EQ");
	public static Condition NotEqual     = new Condition("NE");
	public static Condition Greater      = new Condition("GT");
	public static Condition GreaterEqual = new Condition("GE");
	public static Condition Less         = new Condition("LT");
	public static Condition LessEqual    = new Condition("LE");
	
	private final String type;
	private Condition(String type) {
		this.type = type;
	}
	
	public String conditionString() {
		return type;
	}
	
	@Override
	public String toString() {
		return type;
	}
}
