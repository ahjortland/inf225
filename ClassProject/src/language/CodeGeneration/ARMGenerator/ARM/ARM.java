package language.CodeGeneration.ARMGenerator.ARM;

import entry.Main;

public class ARM {
	private static final int tabWidth = 4;
	private static final String tab = String.format("%-" + tabWidth + "s", "");
	
	private static String format(String format, Object...values) {
		return tab + formatNoTab(format, values);
	}
	private static String formatNoTab(String format, Object...values) {
		return String.format(format + '\n', values);
	}
	
	private static String form1 = "%s %s";
	private static String form2 = "%s %s, %s";
	private static String form3 = "%s %s, %s, %s";
	
	public static String Comment(String comment) {
		if(Main.isVerbose()){
			return format("@ %s", comment);
		}
		return "";
	}
	
	public static String MOV(Register destination, Operand op) {
		if(op.isLdr()) {
			return format(form2, "ldr", destination, op);
		}
		return format(form2, "mov", destination, op);
	}
	public static String MOV(Register destination, Condition cond, Operand op) {
		if(op.isLdr()) {
			return format(form2, "ldr" + cond, destination, op);
		}
		return format(form2, "mov" + cond, destination, op);
	}
	
	/*
	 * Arithmetic Operations
	 */
	public static String ADD(Register destination, Register base, Operand op) {
		return format(form3, "add", destination, base, op);
	}
	
	public static String ADD(Register destination, Operand op) {
		return format(form2, "add", destination, op);
	}
	
	public static String SUB(Register destination, Register base, Operand op) {
		return format(form3, "sub", destination, base, op);
	}
	
	public static String SUB(Register destination, Operand op) {
		return format(form2, "sub", destination, op);
	}
	
	public static String MUL(Register destination, Register base, Register op) {
		return format(form3, "mul", destination, base, op);
	}
	
	public static String MUL(Register destination, Register op) {
		return format(form2, "mul", destination, op);
	}
	/*
	 * End arithmetic operations
	 */
	
	/*
	 * Logical operations
	 */
	public static String CMP(Register base, Operand op) {
		return format(form2, "cmp", base, op);
	}
	/*
	 * End logical operations
	 */
	
	/*
	 * Branching operations
	 */
	public static String Branch(Condition cond, Address label) {
		return format(form1, "b" + cond, label);
	}
	
	public static String Branch(Address label) {
		return format(form1, "b", label);
	}
	
	public static String BranchLink(Condition cond, Address label) {
		return format(form1, "bl" + cond, label);
	}
	
	public static String BranchLink(Address label) {
		return format(form1, "bl", label);
	}
	
	/*
	 * End branching operations
	 */
	
	/*
	 * Memory operations
	 */
	public static String Load(Register reg, Address from) {
		StringBuilder sb = new StringBuilder();
		sb.append(format(form2, "ldr", reg, from));
		return sb.toString();
	}
	
	public static String LoadMultiple(Register reg, boolean update, AddressingMode mode, Register...from) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for(int i = 0; i < from.length; i++) {
			if(i > 0) {
				sb.append(", ");
			}
			sb.append(from[i]);
		}
		sb.append("}");
		
		return format("ldm%s %s%s, %s", mode, reg, update ? "!" : "", sb.toString());
	}
	
	public static String Store(Register reg, Address from) {
		return format(form2, "str", reg, from);
	}
	
	public static String StoreMultiple(Register reg, boolean update, AddressingMode mode, Register...into) {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		for(int i = 0; i < into.length; i++) {
			if(i > 0) {
				sb.append(", ");
			}
			sb.append(into[i]);
		}
		sb.append("}");
		
		return format("stm%s %s%s, %s", mode, reg, update ? "!" : "", sb.toString());
	}
	/*
	 * End memory operations
	 */
	
	private static String declare(Address name, String init, int alignment) {
		StringBuilder sb = new StringBuilder();
		sb.append(format(".global .%s", name));
		sb.append(format(".data"));
		sb.append(format(".balign %d", alignment));
		sb.append(formatNoTab(".%s:", name));
		sb.append(init);
		sb.append("\n");
		sb.append(format(".text"));
		sb.append(formatNoTab("%s:", name));
		sb.append(format(".word .%s", name));
		sb.append("\n");
		return sb.toString();
	}
	
	public static String declareVar(Address name, int value, int alignment) {
		return declare(name, format(".word %s", value), alignment);
	}
	
	public static String declareArr(Address name, int elements, int alignment) {
		return declare(name, format(".skip %s", elements*alignment), alignment);
	}
	
	public static String declareVar(Address name, String value, int alignment) {
		return declare(name, format(".asciz %s", value), alignment);
	}
	
	public static String globalFun(String name) {
		return format(".global %s", name);
	}
	
	public static String declareFun(String name) {
		StringBuilder sb = new StringBuilder();
		sb.append(globalFun(name));
		sb.append(format(".text", name));
		sb.append(formatNoTab("%s:", name));
		
		return sb.toString();
	}
	
	public static String label(Address label) {
		return formatNoTab("%s:", label);
	}
}
