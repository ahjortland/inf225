package language.CodeGeneration.ARMGenerator.ARM;

public class Address {
	public final Register regAddr;
	private final Register regAddr2;
	private final String address;
	public final int offset;
	public final boolean isLabel;
	
	private Address(String address, Register regAddr, Register regAddr2, int offset, boolean isLabel) {
		this.address = address;
		this.regAddr = regAddr;
		this.regAddr2 = regAddr2;
		this.offset = offset;
		this.isLabel = isLabel;
	}
	
	public Address(Register regAddr, Register regAddr2) {
		this(null, regAddr, regAddr2, 0, false);
	}
	
	public Address(Register base, int offset) {
		this(null, base, null, offset, false);
	}
	
	public Address(Register base) {
		this(base, 0);
	}
	
	public Address(String label) {
		this(label, null, null, 0, true);
	}
	
	public String address() {
		String reg;
		if(address == null) {
			reg = regAddr.getName();
		} else {
			reg = address;
		}
		if(!isLabel) {
			if(regAddr2 != null) {
				return String.format("[%s, %s]", reg, regAddr2);
			} else {
				return String.format("[%s, #%d]", reg, offset);
			}
		}
		return reg;
	}
	
	public String compute() {
		ARM.ADD(Register.exprReg, new Operand(offset));
		return "";
	}
	
	public Address offset(int offset) {
		return new Address(this.address, this.regAddr, this.regAddr2, this.offset + offset, isLabel);
	}
	
	public Address offset(Register offset) {
		return new Address(this.address, this.regAddr, offset, this.offset, isLabel);
	}
	
	@Override
	public String toString() {
		return address();
	}
}
