package language.CodeGeneration.ARMGenerator.ARM;

import exceptions.IllegalAllignmentError;
import frontend.environment.Environment;

public class ARMStack {
	private int stackSize = 0;
	private int padding = 0;
	public final int alignment;
	
	
	public ARMStack(int alignment) {
		this.alignment = alignment;
	}
	
	public int getSize() {
		return stackSize;
	}
	
	/**
	 * You'd better be sure you actually have reset the stack before calling
	 * this
	 */
	public void resetStackSize() {
		padding = stackSize = 0;
	}
	
	public String declare(int words, String identifier, Environment<Address> env) {
		int startPos = -1 * (((words-1)+stackSize)*alignment);
		env.declare(identifier, new Address(Register.framePointer, startPos));
		
		return allocate(words);
	}
	
	public String allocate(int words) {
		if(words > 0) {
			stackSize += words;
			return ARM.Comment("allocating") + ARM.SUB(Register.stackPointer, new Operand(words * alignment));
		}
		return "";
	}

	public String deallocate(int words) {
		if(words > 0) {
			stackSize -= words;
			return ARM.Comment("deallocating") + ARM.ADD(Register.stackPointer, new Operand(words * alignment));
		}
		return "";
	}

	/**
	 * Cannot call @link{CodeGenerator.dealign} before any scope changes
	 *  
	 * @param alignTo Size which we are to align the stack
	 * @return The assembly string which will align the stack
	 */
	public String align(int alignTo) {
		if(alignTo % alignment != 0) {
			throw new IllegalAllignmentError("Cannot allign " + this.alignment + " to " + alignment);
		}
		int diff = alignTo / alignment;
		
		padding += (diff - (stackSize % diff)) % diff;
		if(padding > 0) {
			stackSize += padding;
			Operand op = new Operand(padding*alignment);
			return ARM.Comment("aligning") + ARM.SUB(Register.stackPointer, op);
		}
		return "";
	}

	/**
	 * Cannot call @link{CodeGenerator.align} before any scope changes
	 *  
	 * @return The assembly string which will dealign the stack 
	 */
	public String dealign() {
		if(padding > 0) {
			stackSize -= padding;
			Operand spDiff = new Operand(padding*alignment);
			padding = 0;
			return ARM.Comment("dealigning") + ARM.ADD(Register.stackPointer, spDiff) ;
		}
		return "";
	}

	public String push(Register...pushFrom) {
		stackSize += pushFrom.length;
		return ARM.StoreMultiple(Register.stackPointer, true, AddressingMode.FullDescending, pushFrom);
	}

	public String pop(Register...popInto) {
		stackSize -= popInto.length;
		return ARM.LoadMultiple(Register.stackPointer, true, AddressingMode.FullDescending, popInto);
	}
}
