package language.CodeGeneration.ARMGenerator.ARM;

public class Register extends Operand {
	public static final Register[] registers = { 
			new Register("r0"), 
			new Register("r1"), 
			new Register("r2"), 
			new Register("r3"), 
			new Register("r4"), 
			new Register("r5"), 
			new Register("r6"), 
			new Register("r7"), 
			new Register("r8"), 
			new Register("r9"), 
			new Register("r10"), 
			new Register("fp"), 
			new Register("r12"), 
			new Register("sp"), 
			new Register("lr"), 
			new Register("pc"),
	};

	public static final Register returnReg = registers[0];
	public static final Register exprReg = registers[0];
	public static final Register opReg = registers[1];
	public static final Register framePointer = registers[11];
	public static final Register stackPointer = registers[13];
	public static final Register linkRegister = registers[14];
	public static final Register programCounter = registers[15];

	private Register(String name) {
		super(name);
	}

	public String getName() {
		return super.GetOperand();
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
