package language.CodeGeneration.ARMGenerator.ARM;

public class Operand {
	private final String operand;
	private final boolean isLdr;
	protected Operand(String operand) {
		isLdr = false;
		this.operand = operand;
	}
	
	public Operand(int number) {
		isLdr = number > 255; // mov only support up to 8-bit numbers
		if(isLdr) {
			this.operand = String.format("=%d", number);
		} else {
			this.operand = String.format("#%d", number);
		}
	}
	
	public boolean isLdr() {
		return isLdr;
	}
	
	public String GetOperand() {
		return operand;
	}
	
	@Override
	public String toString() {
		return GetOperand();
	}
}
