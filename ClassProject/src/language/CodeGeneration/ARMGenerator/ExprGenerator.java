package language.CodeGeneration.ARMGenerator;


import static nuthatch.library.JoinPoints.*;
import static frontend.syntax.nuthatch.SyntaxTree.*;

import java.util.List;
import java.util.Stack;

import language.CodeGeneration.ARMGenerator.ARM.ARM;
import language.CodeGeneration.ARMGenerator.ARM.ARMStack;
import language.CodeGeneration.ARMGenerator.ARM.Address;
import language.CodeGeneration.ARMGenerator.ARM.Condition;
import language.CodeGeneration.ARMGenerator.ARM.Operand;
import language.CodeGeneration.ARMGenerator.ARM.Register;
import nuthatch.library.BaseWalk;
import frontend.environment.Environment;
import frontend.syntax.nuthatch.ProductionPattern;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;

public class ExprGenerator implements frontend.CodeGenerator {
	private StringBuilder assembly;
	
	private final Environment<Address> environment;
	private final ARMStack stack;
	private final SyntaxTreeCursor cursor;
	public final Stack<Integer> allocated;
	private final Address divFun = new Address("__aeabi_idivmod");

	public ExprGenerator(ARMStack stack, Environment<Address> environment, SyntaxTreeCursor cursor) {
		this(stack, environment, cursor, 0);
	}
	
	private ExprGenerator(ARMStack stack, Environment<Address> environment, SyntaxTreeCursor cursor, int basePos) {
		this.stack = stack;
		this.cursor = (SyntaxTreeCursor) cursor.copySubtree();
		this.environment = environment;
		this.allocated = new Stack<Integer>();
	}

	public String generate() {
		assembly = new StringBuilder();
		SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor, new ExprWalker());
		walker.start();
		return assembly.toString();
	}
	
	private class ExprWalker extends BaseWalk<SyntaxTreeWalker> {
		@Override
		public int step(SyntaxTreeWalker w) {
			if(w.match(new ProductionPattern(language.production.Expression.class), null)) {
				return NEXT;
			}
			if(down(w)) {
				if(w.match(new ProductionPattern(language.production.Variable.class), null)) {
					Address label = getAddress((SyntaxTreeCursor) w.copy());
					assembly.append(ARM.Load(Register.exprReg, label));
				} else if(w.match(new ProductionPattern(language.production.Function_Call.class), null)) {
					Address identifier = new Address(getFirstToken(QueryFirst(w, language.production.Identifier.class)).literal);
					int allocated = 0;
					SyntaxTreeCursor args = QueryFirst(w, language.production.Args.class);
					if(args != null) {
						List<SyntaxTreeCursor> arguments = Query(args, 
								language.production.Argument.class);
						if(arguments.size() > 4) {
							int stackArgs = arguments.size() - 4;
							allocated = stackArgs;
							assembly.append(stack.allocate(allocated));
						}
						assembly.append(stack.align(8));
						int basePos = -1 * (stack.getSize() - 1) ;
						for(SyntaxTreeCursor arg : arguments) {
							assembly.append(new ExprGenerator(stack, environment, QueryFirst(arg, language.production.Expression.class)).generate());
							assembly.append(stack.push(Register.exprReg));
						}
						for (int i = arguments.size()-1; i >= 0; i--) {
							if(i >= 4) {
								int memLoc = (basePos + (i-4)) * stack.alignment;
								assembly.append(stack.pop(Register.exprReg));
								assembly.append(ARM.Store(Register.exprReg, new Address(Register.framePointer, memLoc)));
							} else {
								assembly.append(stack.pop(Register.registers[i]));
							}
						}
						frontend.CodeGenerator argAssembler = new ExprGenerator(stack, environment, args, basePos);
						assembly.append(argAssembler.generate());
					} else {
						assembly.append(stack.align(8));
					}
					assembly.append(ARM.BranchLink(identifier));
					assembly.append(stack.dealign());
					assembly.append(stack.deallocate(allocated));
				} else if(w.match(new ProductionPattern(language.production.Assign.class), null)) {
					SyntaxTreeCursor expr = QueryFirst(w, language.production.Expression.class);
					frontend.CodeGenerator exprWalk = new ExprGenerator(stack, environment, expr);
					assembly.append(exprWalk.generate());
					assembly.append(stack.push(Register.exprReg));
					Address name = getAddress(QueryFirst(w, language.production.Variable.class));
					assembly.append(stack.pop(Register.exprReg));
					assembly.append(ARM.Comment(String.format("saving variable '%s' at: %s", getFirstToken(QueryFirst(w, language.production.Variable.class)).literal, name)));
					assembly.append(ARM.Store(Register.exprReg, name));
				} else if(w.match(new ProductionPattern(language.production.Number.class), null)) {
					// Add the number to a register
					int number = Integer.parseInt(getFirstToken(w).literal);
					assembly.append(ARM.MOV(Register.exprReg, new Operand(number)));
				} else if(w.match(new ProductionPattern(language.production.Bool.class), null)) {
					String bool = getFirstToken(w).literal;
					if(bool.equals("true")) {
						assembly.append(ARM.MOV(Register.exprReg, CodeGenerator.TRUE));
					} else {
						assembly.append(ARM.MOV(Register.exprReg, CodeGenerator.FALSE));
					}
				} else if(w.match(new ProductionPattern(language.production.String.class), null)) {
					// Add the number to a register
					String string = getFirstToken(w).literal;
					Address op = environment.get(string);
					assembly.append(ARM.Load(Register.exprReg, op));
				} else if(w.match(new ProductionPattern(language.production.Add.class), null)) {
					assembly.append(binOp(w));
					assembly.append(ARM.ADD(Register.exprReg, Register.opReg));
				} else if(w.match(new ProductionPattern(language.production.Sub.class), null)) {
					assembly.append(binOp(w));
					assembly.append(ARM.SUB(Register.exprReg, Register.opReg));
				} else if(w.match(new ProductionPattern(language.production.Mul.class), null)) {
					assembly.append(binOp(w));
					assembly.append(ARM.MUL(Register.exprReg, Register.opReg));
				} else if(w.match(new ProductionPattern(language.production.Div.class), null)) {
					assembly.append(binOp(w));
					assembly.append(ARM.BranchLink(divFun));
				} else if(w.match(new ProductionPattern(language.production.Mod.class), null)) {
					assembly.append(binOp(w));
					assembly.append(ARM.BranchLink(divFun));
					assembly.append(ARM.MOV(Register.exprReg, Register.opReg));
				} else if(w.match(new ProductionPattern(
						language.production.Equal.class,
						language.production.NotEqual.class,
						language.production.GreaterEqual.class,
						language.production.Greater.class,
						language.production.LessEqual.class,
						language.production.Less.class
						), null)) {
					assembly.append(binOp(w));
					assembly.append(ARM.CMP(Register.exprReg, Register.opReg));
					Condition compare = null;
					if(w.match(new ProductionPattern(language.production.Equal.class), null)) {
						compare = Condition.Equal;
					} else if(w.match(new ProductionPattern(language.production.NotEqual.class), null)) {
						compare = Condition.NotEqual;
					} else if(w.match(new ProductionPattern(language.production.GreaterEqual.class), null)) {
						compare = Condition.GreaterEqual;
					} else if(w.match(new ProductionPattern(language.production.Greater.class), null)) {
						compare = Condition.Greater;
					} else if(w.match(new ProductionPattern(language.production.LessEqual.class), null)) {
						compare = Condition.LessEqual;
					} else if(w.match(new ProductionPattern(language.production.Less.class), null)) {
						compare = Condition.Less;
					}
					assembly.append(ARM.MOV(Register.exprReg, CodeGenerator.FALSE));
					assembly.append(ARM.MOV(Register.exprReg, compare, CodeGenerator.TRUE));
				}
				return PARENT;
			}
			throw new Error("Whut?");
		}
	}
	
	private Address getAddress(SyntaxTreeCursor w) {
		w = (SyntaxTreeCursor) w.copySubtree();
		
		SyntaxTreeCursor identifier = QueryFirst(w, language.production.Identifier.class);
		String key = getFirstToken(identifier).literal;
		Address label = environment.get(key);
		boolean global = label.isLabel;
		if(global) {
			assembly.append(ARM.Load(Register.opReg, label));
			label = new Address(Register.opReg);
		}
		SyntaxTreeCursor arrIndex = QueryFirst(w, language.production.Array.class, language.production.Expression.class);
		if(arrIndex != null) {
			if(global) {
				assembly.append(stack.push(Register.opReg));
				frontend.CodeGenerator expr = new ExprGenerator(stack, environment, arrIndex);
				assembly.append(expr.generate());
				assembly.append(ARM.MOV(Register.opReg, new Operand(stack.alignment)));
				assembly.append(ARM.MUL(Register.exprReg, Register.opReg));
				assembly.append(stack.pop(Register.opReg));
				assembly.append(ARM.ADD(Register.opReg, Register.exprReg));
				return new Address(Register.opReg);
			} else {
				frontend.CodeGenerator expr = new ExprGenerator(stack, environment, arrIndex);
				assembly.append(expr.generate());
				assembly.append(ARM.MOV(Register.opReg, new Operand(stack.alignment)));
				assembly.append(ARM.MUL(Register.exprReg, Register.opReg));
				assembly.append(ARM.ADD(Register.opReg, Register.exprReg, new Operand(label.offset)));
				return label.offset(Register.opReg);
			}
		}
		return label;
	}
	
	private String binOp(SyntaxTreeWalker w) {
		StringBuilder op = new StringBuilder();
		frontend.CodeGenerator left = new ExprGenerator(stack, environment, (SyntaxTreeCursor) w.getBranch(1));
		frontend.CodeGenerator right = new ExprGenerator(stack, environment, (SyntaxTreeCursor) w.getBranch(2));
		op.append(left.generate());
		op.append(stack.push(Register.exprReg));
		op.append(right.generate());
		op.append(ARM.MOV(Register.opReg, Register.exprReg));
		op.append(stack.pop(Register.exprReg));
		return op.toString();
	}
}
