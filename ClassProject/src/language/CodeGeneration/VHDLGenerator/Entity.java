package language.CodeGeneration.VHDLGenerator;

import static language.CodeGeneration.VHDLGenerator.HelperMethods.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import language.TypeChecker.Type;
import exceptions.TypeError;

public class Entity {
	private final static String template = HelperMethods.readFileFromPackage("language/CodeGeneration/VHDLGenerator/templates/entity.vhdl");

	private final String name;
	private final List<Port> ports;
	private String entityString = null;

	public String getName() {
		return name;
	}

	public Iterable<Port> getPorts() {
		return ports;
	}
	
	public int numPorts() {
		return ports.size();
	}

	public Entity(String name, Port...ports) {
		this.name = name;
		this.ports = new LinkedList<>();
		for(Port p : ports) {
			this.ports.add(p);
		}
	}
	
	public void addPort(Port port) {
		this.ports.add(port);
	}

	public static class Port {
		public final String name;
		public final DataTypes dataType;
		private final boolean inPort;
		private int size;

		public Port(String name, boolean inPort, Type dataType) {
			this.name = name;
			this.inPort = inPort;
			this.dataType = getType(dataType);
		}

		private DataTypes getType(Type dataType) {
			if (dataType instanceof Type.Bool) {
				this.size = 1;
				return DataTypes.STD_LOGIC;
			} else if (dataType instanceof Type.Str) {
				this.size = 1;
				return DataTypes.STRING;
			} else if (dataType instanceof Type.Int) {
				return DataTypes.INTEGER;
			} else if (dataType instanceof Type.Arr) {
				Type.Arr array = (Type.Arr) dataType;
				getType(array.arrType); // This is to calculate the size of the interior type
				this.size = array.size * Math.max(1, this.size);
				return DataTypes.STD_LOGIC_VECTOR;
			}
			throw new TypeError("Unsupported type: " + dataType, dataType.pos);
		}

		public String generateString() {
			String base = String.format("%s : %s %s", name, (inPort ? "in" : "out"), dataType); 
			switch(dataType) {
			case INTEGER:
			case STD_LOGIC:
			case STRING:
				return base;
			case STD_LOGIC_VECTOR:
				return String.format("%s(%d downto 0)", base, size - 1);
			}
			throw new Error("This should not happen");
		}
	}

	public enum DataTypes {
		STD_LOGIC, STD_LOGIC_VECTOR, STRING, INTEGER
	}

	@Override
	public String toString() {
		if(entityString == null) {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < ports.size(); i++) {
				if(i < ports.size() -1 ) {
					sb.append(ports.get(i).generateString() + ";\n");
				} else {
					sb.append(ports.get(i).generateString());
				}
			}
			entityString = template.replaceAll(namePattern, name);
			entityString = entityString.replaceAll(inoutPattern, sb.toString());
			entityString = entityString.trim();
		}
		return entityString + '\n';
	}
}
