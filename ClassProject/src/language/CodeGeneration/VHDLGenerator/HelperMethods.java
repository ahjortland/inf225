package language.CodeGeneration.VHDLGenerator;

import java.io.IOException;
import java.io.InputStream;

public final class HelperMethods {
	public static final String pattern          = "\\<\\+\\s+%s+\\s+\\+\\>";
	public static final String namePattern      = String.format(pattern, "name");
	public static final String datePattern      = String.format(pattern, "date");
	public static final String sourcePattern    = String.format(pattern, "source");
	public static final String archPattern      = String.format(pattern, "architectures");
	public static final String archNamePattern  = String.format(pattern, "arch");
	public static final String componentPattern = String.format(pattern, "components");
	public static final String entityPattern    = String.format(pattern, "entities");
	public static final String declsPattern     = String.format(pattern, "decls");
	public static final String concstatsPattern = String.format(pattern, "concstats");;
	public static final String inoutPattern     = String.format(pattern, "ports");
	public static final String importPattern     = String.format(pattern, "import");
	
	public static final String readFileFromPackage(String path) {
		try (InputStream s = ClassLoader.getSystemResourceAsStream(path)) {
			StringBuilder templateBuilder = new StringBuilder();
			int character;
			while ((character = s.read()) != -1) {
				templateBuilder.append((char) character);
			}
			return templateBuilder.toString();
		} catch (IOException e) {
			throw new Error(e);
		}
	}
}
