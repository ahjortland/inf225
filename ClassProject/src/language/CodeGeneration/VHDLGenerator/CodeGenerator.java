package language.CodeGeneration.VHDLGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.TimeZone;

import language.TypeChecker.Type;
import language.TypeChecker.Type.Str;
import nuthatch.library.BaseWalk;
import nuthatch.tree.TreeCursor;
import nuthatch.util.Pair;
import frontend.environment.Environment;
import frontend.syntax.Production;
import frontend.syntax.nuthatch.ProductionPattern;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.syntax.nuthatch.SyntaxTreeCursor;
import frontend.syntax.nuthatch.SyntaxTreeWalker;
import frontend.tokens.Position;
import frontend.tokens.Token;
import static language.CodeGeneration.VHDLGenerator.HelperMethods.*;
import static nuthatch.library.JoinPoints.*;

public class CodeGenerator implements frontend.CodeGenerator {
	private final SyntaxTree ast;

	private final String date;
	private final String source;
	private final String mainTemplate;
	private final String importTemplate;

	private final List<Component> components;
	private final List<Entity> entities;
	private final LinkedList<Architecture> architectures;

	public Environment<String> environment;

	public CodeGenerator(SyntaxTree ast) {
		environment = new Environment<String>();
		this.ast = ast;
		source = ast.getMetadata("source").toString();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		date = df.format(new Date());

		architectures = new LinkedList<>();
		components = new LinkedList<>();
		entities = new LinkedList<>();

		mainTemplate = HelperMethods.readFileFromPackage("language/CodeGeneration/VHDLGenerator/templates/header.vhdl");
		importTemplate = HelperMethods.readFileFromPackage("language/CodeGeneration/VHDLGenerator/templates/imports.vhdl");
	}

	public String generate() {
		String template = "";
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(ast);
		SyntaxTreeWalker walker = new SyntaxTreeWalker(cursor, new CodeWalker());
		walker.start();

		StringBuilder components = new StringBuilder();
		for(Component component : this.components) {
			components.append(component.toString());
		}

		StringBuilder entities = new StringBuilder();
		for(Entity entity : this.entities) {
			entities.append(entity.toString());
		}

		StringBuilder architectures = new StringBuilder();
		for(Architecture architecture : this.architectures) {
			architectures.append(architecture.toString());
		}


		template = mainTemplate.replaceAll(datePattern, date);
		template = template.replaceAll(sourcePattern, source);
		template = template.replaceAll(componentPattern, components.toString());
		template = template.replaceAll(entityPattern, entities.toString());
		template = template.replaceAll(archPattern, architectures.toString());
		//System.out.println("? " + architectures.toString());
		template = template.replaceAll(importPattern, importTemplate);
		return template.trim();
	}

	private class CodeWalker extends BaseWalk<SyntaxTreeWalker> {
		private Stack<List<String>> exprLvl = new Stack<>();
		
		@Override
		public int step(SyntaxTreeWalker w) {
			if(down(w)) {
				if(w.match(new ProductionPattern(language.production.Function_Declaration.class), null)) {
					String identifier = SyntaxTree.getFirstToken(SyntaxTree.QueryFirst(w, language.production.Identifier.class)).literal;
					SyntaxTree.Query(w, language.production.Params.class);
					SyntaxTreeCursor debug1 = SyntaxTree.QueryFirst(w, language.production.Type_Specifier.class);
					Object debug2 = debug1.getMetadata("type");
					Type t = (Type) debug2;
					TreeCursor<Token, Production> param = w.copy().go(3);
					Entity.Port p = new Entity.Port("funRet", false, t);
					Entity ent = new Entity(identifier, p);
					for(int i = 1; i <= param.getNumChildren(); i++) {
						SyntaxTreeCursor typeCursor = (SyntaxTreeCursor) param.copy().go(i).go(1);
						SyntaxTreeCursor idCursor   = (SyntaxTreeCursor) param.copy().go(i).go(2);
						Token type = SyntaxTree.getFirstToken(typeCursor);
						String id  = SyntaxTree.getFirstToken(idCursor).literal;
						ent.addPort(new Entity.Port(id, true, Type.getType(type)));
					}
					
					entities.add(ent);
					components.add(new Component(ent));
					
					SyntaxTreeCursor token = SyntaxTree.QueryFirst(w, language.production.Compound_Statement.class);
					architectures.add(new Architecture(ent.getName()));
					if(token != null) {
						return token.getLastPathElement();
					}
				}
				if(w.match(new ProductionPattern(language.production.Return.class), null)) {
					Architecture current = architectures.getLast();
					current.Append("funRet <= ");
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Number.class,
						language.production.Variable.class), null)) {
					exprLvl.peek().add(SyntaxTree.getFirstToken(w).literal);
					return PARENT;
				}
				if(w.match(new ProductionPattern(language.production.Expression.class,
						language.production.Sub.class,
						language.production.Add.class,
						language.production.Mul.class,
						language.production.Div.class,
						language.production.Mod.class), null)) {
					exprLvl.push(new LinkedList<String>());
					return NEXT;
				}
			}
			if(up(w)) {
				if(w.match(new ProductionPattern(language.production.Sub.class), null)) {
					List<String> args = exprLvl.pop();
					exprLvl.peek().add(args.get(0) + " - " + args.get(1));
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Add.class), null)) {
					List<String> args = exprLvl.pop();
					exprLvl.peek().add(args.get(0) + " + " + args.get(1));
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Mul.class), null)) {
					List<String> args = exprLvl.pop();
					exprLvl.peek().add(args.get(0) + " * " + args.get(1));
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Div.class), null)) {
					List<String> args = exprLvl.pop();
					exprLvl.peek().add(args.get(0) + " / " + args.get(1));
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Mod.class), null)) {
					List<String> args = exprLvl.pop();
					exprLvl.peek().add(args.get(0) + " % " + args.get(1));
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Expression.class), null)) {
					Architecture current = architectures.get(architectures.size() - 1);
					List<String> expr = exprLvl.pop();
					current.Append(expr.get(0));
					return NEXT;
				}
				if(w.match(new ProductionPattern(language.production.Return.class), null)) {
					Architecture current = architectures.get(architectures.size() - 1);
					current.Append(";\n");
					return NEXT;
				}
			}
			return NEXT;
		}
	}
}
