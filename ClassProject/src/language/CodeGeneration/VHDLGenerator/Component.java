package language.CodeGeneration.VHDLGenerator;

import static language.CodeGeneration.VHDLGenerator.HelperMethods.*;

public class Component {
	private final String template = HelperMethods.readFileFromPackage("language/CodeGeneration/VHDLGenerator/templates/component.vhdl");
	public final Entity entity;
	private String componentString = null;
	
	public Component(Entity entity) {
		this.entity = entity;
	}
	
	@Override
	public String toString() {
		if(componentString == null) {
			StringBuilder sb = new StringBuilder();
			int index = 0;
			for(Entity.Port p : entity.getPorts()) {
				if(index++ < entity.numPorts() -1 ) {
					sb.append(p.generateString() + ";\n");
				} else {
					sb.append(p.generateString());
				}
			}
			componentString = template.replaceAll(namePattern, entity.getName());
			componentString = componentString.replaceAll(inoutPattern, sb.toString());
			componentString = componentString.trim();
		}
		return componentString + '\n';
	}
}
