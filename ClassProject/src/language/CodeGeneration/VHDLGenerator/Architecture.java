package language.CodeGeneration.VHDLGenerator;

public class Architecture {
	private static final String template = HelperMethods.readFileFromPackage("language/CodeGeneration/VHDLGenerator/templates/architecture.vhdl");
	
	private final String name;
	private StringBuilder architectureString = new StringBuilder();
	
	public Architecture(String name) {
		this.name = name;
	}
	
	public void Append(String expression) {
		architectureString.append(expression);
	}

	@Override
	public String toString() {
		return template.replaceAll(HelperMethods.namePattern, name)
				.replaceAll(HelperMethods.declsPattern, "")
				.replaceAll(HelperMethods.archNamePattern, "arch_" + name)
				.replaceAll(HelperMethods.concstatsPattern, architectureString.toString()) + '\n';
	}
}
