package language;
import java.util.ArrayList;
import java.util.regex.Pattern;

import language.production.Program;
import frontend.syntax.Production;
import frontend.tokens.TokenRule;

public class Grammar implements frontend.IGrammar {
	private final ArrayList<TokenRule> rules;
	private final TokenRule[] tokenRules = new TokenRule[] {
			// Ignore
			new TokenRule("Whitespace",           Pattern.compile("\\s+")),
			new TokenRule("Comment",              Pattern.compile("//.*")),
			new TokenRule("Block comment",        Pattern.compile("/\\*([^\\*]|\\*[^/])*\\*/")),

			// Keywords
			new TokenRule("If",                   Pattern.compile("if"),                          language.tokens.keyword.If.class),
			new TokenRule("Else",                 Pattern.compile("else"),                        language.tokens.keyword.Else.class),
			new TokenRule("While",                Pattern.compile("while"),                       language.tokens.keyword.While.class),
			new TokenRule("Return",               Pattern.compile("return"),                      language.tokens.keyword.Return.class),
			new TokenRule("Rewrite",              Pattern.compile("rewrite"),                     language.tokens.keyword.Rewrite.class),

			// Types
			new TokenRule("Bool type",            Pattern.compile("bool"),                        language.tokens.types.Bool.class),
			new TokenRule("Int type",             Pattern.compile("int"),                         language.tokens.types.Int.class),
			new TokenRule("String type",          Pattern.compile("str"),                         language.tokens.types.Str.class),
			new TokenRule("Void type",            Pattern.compile("void"),                        language.tokens.types.Void.class),
			new TokenRule("Variable arguments",   Pattern.compile("\\.\\.\\."),                   language.tokens.VarArgs.class),

			// Literals
			new TokenRule("Bool literal",         Pattern.compile("(true|false)"),                language.tokens.constants.BoolConst.class),
			new TokenRule("Int literal",          Pattern.compile("[0-9]+"),                      language.tokens.constants.NumberConst.class),
			//TODO: Doesn't work properly
			new TokenRule("String literal",       Pattern.compile("\"([^\"])*\""),                language.tokens.constants.StringConst.class),
			new TokenRule("Identifier",           Pattern.compile("[_a-zA-Z][_a-zA-Z0-9]*"),      language.tokens.Identifier.class),

			// Braces
			new TokenRule("Left  curly brace",    Pattern.compile("\\{"),                         language.tokens.bracket.LCurlyBrace.class),
			new TokenRule("Right curly brace",    Pattern.compile("\\}"),                         language.tokens.bracket.RCurlyBrace.class),
			new TokenRule("Left  parenthesis",    Pattern.compile("\\("),                         language.tokens.bracket.LParenthesis.class),
			new TokenRule("Right parenthesis",    Pattern.compile("\\)"),                         language.tokens.bracket.RParenthesis.class),
			new TokenRule("Left  square bracket", Pattern.compile("\\["),                         language.tokens.bracket.LSquareBracket.class),
			new TokenRule("Right square bracket", Pattern.compile("\\]"),                         language.tokens.bracket.RSquareBracket.class),

			// Rewrite operator
			new TokenRule("Rewrite",              Pattern.compile("->"),                          language.tokens.operators.Rewrite.class),
			
			// Arithmetic operators
			new TokenRule("Divide",               Pattern.compile("/"),                           language.tokens.operators.arithmetic.Divide.class),
			new TokenRule("Modulo",               Pattern.compile("%"),                           language.tokens.operators.arithmetic.Modulo.class),
			new TokenRule("Multiply",             Pattern.compile("\\*"),                         language.tokens.operators.arithmetic.Multiply.class),
			new TokenRule("Add",                  Pattern.compile("\\+"),                         language.tokens.operators.arithmetic.Add.class),
			new TokenRule("Subtract",             Pattern.compile("-"),                           language.tokens.operators.arithmetic.Subtract.class),

			// Comparison operators
			new TokenRule("Equal",                Pattern.compile("=="),                          language.tokens.operators.comparison.Equal.class),
			new TokenRule("NotEqual",             Pattern.compile("!="),                          language.tokens.operators.comparison.NotEqual.class),
			new TokenRule("GreaterEqual",         Pattern.compile(">="),                          language.tokens.operators.comparison.GreaterEqual.class),
			new TokenRule("Greater",              Pattern.compile(">"),                           language.tokens.operators.comparison.Greater.class),
			new TokenRule("LessEqual",            Pattern.compile("<="),                          language.tokens.operators.comparison.LessEqual.class),
			new TokenRule("Less",                 Pattern.compile("<"),                           language.tokens.operators.comparison.Less.class),

			// Separators
			new TokenRule("Comma",                Pattern.compile(","),                           language.tokens.Comma.class),
			new TokenRule("Semicolon",            Pattern.compile(";"),                           language.tokens.Semicolon.class),
			new TokenRule("Assignment",           Pattern.compile("="),                           language.tokens.Assign.class),
	};

	public Grammar() {
		this.rules = new ArrayList<TokenRule>();
		for(TokenRule r : tokenRules) {
			rules.add(r);
		}
	}

	public Iterable<TokenRule> getTokenRules() {
		return rules;
	}

	@Override
	public Production GetStartSymbol() {
		return new Program();
	}
}
