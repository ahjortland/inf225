package entry;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import language.Grammar;
import language.Rewrites.RewriteEngine;
import language.TypeChecker.TypeChecker;
import exceptions.CompilerError;
import frontend.Parser;
import frontend.syntax.AST;
import frontend.syntax.nuthatch.SyntaxTree;


public class Main {
	private enum TargetLang {
		ARM, VHDL;
	}
	
	private static boolean typeCheck    = true;
	private static boolean generateCode = true;
	private static boolean linkCode     = true;
	private static boolean Rewrite      = true;
	private static boolean Verbose      = false;
	private static String astPath       = null;
	private static String cstPath       = null;
	private static String Input         = null;
	private static String Output        = null;
	private static TargetLang lang      = TargetLang.ARM;
	
	public static boolean isVerbose() {
		return Verbose;
	}

	public static void main(String[] args) throws Exception {
		checkArgs(args);

		try {
			String program = readPath(Input);
			SyntaxTree ast = parse(program);
			String source = Input;
			String filename = Output;
			if(Input.equals("-")) {
				source = "<stdin>";
			} else {
				int idx = source.replaceAll("\\\\", "/").lastIndexOf("/");
				source = idx >= 0 ? source.substring(idx + 1) : source;
			}
			if(Output.equals("-")) {
				filename = "<stdout>";
			} else {
				int idx = filename.replaceAll("\\\\", "/").lastIndexOf("/");
				filename = idx >= 0 ? Input.substring(idx + 1) : filename;
			}
			ast.setMetadata("source", source);
			ast.setMetadata("filename", filename);
			if(typeCheck) {
				TypeCheck(ast);
			}
			if(Rewrite) {
				Rewrite(ast);
			}
			if(generateCode) {
				switch(lang) {
				case ARM:
					String assembly = GenerateCode(new language.CodeGeneration.ARMGenerator.CodeGenerator(ast));
					if(linkCode) {
						linkCode(assembly);
					} else {
						outputString(Output, assembly);
					}
					break;
				case VHDL:
					String vhdl = GenerateCode(new language.CodeGeneration.VHDLGenerator.CodeGenerator(ast));
					outputString(Output, vhdl);
					break;
				}
			}

			if(Verbose) {
				System.err.println("Done");
			}
		} catch(CompilerError error) {
			System.err.println(error.getClass().getSimpleName() + ": " + error.getMessage() + "\n");
			if(Verbose) {
				throw error;
			}
		}
	}

	/**
	 * Will check all the command line arguments and set variables based on them
	 * 
	 * @param args Command line arguments to the program
	 */
	private static void checkArgs(String[] args) {
		for(int i = 0; i < args.length; i++) {
			switch(args[i].toLowerCase()) {
			case "-h":
			case "--help":
				printUsage();
				break;
			case "-v":
			case "--verbose":
				Verbose = true;
				break;
			case "-a":
			case "--ast":
				astPath = args[i + 1];
				i += 1;
				break;
			case "-c":
			case "--cst":
				cstPath = args[i + 1];
				i += 1;
				break;
			case "-t":
			case "--no-typecheck":
				typeCheck = false;
			case "-n":
			case "--no-assembly":
				generateCode = false;
				break;
			case "-s":
			case "--only-assembly":
				linkCode = false;
				break;
			case "-r":
			case "--no-rewrite":
				Rewrite = false;
				break;
			case "-l":
			case "--lang":
				switch(args[i + 1].toLowerCase()) {
				case "vhdl":
					lang = TargetLang.VHDL;
					break;
				case "arm":
					lang = TargetLang.ARM;
					break;
				default:
					System.err.println(args[i+1] + " is not an available language.");
					printUsage();
					break;
				}
				i += 1;
				break;
			default:
				if(Input == null) {
					Input = args[i];
				} else if(Output == null) {
					Output = args[i];
				} else {
					printUsage(args[i]);
				}
				break;
			}
		}
		if(Output == null && generateCode) {
			System.err.println("Need to specify a output path for the assembly");
			printUsage();
		}
	}

	/**
	 * Will read a path string from the path. If path is "-" it will read form stdin 
	 * 
	 * @param path Where to read from
	 * @return The content of the file or stdin
	 * @throws IOException If it fails to read from the file or stdin
	 */
	private static String readPath(String path) throws IOException {
		String program = null;
		if(Input.equals("-")) {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			StringBuilder sb = new StringBuilder();
			sb.append((char)br.read());
			program = sb.toString();
		} else {
			byte[] encoded = Files.readAllBytes(Paths.get(Input));
			program = Charset.defaultCharset().decode(ByteBuffer.wrap(encoded)).toString();
		}
		return program;
	}

	/**
	 * Will parse the program and return its abstract syntax tree.
	 * 
	 * This method will also print the CST if the variable {@code cstPath != null} 
	 * and the AST if the variable {@code astPath != null}
	 * 
	 * @param program The program to be parsed 
	 * @return An abstract syntax tree representing the program
	 * @throws IOException If it tries and fails to print either the AST or the CST.
	 */
	private static SyntaxTree parse(String program) throws IOException {
		long time = System.currentTimeMillis();
		Parser parser = new Parser(program, new Grammar());

		SyntaxTree root = parser.parse();
		outputString(cstPath, root.toString());

		SyntaxTree ast = AST.constructAST(root);
		outputString(astPath, ast.toString());

		if(Verbose) {
			System.err.printf("Used: %d milliseconds on parsing\n", System.currentTimeMillis() - time);
		}
		return ast;
	}
	

	/**
	 * Will do the rewrites on the program
	 * @param ast
	 */
	private static void Rewrite(SyntaxTree ast) {
		long time = System.currentTimeMillis();
		new RewriteEngine().rewrite(ast);
		if(Verbose) {
			System.err.printf("Used: %d milliseconds on AST rewriting\n", System.currentTimeMillis() - time);
		}
	}

	/**
	 * Will type check the program
	 * 
	 * @param ast The abstract syntax tree to type check
	 */
	private static void TypeCheck(SyntaxTree ast) {
		long time = System.currentTimeMillis();
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
		if(Verbose) {
			System.err.printf("Used: %d milliseconds on type checking\n", System.currentTimeMillis() - time);
		}
	}

	/**
	 * Will generate arm assembly of the program
	 * 
	 * @param ast The abstract syntax tree to generate assembly from
	 * 
	 * @return A string containing ARM assembly.
	 */
	private static String GenerateCode(frontend.CodeGenerator generator) {
		long time = System.currentTimeMillis();
		String assembly = generator.generate();
		if(Verbose) {
			System.err.printf("Used: %d milliseconds on code generation\n", System.currentTimeMillis() - time);
		}
		return assembly;
	}

	/**
	 * Will call the assembler and gcc to link the program. 
	 * 
	 * @param path Path which we will output the program
	 * @param assembly What is going to be assembled and linked
	 * @throws IOException 
	 * @throws InterruptedException 
	 * @throws URISyntaxException 
	 */
	private static void linkCode(String assembly) throws IOException, InterruptedException, URISyntaxException {
		String objFile = Output + ".o";
		ProcessBuilder as = new ProcessBuilder("as", "-o", objFile);
		Process asProcess;
		asProcess = as.start();


		OutputStreamWriter os = new OutputStreamWriter(asProcess.getOutputStream());
		os.write(assembly);
		os.close();

		if(Verbose) {
			InputStreamReader is = new InputStreamReader(asProcess.getInputStream());
			Scanner s = new Scanner(is);
			Scanner s2 = s.useDelimiter("\\A");
			System.err.println(s.hasNext() ? s.next() : "");
			s2.close();
			s.close();
			is.close();

			InputStreamReader es = new InputStreamReader(asProcess.getErrorStream());
			s = new Scanner(es);
			s2 = s.useDelimiter("\\A");
			System.err.println(s2.hasNext() ? s2.next() : "");
			s2.close();
			s.close();
			es.close();

			System.err.println("as returned: " + asProcess.waitFor());
		} else {
			asProcess.waitFor();
		}

		List<String> command = new ArrayList<>();
		command.add("cc");
		command.add("-o " + Output);
		command.add(objFile);

		ProcessBuilder cc = new ProcessBuilder("cc", "-o", Output, objFile);
		Process ccProcess = cc.start();
		if(Verbose) {
			InputStreamReader is = new InputStreamReader(ccProcess.getInputStream());
			Scanner s = new Scanner(is);
			Scanner s2 = s.useDelimiter("\\A");
			System.err.println(s.hasNext() ? s.next() : "");
			s2.close();
			s.close();
			is.close();

			InputStreamReader es = new InputStreamReader(ccProcess.getErrorStream());
			s = new Scanner(es);
			s2 = s.useDelimiter("\\A");
			System.err.println(s2.hasNext() ? s2.next() : "");
			s2.close();
			s.close();
			es.close();

			System.err.println("cc returned: " + ccProcess.waitFor());
		} else {
			ccProcess.waitFor();
		}

		File f = new File(objFile);
		f.delete();
	}

	/**
	 * Helper method to print a string. If path equals "-" it will write on 
	 * stdout, else it will try to write the string to the file on that path
	 * 
	 * @param path Where to write the content
	 * @param content What should be written
	 * @throws IOException If it fails to create a new file or write to that file.
	 */
	private static void outputString(String path, String content) throws IOException {
		if(path != null) {
			if(path.equals("-")) {
				System.out.println(content);
			} else {
				File f = new File(path);
				f.createNewFile();
				try(PrintWriter out = new PrintWriter(path)) {
					out.print(content);
				}
			}
		}
	}

	/**
	 * Helper method which says this is an invalid command line argument before calling printUsage
	 * 
	 * @param argument Which argument is invalid
	 */
	private static void printUsage(String argument) {
		System.err.println("Invalid command line argument " + argument);
		printUsage();
	}

	/**
	 * Helper method to print the help text of how to use the program.
	 * 
	 * This will also call {@code System.exit(1)}, and therefore exit the program
	 */
	private static void printUsage() {
		System.err.println("Usage:");
		System.err.println("java -jar Compiler.jar [-htrv] [-a file] [-c file] [-l lang] input output");
		System.err.println("java -jar Compiler.jar -n [-htrv] [-a file] [-c file] input");
		System.err.println();
		System.err.println("  -h, --help           Print this help text and exit.");
		System.err.println("  -v, --verbose        Be verbose. Will print timings and other debug stuff on");
		System.err.println("                       stderr.");
		System.err.println("  -a, --ast            Print the ast to a file. Will print to stdout if a dash");
		System.err.println("                       ('-') is given");
		System.err.println("  -c, --cst            Print the cst to a file. Will print to stdout if a dash");
		System.err.println("                       ('-') is given");
		System.err.println("  -t, --no-typecheck   Disable typechecking");
		System.err.println("  -n, --no-assembly    Disable code generation. This will disable linking.");
		System.err.println("  -s, --only-assembly  Disable linking");
		System.err.println("  -r, --no-rewrite     Ignore rewrite rules");
		System.err.println("  -l, --lang           Choose target language. Supported options as of now are");
		System.err.println("                       'ARM' and 'VHDL'. If you choose VHDL then it won't link");
		System.err.println("                       the program which means that '-s' does not mean anything.");
		System.err.println("                       This only affects code generation, so if code generation");
		System.err.println("                       is disabled ('-n'), it will not have any effect.");
		System.err.println();
		System.err.println("input                  This is the input source file which should be compiled.");
		System.err.println("                       Will read from stdin if input is a dash ('-')");
		System.err.println("output                 This is the path where the compiler will output the");
		System.err.println("                       complete program (or the assembly if -s is set).");
		System.err.println("                       Will print to stdout if output is a dash ('-')");
		System.err.println();

		System.exit(1);
	}
}
