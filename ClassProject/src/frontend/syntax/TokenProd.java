package frontend.syntax;

import language.TypeChecker.Type;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public final class TokenProd extends Production {
	public final Token token;

	public TokenProd(Token t) {
		this.token = t;
	}

	@Override
	public boolean ShowInAST() {
		return token.showInAST();
	}

	@Override
	public String GetName() {
		return token.literal;
	}

	@Override
	public SyntaxTree parse(Scanner sc) {
		return new SyntaxTree(sc.popToken());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TokenProd) {
			return ((TokenProd) obj).equals(token);
		}
		return false;
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		if (token instanceof language.tokens.operators.comparison.CompareToken) {
			return new Type[] { new Type.Bool(token.start) };
		} else if (token instanceof language.tokens.operators.arithmetic.Add) {
			return new Type[] { new Type.Int(token.start) };
		} else if (token instanceof language.tokens.types.Void) {
			return new Type[] { new Type.Void(token.start) };
		} else if (token instanceof language.tokens.types.Bool) {
			return new Type[] { new Type.Bool(token.start) };
		} else if (token instanceof language.tokens.types.Int) {
			return new Type[] { new Type.Int(token.start) };
		} else if (token instanceof language.tokens.types.Str) {
			return new Type[] { new Type.Str(token.start) };
		} else if (token instanceof language.tokens.Identifier) {
			return new Type[] { new Type.Identifier(token) };
		}
		return null;
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
	}

	@Override
	public Position getPosition() {
		return token.start;
	}
}