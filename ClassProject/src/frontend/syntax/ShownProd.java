package frontend.syntax;

/**
 * This class and any classes inheriting from this one will be shown in the AST
 * @author andreas
 *
 */
public abstract class ShownProd extends Production {
	
	@Override
	public boolean ShowInAST() {
		return true;
	}
}
