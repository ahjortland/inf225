package frontend.syntax.nuthatch;

import frontend.syntax.Production;
import frontend.tokens.Token;
import nuthatch.tree.Tree;
import nuthatch.tree.TreeCursor;
import nuthatch.tree.impl.AbstractTreeCursor;

public class SyntaxTreeCursor extends AbstractTreeCursor<Token, Production, SyntaxTree> {

	public SyntaxTreeCursor(SyntaxTree tree) {
		super(tree);
	}
	
	public void setMetadata(String key, Object value) {
		getCurrent().setMetadata(key, value);
	}
	
	public Object getMetadata(String key) {
		return getTree().getMetadata(key);
	}

	private SyntaxTreeCursor(SyntaxTreeCursor src, boolean fullTree) {
		super(src, fullTree);
	}
	
	private SyntaxTreeCursor(SyntaxTreeCursor src, SyntaxTree replacement) {
		super(src, replacement);
	}
	
	public SyntaxTree getTree() {
		return getCurrent();
	}
	
	@Override
	public TreeCursor<Token, Production> copy() {
		return new SyntaxTreeCursor(this, true);
	}
	
	@Override
	public String toString() {
		return super.toString() + ": " + getCurrent().toString();
	}


	@Override
	public TreeCursor<Token, Production> copyAndReplaceSubtree(TreeCursor<Token, Production> replacement) {
		if(replacement instanceof SyntaxTreeCursor) {
			return new SyntaxTreeCursor(this, ((SyntaxTreeCursor) replacement).getCurrent());
		}
		else {
			throw new UnsupportedOperationException("Replacing with different cursor Production");
		}
	}


	@Override
	public TreeCursor<Token, Production> copySubtree() {
		return new SyntaxTreeCursor(this, false);
	}


	@Override
	public Token getData() {
		return getCurrent().getData();
	}


	@Override
	public String getName() {
		return getCurrent().getName();
	}


	@Override
	public int getNumChildren() {
		return getCurrent().numChildren();
	}


	@Override
	public Production getType() {
		return getCurrent().getType();
	}


	@Override
	public boolean hasData() {
		return getData() != null;
	}


	@Override
	public boolean hasName() {
		return getName() != null;
	}

	@Override
	public boolean subtreeEquals(TreeCursor<Token, Production> other) {
		if(this == other) {
			return true;
		}
		else if(other == null) {
			return false;
		}
		else if(other instanceof SyntaxTreeCursor) {
			return getCurrent().equals(((SyntaxTreeCursor) other).getCurrent());
		}
		else {
			throw new UnsupportedOperationException("Equality only supported on ASTTreeCursor");
		}
	}


	@Override
	protected SyntaxTree getChild(int i) {
		return (SyntaxTree) getCurrent().getBranch(i + 1);
	}


	@Override
	protected SyntaxTree replaceChild(SyntaxTree node, SyntaxTree child, int i) {
		int n = node.numChildren();
		SyntaxTree children[] = new SyntaxTree[n];
		int j = 0;
		for(Tree<Token, Production> c : node.children()) {
			if(i == j) {
				children[j++] = child;
			}
			else {
				children[j++] = (SyntaxTree) c;
			}
		}
		return new SyntaxTree(node.getType(), children);
	}
}
