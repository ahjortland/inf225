package frontend.syntax.nuthatch;

import java.util.ArrayList;

import nuthatch.pattern.Environment;
import nuthatch.pattern.NotBuildableException;
import nuthatch.pattern.Pattern;
import nuthatch.tree.TreeCursor;
import frontend.syntax.Production;
import frontend.tokens.Token;

public class ProductionPattern implements Pattern<Token, Production> {
	private final Class<? extends Production>[] production;
	private final Token[] token;
	private boolean inverse = false;
	
	public ProductionPattern(Production... prod) {
		@SuppressWarnings("unchecked")
		Class<? extends Production>[] productions = new Class[prod.length];
		int index = 0;
		for(Production production : prod) {
			productions[index++] = production.getClass();
		}
		this.production = productions;
		this.token = null;
	}
	
	private ProductionPattern(ProductionPattern pattern) {
		this.token = pattern.token;
		this.production = pattern.production;
		this.inverse = pattern.inverse;
	}
	
	public ProductionPattern invertPattern() {
		ProductionPattern pattern = new ProductionPattern(this);
		pattern.inverse = true;
		return pattern;
	}
	
	@SafeVarargs
	public ProductionPattern(Class<? extends Production>... prod) {
		this.production = prod;
		this.token = null;
	}
	
	public ProductionPattern(Token... tok) {
		this.production = null;
		this.token = tok;
	}

	@Override
	public boolean subTreeOnly() {
		return true;
	}
	
	public SyntaxTreeCursor build(SyntaxTreeCursor tree) {
		ArrayList<SyntaxTreeCursor> children = new ArrayList<SyntaxTreeCursor>();
		for(TreeCursor<Token, Production> child : tree){
			if(match(child, null)) {
				children.add((SyntaxTreeCursor) child.copy());
			}
		}
		SyntaxTree[] childArr = new SyntaxTree[children.size()];
		for(int i = 0; i < childArr.length; i++) {
			childArr[i] = ((SyntaxTreeCursor) children.get(i)).getTree();
		}
		return new SyntaxTreeCursor(new SyntaxTree(tree.getType(), childArr));
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends TreeCursor<Token, Production>> T build(T tree, Environment<? extends T> env) throws NotBuildableException {
		if(!(tree instanceof SyntaxTreeCursor)) {
			throw new NotBuildableException("Must be of type SyntaxTreeCursor to be buildable");
		}
		return (T) build((SyntaxTreeCursor) tree);
	}

	@Override
	public <T extends TreeCursor<Token, Production>> boolean match(T tree,
			Environment<T> env) {
		if(tree.getType() != null && production != null) {
			for(Class<? extends Production> prod : production) {
				if(prod.equals(tree.getType().getClass())) {
					return !inverse;
				}
			}
		}
		if(token != null && tree.getData() != null) {
			for(Token tok : token) {
				if(tok.equals(tree.getData())) {
					return !inverse;
				}
			}
		}
		return inverse;
	}
}
