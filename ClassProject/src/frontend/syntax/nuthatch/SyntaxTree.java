package frontend.syntax.nuthatch;

import static nuthatch.library.JoinPoints.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import nuthatch.library.BaseWalk;
import nuthatch.tree.Tree;
import nuthatch.tree.TreeCursor;
import nuthatch.tree.impl.StandardTree;
import nuthatch.walker.impl.SimpleWalker;
import exceptions.ElementExistsException;
import frontend.syntax.Production;
import frontend.tokens.Token;

public class SyntaxTree extends StandardTree<Token, Production> {
	private final HashMap<String, Object> metadata;

	public static String treeString(SyntaxTree tree) {
		StringBuilder sb = new StringBuilder();
		if (tree.isLeaf()) {
			sb.append(String.format("[%s]", tree.getData()));
		} else {
			sb.append(String.format("[%s", tree.getName()));
			for (Tree<Token, Production> child : tree.children()) {
				sb.append(String.format(" %s", treeString((SyntaxTree) child)));
			}
			sb.append("]");
		}
		return sb.toString().replaceAll("_", "-"); // to play nice with my syntax tree generator
	}

	public static String xmlString(SyntaxTree tree) {
		return xmlString(tree, 0, 0);
	}

	private static String xmlString(SyntaxTree tree, int tab, int index) {
		int tabWidth = 4;
		StringBuilder sb = new StringBuilder();
		String formatString;
		if (tab > 0) {
			formatString = "%-" + tab + "s%s";
		} else {
			formatString = "%s%s";
		}
		if (tree.isLeaf()) {
			String node = String
					.format("<%s literal=\"%s\" start=\"%d\" end=\"%d\"  index=\"%d\" />\n",
							tree.getData().getClass().getName(),
							tree.getData().literal, tree.getData().start,
							tree.getData().end, index);
			sb.append(String.format(formatString, "", node));
		} else {
			sb.append(String.format(formatString, "",
					String.format("<%s index=\"%d\">\n", tree.getName(), index)));
			int i = 0;
			for (Tree<Token, Production> child : tree.children()) {
				sb.append(xmlString((SyntaxTree) child, tab + tabWidth, i++));
			}
			sb.append(String.format(formatString, "",
					String.format("</%s>\n", tree.getName())));
		}
		return sb.toString();
	}

	public SyntaxTree(Token data) {
		super(data);
		metadata = new HashMap<>();
	}

	public SyntaxTree(Production type, SyntaxTree... children) {
		super(type.GetName(), type,
				children != null ? (children.length > 0 ? children
						: new SyntaxTree[0]) : new SyntaxTree[0]);
		metadata = new HashMap<>();
	}
	
	public boolean hasMetadata(String key) {
		return metadata.containsKey(key);
	}

	/**
	 * Get some metadata from this node. Note that this will throw
	 * {@link NoSuchElementException} if the metadata does not exist.
	 * 
	 * @param key
	 *            Key to get
	 * @return The metadata linked to the key.
	 */
	public Object getMetadata(String key) {
		Object data = metadata.get(key);
		if (data != null) {
			return data;
		}
		throw new NoSuchElementException(key);
	}

	/**
	 * Set metadata to this node. Note that metadata is immutable, and once set
	 * should not change. Therefore this will throw an
	 * {@link ElementExistsException} if you try to overwrite some metadata.
	 * 
	 * @param key Key to link the metadata
	 * @param value The metadata
	 */
	public void setMetadata(String key, Object value) {
		Object oldVal = metadata.get(key);
		if (oldVal != null) {
			throw new ElementExistsException(key, oldVal, key, value,
					"Metadata is immutable and should not be overwritten once set.");
		}
		metadata.put(key, value);
	}

	@Override
	public String toString() {
		return treeString(this);
	};

	public static Token getFirstToken(SyntaxTreeWalker subtree) {
		return getFirstToken((SyntaxTreeCursor) subtree.copySubtree());
	}

	public static Token getFirstToken(SyntaxTreeCursor subtree) {
		List<Token> l = getTokens(subtree);
		if (l.size() > 0) {
			return l.get(0);
		}
		return null;
	}

	public static Token getFirstToken(SyntaxTree subtree) {
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(subtree);
		return getFirstToken(cursor);
	}

	public static List<Token> getTokens(SyntaxTreeWalker subtree) {
		return getTokens((SyntaxTreeCursor) subtree.copySubtree());
	}

	public static List<Token> getTokens(SyntaxTree subtree) {
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(subtree);
		return getTokens(cursor);
	}

	public static List<Token> getTokens(SyntaxTreeCursor subtree) {
		final ArrayList<Token> l = new ArrayList<Token>();
		BaseWalk<SimpleWalker<Token, Production>> w = new BaseWalk<SimpleWalker<Token, Production>>() {
			int depth = 0;

			@Override
			public int step(SimpleWalker<Token, Production> walker) {
				if (depth < 0) {
					return PARENT;
				}
				if (down(walker)) {
					depth++;
				}
				if (up(walker)) {
					depth--;
				}
				if (leaf(walker)) {
					l.add(walker.getData());
				}
				return NEXT;
			}
		};
		SimpleWalker<Token, Production> walker = new SimpleWalker<Token, Production>(
				subtree, w);
		walker.start();
		return l;
	}

	@SafeVarargs
	public static SyntaxTreeCursor QueryFirst(SyntaxTreeWalker start,
			final Class<? extends Production>... path) {
		List<SyntaxTreeCursor> l = Query(
				(SyntaxTreeCursor) start.copySubtree(), path);
		if (l.size() > 0) {
			return l.get(0);
		}
		return null;
	}

	@SafeVarargs
	public static SyntaxTreeCursor QueryFirst(SyntaxTreeCursor start,
			final Class<? extends Production>... path) {
		List<SyntaxTreeCursor> l = Query(start, path);
		if (l.size() > 0) {
			return l.get(0);
		}
		return null;
	}

	@SafeVarargs
	public static SyntaxTreeCursor QueryFirst(SyntaxTree start,
			final Class<? extends Production>... path) {
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(start);
		return QueryFirst(cursor, path);
	}

	@SafeVarargs
	public static List<SyntaxTreeCursor> Query(SyntaxTreeWalker start,
			final Class<? extends Production>... path) {
		return Query((SyntaxTreeCursor) start.copy(), path);
	}

	@SafeVarargs
	public static List<SyntaxTreeCursor> Query(SyntaxTree start,
			final Class<? extends Production>... path) {
		SyntaxTreeCursor cursor = new SyntaxTreeCursor(start);
		return Query(cursor, path);
	}

	@SafeVarargs
	public static List<SyntaxTreeCursor> Query(SyntaxTreeCursor start,
			final Class<? extends Production>... path) {
		if (start == null) {
			return new ArrayList<>();
		}
		final SyntaxTreeCursor cursor = (SyntaxTreeCursor) start.copy();
		final ArrayList<SyntaxTreeCursor> l = new ArrayList<SyntaxTreeCursor>();
		BaseWalk<SyntaxTreeWalker> w = new BaseWalk<SyntaxTreeWalker>() {
			int depth = 0;

			@Override
			public int step(SyntaxTreeWalker walker) {
				if (depth < 0) {
					return PARENT;
				}
				if (down(walker)) {
					if (walker.getType() != null
							&& walker.getType().getClass().equals(path[depth])) {
						depth++;
						if (depth == path.length) {
							l.add((SyntaxTreeCursor) walker.copy());
							depth--;
							return PARENT;
						}
					} else {
						if (depth == 0
								&& walker.match(new ProductionPattern(cursor
										.getType().getClass()), null)) {
							return NEXT;
						}
						return PARENT;
					}
				} else if (up(walker)) {
					depth--;
				}
				return NEXT;
			}
		};
		SyntaxTreeWalker walker = new SyntaxTreeWalker(start, w);
		walker.start();
		return l;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SyntaxTree) {
			SyntaxTree tree = (SyntaxTree) obj;
			if (getType() != null) {
				if (!(getType().equals(tree.getType()))) {
					return false;
				}
			} else {
				if (tree.getType() != null) {
					return false;
				}
			}
			if (getData() != null) {
				if (!(getData().equals(tree.getData()))) {
					return false;
				}
			} else {
				if (tree.getData() != null) {
					return false;
				}
			}
			if (numChildren() != tree.numChildren()) {
				return false;
			}
			for (int i = FIRST; i <= numChildren(); i++) {
				if (!getBranch(i).equals(tree.getBranch(i))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
