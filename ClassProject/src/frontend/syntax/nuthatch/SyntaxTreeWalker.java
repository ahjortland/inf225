package frontend.syntax.nuthatch;

import frontend.syntax.Production;
import frontend.tokens.Token;
import nuthatch.library.Walk;
import nuthatch.tree.TreeCursor;
import nuthatch.walker.impl.AbstractWalker;

public class SyntaxTreeWalker extends AbstractWalker<Token, Production, SyntaxTreeWalker> {
	
	public SyntaxTreeWalker(SyntaxTree tree, Walk<SyntaxTreeWalker> step) {
		super(tree, step);
	}
	
	public SyntaxTreeWalker(SyntaxTreeCursor cursor, Walk<SyntaxTreeWalker> step) {
		super(cursor, step);
	}
	
	public Object getMetadata(String key) {
		return ((SyntaxTreeCursor) copy()).getMetadata(key);
	}
	
	public void setMetadata(String key, Object value) {
		((SyntaxTreeCursor) copy()).setMetadata(key, value);
	}

	@Override
	protected SyntaxTreeWalker subWalk(TreeCursor<Token, Production> cursor, Walk<SyntaxTreeWalker> step) {
		return new SyntaxTreeWalker((SyntaxTreeCursor) cursor, step);
	}
	
	@Override
	public String toString() {
		return super.toString() + ": " + copy();
	}
}
