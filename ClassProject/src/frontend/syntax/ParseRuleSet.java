package frontend.syntax;

import java.util.Iterator;

public class ParseRuleSet implements Iterable<ParseRule> {
	private final ParseRule[] rules;
	public final Class<? extends Production> alt;

	public ParseRuleSet(Class<? extends Production> prod, ParseRule... rules) {
		this.alt = prod;
		this.rules = rules;
	}

	@Override
	public Iterator<ParseRule> iterator() {
		return new Iterator<ParseRule>() {
			private int index = 0;
			
			@Override
			public boolean hasNext() {
				return index < rules.length;
			}

			@Override
			public ParseRule next() {
				return rules[index++];
			}

			@Override
			public void remove() {
				throw new IllegalAccessError("You are not allowed to remove an element from here");			
			}
		};
	}
	
	public int size() {
		return rules.length;
	}

	@Override
	public String toString() {
		if(alt != null) {
			return alt.toString();
		}
		return "";
	}
}
