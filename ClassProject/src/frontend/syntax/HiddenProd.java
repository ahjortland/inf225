package frontend.syntax;

import language.TypeChecker.Type;
import frontend.environment.Environment;

/**
 * This will be hidden in the AST
 * 
 * @author andreas
 *
 */
public abstract class HiddenProd extends Production {

	@Override
	public boolean ShowInAST() {
		return false;
	}

	@Override
	public Type[] typeCheckUp(Environment<Type> environment, Type... subtypes) {
		throw new UnsupportedOperationException("This is not part of the AST");
	}

	@Override
	public void typeCheckDown(Environment<Type> environment) {
		throw new UnsupportedOperationException("This is not part of the AST");
	}
}
