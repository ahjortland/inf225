package frontend.syntax;

import java.util.ArrayList;

import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Token;
import nuthatch.tree.Tree;
import nuthatch.tree.impl.StandardTree;

public class AST {
	/**
	 * This class is a temporary object used to convert a {@link SyntaxTree} 
	 * to an {@link AST} since it was a bit hard to do implosion using the 
	 * {@link StandardTree} and I was not interested in implementing a tree 
	 * nuthatch {@link Tree} myself.
	 * 
	 * @author Andreas Hjortland
	 * 
	 */
	private static class TmpAST {
		public static TmpAST constructAST(TmpAST parent, SyntaxTree cst) {
			TmpAST ast = parent;
			if (cst.isLeaf()) {
				if(cst.getData() != null) {
					if (cst.getData().showInAST()) {
						return new TmpAST(new TokenProd(cst.getData()));
					}
				} else {
					throw new Error("This is not right.");
					//Token t = new language.tokens.types.Void();
					//ast.children.add(new TmpAST(new TokenProd(t)));
				}
			} else {
				if (cst.getType().ShowInAST()) {
					ast = new TmpAST(cst.getType());
				}
				for (Tree<Token, Production> child : cst.children()) {
					if (child != null) {
						TmpAST astNode = TmpAST.constructAST(ast,
								(SyntaxTree) child);
						if (astNode != ast) {
							ast.children.add(astNode);
						}
					}
				}
				if(ast.children.size() == 0) {
					Token posToken = SyntaxTree.getFirstToken(cst);
					Token t = new language.tokens.types.Void();
					t.start = posToken.end;
					t.end = posToken.end;
					ast.children.add(new TmpAST(new TokenProd(t)));
				}
			}
			return ast;
		}

		public final Production prod;
		public final ArrayList<TmpAST> children;

		public TmpAST(Production prod) {
			this.prod = prod;
			this.children = new ArrayList<TmpAST>();
		}

		public boolean isTerminal() {
			return prod instanceof TokenProd;
		}

		public Token getTerminal() {
			if (isTerminal()) {
				return ((TokenProd) prod).token;
			}
			return null;
		}
	}

	public static SyntaxTree constructAST(SyntaxTree cst) {
		return constructAST(TmpAST.constructAST(null, cst));
	}

	public static SyntaxTree constructAST(TmpAST ast) {
		if (ast.isTerminal()) {
			return new SyntaxTree(ast.getTerminal());
		}
		SyntaxTree[] children = new SyntaxTree[ast.children.size()];
		int index = 0;
		for (TmpAST child : ast.children) {
			children[index++] = constructAST(child);
		}

		return new SyntaxTree(ast.prod, children);
	}
}
