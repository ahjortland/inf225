package frontend.syntax;

import java.util.Collection;

import language.TypeChecker.Type;
import exceptions.SyntaxError;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;

public abstract class Production {
	public static SyntaxTree[] fromCollection(Collection<SyntaxTree> trees) {
		SyntaxTree[] children = new SyntaxTree[trees.size()];
		int index = 0;
		for(SyntaxTree child : trees) {
			children[index++] = child;
		}
		return children;
	}
	
	/**
	 * Will typecheck this vertex based on the subtypes and the environment. Might exit scopes if that is applicable 
	 * 
	 * @param environment Environment of types
	 * @param subtypes Type of every child of this node
	 * @return The type of this vertex
	 */
	public abstract Type[] typeCheckUp(Environment<Type> environment, Type...subtypes);
	
	/**
	 * Will do bookkeeping like entering scopes and declaring variables
	 * 
	 * @param environment Environment which might be modified
	 */
	public abstract void typeCheckDown(Environment<Type> environment);
	
	public abstract boolean ShowInAST();
	public abstract SyntaxTree parse(Scanner sc);
	
	public String GetName() {
		return getClass().getSimpleName().replace('_', '-');
	}
	
	private boolean expect(Class<? extends Token> wants, Token has) {
		if (has == null) {
			return false;
		}
		return wants.isInstance(has);
	}
	
	protected Token getTerminal(Scanner scanner, Class<? extends Token> wants, Token has) {
		if (expect(wants, has)) {
			return has;
		}
		if(has != null) {
			throw new SyntaxError(has, wants);
		} else {
			throw new SyntaxError(scanner.getPosition(), wants);
		}
	}
	
	protected Token getTerminal(Scanner scanner, Class<? extends Token> wants) {
		Token token = scanner.popToken();
		return getTerminal(scanner, wants, token);
	}
	
	protected SyntaxTree getTerminalTree(Scanner scanner, Class<? extends Token> wants, Token has) {
		return new SyntaxTree(getTerminal(scanner, wants, has));
	}
	
	protected SyntaxTree getTerminalTree(Scanner scanner, Class<? extends Token> wants) {
		return new SyntaxTree(getTerminal(scanner, wants));
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj.getClass().equals(getClass());
	}
	
	/**
	 * Get the position of this node in the source code
	 */
	public abstract Position getPosition();
}
