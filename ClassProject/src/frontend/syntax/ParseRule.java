package frontend.syntax;
import java.util.ArrayList;
import java.util.Iterator;

import frontend.tokens.Token;


public final class ParseRule implements Iterable<ParseRuleSet> {
	public static ParseRule Empty = new ParseRule("Empty");
	
	private ArrayList<ParseRuleSet> alternatives;
	
	public final Class<? extends Token> value;
	public final boolean terminal;
	public final String name;
	
	public ParseRule(Class<? extends Token> value) {
		this(value.getName(), value, true);
	}
	
	public ParseRule(String name) {
		this(name, null, false);
	}
	
	private ParseRule(String name, Class<? extends Token> value, boolean terminal) {
		this.name = name;
		this.value = value;
		this.terminal = terminal;
		this.alternatives = new ArrayList<ParseRuleSet>();
	}
	
	public void Add(Class<? extends Production> prod, ParseRule... g) {
		this.alternatives.add(new ParseRuleSet(prod, g));
	}
	
	public void Add(ParseRule... g) {
		this.alternatives.add(new ParseRuleSet(HiddenProd.class, g));
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public Iterator<ParseRuleSet> iterator() {
		return alternatives.iterator();
	}
}
