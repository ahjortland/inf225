package frontend;

import frontend.syntax.Production;
import frontend.tokens.TokenRule;

public interface IGrammar {
	public Iterable<TokenRule> getTokenRules();
	
	public Production GetStartSymbol();
}
