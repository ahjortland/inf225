package frontend.environment;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import exceptions.ElementExistsException;

public final class Environment<T> {
	private final LinkedList<Map<String, T>> environment;
		
	public Environment() {
		environment = new LinkedList<Map<String, T>>();
		environment.add(new HashMap<String, T>());
	}
	
	public void enterScope() {
		environment.addFirst(new HashMap<String, T>());
	}
	
	public void exitScope() {
		environment.removeFirst();
	}
	
	public void declare(String key, T value) {
		if(environment.getFirst().containsKey(key)) {
			throw new ElementExistsException(key, environment.getFirst().get(key), key, value, "Cannot declare an element which already exists.");
		}
		environment.getFirst().put(key, value);
	}
	
	public void assign(String key, T value) {
		for(Map<String, T> scope : environment) {
			if(scope.containsKey(key)) {
				scope.put(key, value);
				return;
			}
		}
		throw new NoSuchElementException("Cannot assign to a non-declared element: " + key);	
	}
	
	public boolean contains(String key) {
		for(Map<String, T> scope : environment) {
			if(scope.containsKey(key)) {
				return true;
			}
		}
		return false;
	}
	
	public int scopeSize() {
		return environment.getFirst().size();
	}
	
	public T get(String key) {
		for(Map<String, T> scope : environment) {
			if(scope.containsKey(key)) {
				return scope.get(key);
			}
		}
		throw new NoSuchElementException(key);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		for(Map<String, T> scope : environment) {
			sb.append("    [\n");
			for(Entry<String, T> s : scope.entrySet()) {
				sb.append(String.format("        %s: %s\n", s.getKey(), s.getValue()));
			}
			sb.append("    ]\n");
		}
		sb.append("}\n");
		return sb.toString();
	}
}
