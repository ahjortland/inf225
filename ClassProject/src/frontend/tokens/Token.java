package frontend.tokens;

import language.TypeChecker.Type;

public abstract class Token {
	public String literal;
	public int StartInt;
	public int EndInt;
	public Position start;
	public Position end;
	public abstract boolean showInAST();
	
	public Token() {	}
	
	@Override
	public String toString() {
		return this.literal;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj.getClass().equals(getClass()))) {
			return false;
		}
		Token tok = (Token) obj;
		return tok.literal.equals(literal);
	}
	
	public Type getType() {
		return null;
	}
}
