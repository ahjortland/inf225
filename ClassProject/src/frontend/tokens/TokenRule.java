package frontend.tokens;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TokenRule {
	public final String name;
	public final Pattern pattern;
	public final boolean ignore;
	public final Class<? extends Token> type;
	public Matcher matcher;

	// Ignore rules. Whitespace, comments etc.
	public TokenRule(String name, Pattern pattern) {
		this(name, pattern, null, true);
	}
	
	// Standard rules
	public TokenRule(String name, Pattern pattern, Class<? extends Token> type) {
		this(name, pattern, type, false);
	}
	
	private TokenRule(String name, Pattern pattern, Class<? extends Token> type, boolean ignore) {
		this.name = name;
		this.pattern = pattern;
		this.type = type;
		this.ignore = ignore;
	}
}