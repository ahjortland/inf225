package frontend.tokens;

public class Position implements Comparable<Position>{
	
	public int line;
	public int character;

	public Position(int line, int character) {
		this.line = line;
		this.character = character;
	}

	@Override
	public String toString() {
		return String.format("<l:%d c:%d>", line, character);
	}

	@Override
	public int compareTo(Position o) {
		if(this.line != o.line) {
			return this.line - o.line;
		}
		return this.character - o.character;
	}
}