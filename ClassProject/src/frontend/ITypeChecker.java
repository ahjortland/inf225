package frontend;

import language.TypeChecker.Type;
import exceptions.TypeError;
import frontend.environment.Environment;

public interface ITypeChecker {
	/**
	 * Check if the node has a type
	 * 
	 * @return true if this AST node has a type
	 */
	public boolean HasType();

	/**
	 * Do the actual typechecking. This will only make sense it the object you
	 * are type checking actually has a type
	 * 
	 * @param environment
	 * 
	 * @param subtypes
	 *            The types which is contained in this type (for instance an add
	 *            node can take two integers)
	 * 
	 * @return The type of this node
	 * @throws TypeError
	 *             If the subtypes are incompatible with each other or this type
	 */
	public Type GetType(Environment<Type> environment, Type... subtypes)
			throws TypeError;
	
	/**
	 * If there is something to set up before 
	 * 
	 * @param environment
	 */
	public void SetUp(Environment<Type> environment);
}
