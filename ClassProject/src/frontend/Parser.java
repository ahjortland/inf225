package frontend;

import exceptions.SyntaxError;
import frontend.syntax.Production;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Token;

public class Parser {
	public final int maxDepth = 100;

	private final Scanner scanner;
	private final Production startSymbol;

	public Parser(String program, IGrammar grammar) {
		this.scanner = new Scanner(program, grammar.getTokenRules());
		this.startSymbol = grammar.GetStartSymbol();
	}

	public SyntaxTree parse() {
		SyntaxTree result = startSymbol.parse(scanner);
		Token t = scanner.peekToken();
		if (t != null) {
			throw new SyntaxError("Finished parsing, but did not reach EOF. Next token is", t);
		}
		return result;
	}
}
