package frontend;

import exceptions.BadTokenException;
import exceptions.RuleNotMatchedException;
import frontend.tokens.Position;
import frontend.tokens.Token;
import frontend.tokens.TokenRule;

/**
 * This is a simple tokenizer which will scan a source string and return tokens
 * matching the input rules.
 * 
 * @author Andreas Hjortland
 * 
 */
public class Scanner {
	private Iterable<TokenRule> rules;
	private int pos;
	private int endPos;
	private final int end;
	private final String program;

	public Scanner(String program, Iterable<TokenRule> rules) {
		this.program = program;
		for (TokenRule rule : rules) {
			rule.matcher = rule.pattern.matcher(program);
		}
		this.rules = rules;
		this.end = program.length();
		this.pos = 0;
	}
	
	public Position getPosition() {
		return getPosition(pos);
	}

	private Position getPosition(int pos) {
		String sofar = program.substring(0, pos);
		String[] lines = sofar.split("\r\n|\r|\n");
		return new Position(lines.length, lines[lines.length - 1].length() + 1);
	}
	
	private Token nextToken = null;

	/**
	 * This will get the next token in the string. It try to match the token
	 * with the rules in the order the iterator of rules will give them.
	 * 
	 * @return A token
	 * @throws RuleNotMatchedException
	 *             If there is no rule which matches this token
	 * @throws BadTokenException
	 *             This is thrown if the token type does not contain a
	 *             parameterless constructor
	 */
	public Token popToken() throws RuleNotMatchedException, BadTokenException {
		if(nextToken != null) {
			Token t = nextToken;
			nextToken = null;
			return t;
		}
		if (pos < end) {
			Token t = null;
			for (TokenRule rule : rules) {
				rule.matcher.region(pos, end);
				if (rule.matcher.lookingAt()) {
					if (rule.ignore) {
						pos = rule.matcher.end();
						return popToken();
					} else {
						try {
							Position start = getPosition(rule.matcher.start());;
							Position end = getPosition(rule.matcher.end());
							if(t == null || (t.end.compareTo(t.start) < end.compareTo(start))) {
								t = rule.type.newInstance();
								t.start = start;
								t.end = end;
								t.literal = program.substring(rule.matcher.start(), rule.matcher.end());
								endPos = rule.matcher.end();
							}
						} catch (IllegalAccessException
								| InstantiationException e) {
							throw new BadTokenException(
									"A Token type should have a parameterless constructor",
									e);
						}
					}
				}
			}
			if(t != null) {
				pos = endPos;
				return t;
			} else {
				throw new RuleNotMatchedException("Rule not matched at "
						+ getPosition(pos));
			}
		}
		return null;
	}

	/**
	 * This will peek at the next token, but not move to the next token in the
	 * string
	 * 
	 * @return A token
	 * @throws RuleNotMatchedException
	 *             If there is no rule which matches this token
	 * @throws BadTokenException
	 *             This is thrown if the token type does not contain a
	 *             parameterless constructor
	 */
	public Token peekToken() throws BadTokenException, RuleNotMatchedException {
		if(nextToken == null) {
			nextToken = popToken();
		}
		return nextToken;
	}

	/**
	 * Will reset the scanner.
	 */
	public void reset() {
		pos = 0;
	}
}
