package frontend;

public interface ICodeGenerator {
	public String generate();
}
