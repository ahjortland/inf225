package exceptions;

public class IllegalAllignmentError extends CompilerError {
	private static final long serialVersionUID = 1L;

	public IllegalAllignmentError(String message) {
		super(message);
	}
	
	public IllegalAllignmentError() {
		super();
	}
}
