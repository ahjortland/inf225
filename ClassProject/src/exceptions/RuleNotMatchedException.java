package exceptions;


public class RuleNotMatchedException extends CompilerError {
	private static final long serialVersionUID = 1L;

	public RuleNotMatchedException() {
		super();
	}
	
	public RuleNotMatchedException(String message) {
		super(message);
	}
}
