package exceptions;

public class ElementExistsException extends CompilerError {
	private static final long serialVersionUID = 1L;
	public final Object oldKey;
	public final Object oldValue;
	public final Object newKey;
	public final Object newValue;
	
	public ElementExistsException(Object oldKey, Object oldValue, Object newKey, Object newValue, String msg) {
		super(msg);
		this.oldKey = oldKey;
		this.oldValue = oldValue;
		this.newKey = newKey;
		this.newValue = newValue;
	}
	
	public ElementExistsException(Object oldKey, Object oldValue, Object newKey, Object newValue) {
		super();
		this.oldKey = oldKey;
		this.oldValue = oldValue;
		this.newKey = newKey;
		this.newValue = newValue;
	}
	
	@Override
	public String toString() {
		String message = getMessage() + ". ";
		if(getMessage() == null) {
			message = "";
		}
		return String.format("%sOld item: (%s -> %s). New item: (%s -> %s)", message, oldKey, oldValue, newKey, newValue);
	}
}
