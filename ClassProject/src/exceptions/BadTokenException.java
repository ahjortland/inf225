package exceptions;

public class BadTokenException extends CompilerError {
	private static final long serialVersionUID = 1L;
	public BadTokenException() {
		super();
	}
	public BadTokenException(String message) {
		super(message);
	}
	
	public BadTokenException(Exception innerException) {
		super(innerException);
	}
	
	public BadTokenException(String message, Exception innerException) {
		super(message, innerException);
	}
}
