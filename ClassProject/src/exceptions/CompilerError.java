package exceptions;

public abstract class CompilerError extends Error {
	private static final long serialVersionUID = 1L;
	
	public CompilerError() {
		super();
	}

	public CompilerError(String message) {
		super(message);
	}
	
	public CompilerError(Throwable cause) {
		super(cause);
	}
	
	public CompilerError(String message, Throwable cause) {
		super(message, cause);
	}
	
	public CompilerError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
