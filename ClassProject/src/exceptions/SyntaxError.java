package exceptions;

import frontend.tokens.Position;
import frontend.tokens.Token;

public class SyntaxError extends CompilerError {
	private static final long serialVersionUID = 1L;
	
	public SyntaxError(String message) {
		super(message);
	}

	public SyntaxError(String message, Token token) {
		super(String.format("%s: %s at %s", message, token.getClass().getSimpleName(), token.start));
	}
	
	public SyntaxError(Token token) {
		super(String.format("Syntax error in token '%s' at %s", token.literal, token.start, token.end));
	}
	
	public SyntaxError(Token token, Class<? extends Token> expected) {
		super(String.format("Syntax error in token: Expected %s, but got %s at %s", expected.getSimpleName(), token.getClass().getSimpleName(), token.start));
	}
	
	public SyntaxError(Position pos, Class<? extends Token> expected) {
		super(String.format("Syntax error in token: Expected %s at %s", expected.getSimpleName(), pos));
	}
	
}
