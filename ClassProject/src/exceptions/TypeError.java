package exceptions;

import frontend.tokens.Position;

public class TypeError extends CompilerError {
	private static final long serialVersionUID = 1L;

	public TypeError(String msg, Position pos) {
		super(msg + " at " + pos.toString());
	}
}
