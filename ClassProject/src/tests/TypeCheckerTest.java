package tests;

import static org.junit.Assert.*;
import language.Grammar;
import language.TypeChecker.TypeChecker;

import org.junit.Test;

import exceptions.TypeError;
import frontend.Parser;
import frontend.syntax.AST;
import frontend.syntax.nuthatch.SyntaxTree;

public class TypeCheckerTest {

	@Test
	public void simpleFunction() {
		String program = "void main() { return; }";
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void assign() {
		String program = "int s;\n" +
				"int main() { s = 2 + 2; return s; }";
		
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());

		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void assignArr() {
		String program = "int s[2];\n" +
				"int main() { s[1] = 2 + 2; return s[0]; }";
		
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());

		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void returnValueFail() {
		String program = "int main() { return; }";
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		try {
			checker.check();
			fail("should fail at return int");
		} catch(TypeError e) {
			return;
		}
	}
	
	@Test
	public void returnValue() {
		String program = "int main() { return 5; }";
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void varDecl() {
		String program = ""
				+ "int x = 5;\n"
				+ "str y = \"Hello, world!\";\n"
				+ "bool z = false;\n"
				+ "int a[2];\n"
				+ "str b[2];\n"
				+ "bool c[2];\n"
				+ "int d;\n"
				+ "int read();\n"
				+ "void main() { int y = read(); }\n"
				;
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void arithExp() {
		String program = ""
				//+ "int x = 5+5;\n"
				+ "int y = 5-5;\n"
				//+ "int z = 5/5;\n"
				//+ "int a = 5*5;\n"
				//+ "int b = 5%5;\n"
				;
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void compExp() {
		String program = ""
				+ "bool x = 5>5;\n"
				+ "bool y = 5>=5;\n"
				+ "bool z = 5==5;\n"
				+ "bool a = 5!=5;\n"
				+ "bool b = 5<5;\n"
				+ "bool c = 5<=5;\n"
				;
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
	
	@Test
	public void arrDecl() {
		String program = "int d;int e = 2 + 2;\nint y[2];";
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}

	@Test
	public void call() {
		String program = ""
				+ "void print(...);"
				+ "bool x(int y) { return (y == 2); }\n"
				+ "void main() { print(x(2)); }\n"
				;
		Parser p = new Parser(program, new Grammar());
		SyntaxTree ast = AST.constructAST(p.parse());
		TypeChecker checker = new TypeChecker(ast);
		checker.check();
	}
}
