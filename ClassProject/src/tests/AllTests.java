package tests;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LanguageTest.class, ParserTest.class, TypeCheckerTest.class })
public class AllTests {

}