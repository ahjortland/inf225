package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.regex.Pattern;

import language.TypeChecker.Type;
import nuthatch.tree.Tree;

import org.junit.Before;
import org.junit.Test;

import exceptions.RuleNotMatchedException;
import frontend.IGrammar;
import frontend.Parser;
import frontend.Scanner;
import frontend.environment.Environment;
import frontend.syntax.AST;
import frontend.syntax.Production;
import frontend.syntax.ShownProd;
import frontend.syntax.nuthatch.SyntaxTree;
import frontend.tokens.Position;
import frontend.tokens.Token;
import frontend.tokens.TokenRule;

public class ParserTest {
	public static class TestToken extends Token {
		public TestToken() { }

		@Override
		public boolean showInAST() {
			return true;
		}

		@Override
		public Type getType() {
			// TODO Auto-generated method stub
			return null;
		}
	}
	public static class LParen extends Token {
		public LParen() { }

		@Override
		public boolean showInAST() {
			return false;
		}

		@Override
		public Type getType() {
			// TODO Auto-generated method stub
			return null;
		}
	}
	public static class RParen extends Token {
		public RParen() { }

		@Override
		public boolean showInAST() {
			return false;
		}

		@Override
		public Type getType() {
			// TODO Auto-generated method stub
			return null;
		}
	}
	private final String tokenName = "Test";
	private final String tokenRegex = "test";
	
	private IGrammar grammar;
	
	@Before
	public void SetUp() {
		grammar = new IGrammar() {
			private final TokenRule r = new TokenRule(tokenName, Pattern.compile(tokenRegex), TestToken.class);
			
			class TestRule extends ShownProd {
				@Override
				public SyntaxTree parse(Scanner sc) {
					return new SyntaxTree(this, getTerminalTree(sc, TestToken.class));
				}

				@Override
				public Type[] typeCheckUp(Environment<Type> environment,
						Type... subtypes) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void typeCheckDown(Environment<Type> environment) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public Position getPosition() {
					// TODO Auto-generated method stub
					return null;
				}
			}
			
			@Override
			public Iterable<TokenRule> getTokenRules() {
				ArrayList<TokenRule> arr = new ArrayList<TokenRule>();
				arr.add(r);
				return arr;
			}
			
			@Override
			public Production GetStartSymbol() {
				return new TestRule();
			}
		};
	}

	@Test
	public void simpleGrammar() {
		String program = "test";
		Parser parser = new Parser(program, grammar);
		Tree<Token, Production> tree = parser.parse();
		assertEquals(1, tree.numChildren());
		
		Pattern pattern = Pattern.compile(tokenRegex);
		assertTrue(pattern.matcher(tree.getBranch(1).getData().literal).matches());
	}
	
	@Test
	public void shouldFail() {
		String program = "test ";
		Parser parser = new Parser(program, grammar);
		try {
			parser.parse();
			fail("The parser should throw an RuleNotMatchedException.");
		} catch(RuleNotMatchedException e) {
			return;
		}
	}
	
	@Test
	public void ignoreRules() {
		grammar = new IGrammar() {
			private final TokenRule r = new TokenRule(tokenName, Pattern.compile(tokenRegex), TestToken.class);
			private final TokenRule r2 = new TokenRule(tokenName, Pattern.compile(" +"));
			
			@Override
			public Iterable<TokenRule> getTokenRules() {
				ArrayList<TokenRule> arr = new ArrayList<TokenRule>();
				arr.add(r);
				arr.add(r2);
				return arr;
			}
			
			class TestRule extends ShownProd {
				@Override
				public SyntaxTree parse(Scanner sc) {
					if(sc.peekToken() instanceof TestToken) {
						return new SyntaxTree(this, getTerminalTree(sc, TestToken.class));
					}
					return null;
				}

				@Override
				public Type[] typeCheckUp(Environment<Type> environment,
						Type... subtypes) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void typeCheckDown(Environment<Type> environment) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public Position getPosition() {
					// TODO Auto-generated method stub
					return null;
				}
			}
			
			@Override
			public Production GetStartSymbol() {
				return new TestRule();
			}
		};
		
		String program = "    test ";
		Parser parser = new Parser(program, grammar);
		Tree<Token, Production> tree = parser.parse();
		assertEquals(1, tree.numChildren());
		
		Pattern pattern = Pattern.compile(tokenRegex);
		assertTrue(pattern.matcher(tree.getBranch(1).getData().literal).matches());
		simpleGrammar();
	}
	
	@Test
	public void alternativeRules() {
		
		grammar = new IGrammar() {
			private final TokenRule r = new TokenRule(tokenName, Pattern.compile(tokenRegex), TestToken.class);
			private final TokenRule r2 = new TokenRule("LParen", Pattern.compile("\\("), LParen.class);
			private final TokenRule r3 = new TokenRule("RParen", Pattern.compile("\\)"), RParen.class);
			private final TokenRule r4 = new TokenRule(tokenName, Pattern.compile(" +"));
			
			@Override
			public Iterable<TokenRule> getTokenRules() {
				ArrayList<TokenRule> arr = new ArrayList<TokenRule>();
				arr.add(r);
				arr.add(r2);
				arr.add(r3);
				arr.add(r4);
				return arr;
			}
			
			@Override
			public Production GetStartSymbol() {
				return new Program();
			}
			
			class Program extends ShownProd {
				@Override
				public SyntaxTree parse(Scanner sc) {
					if(sc.peekToken() instanceof TestToken) {
						return new SyntaxTree(this, getTerminalTree(sc, TestToken.class));
					}
					return new SyntaxTree(this, new ParenList().parse(sc));
				}

				@Override
				public Type[] typeCheckUp(Environment<Type> environment,
						Type... subtypes) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void typeCheckDown(Environment<Type> environment) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public Position getPosition() {
					// TODO Auto-generated method stub
					return null;
				}
			}
			
			class ParenList extends ShownProd {
				@Override
				public SyntaxTree parse(Scanner sc) {
					return new SyntaxTree(this, 
							getTerminalTree(sc, LParen.class),
							new Paren().parse(sc),
							getTerminalTree(sc, RParen.class)
							);
				}

				@Override
				public Type[] typeCheckUp(Environment<Type> environment,
						Type... subtypes) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void typeCheckDown(Environment<Type> environment) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public Position getPosition() {
					// TODO Auto-generated method stub
					return null;
				}
			}
			
			class Paren extends ShownProd {
				@Override
				public SyntaxTree parse(Scanner sc) {
					if(sc.peekToken() instanceof TestToken) {
						return new SyntaxTree(this, getTerminalTree(sc, TestToken.class), new Paren().parse(sc));
					}
					return null;
				}

				@Override
				public Type[] typeCheckUp(Environment<Type> environment,
						Type... subtypes) {
					// TODO Auto-generated method stub
					return null;
				}

				@Override
				public void typeCheckDown(Environment<Type> environment) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public Position getPosition() {
					// TODO Auto-generated method stub
					return null;
				}
			}
		};
		
		String program = "(test test)";
		Parser parser = new Parser(program, grammar);
		
		// Now it is too cumbersome to work on a CST to bother.
		Tree<Token, Production> tree = AST.constructAST(parser.parse());
		
		assertEquals(1, tree.numChildren());
		assertEquals(1, tree.getBranch(1).numChildren());
		assertEquals(2, tree.getBranch(1).getBranch(1).numChildren());
		assertEquals(TestToken.class, tree.getBranch(1).getBranch(1).getBranch(1).getData().getClass());
		assertEquals(TestToken.class, tree.getBranch(1).getBranch(1).getBranch(2).getBranch(1).getData().getClass());
	}
}
