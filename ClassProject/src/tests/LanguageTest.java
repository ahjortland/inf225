package tests;

import language.Grammar;

import org.junit.Test;

import frontend.Parser;

public class LanguageTest {

	@Test
	public void simpleFunction() {
		String program = "void main() { return; }";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void negativeNumbers() {
		String program = "void main() { return -1; }";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void If() {
		String program ="int h(int s) {\n" +
				"if(s == 2)\n" +
				"y[0] = 2;\n" +
				"else\n" +
				"d = 1;\n" +
				"return f(y[0]+x);\n" +
				"}";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void varDecl() {
		String program = "int x;";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void varDeclArray() {
		String program = "int x[2];";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void varDeclAssign() {
		String program = "int x = 2;";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void arithmetic() {
		String program = ""
				+ "int x = 2+2;\n"
				+ "int y = 2-2;\n"
				+ "int z = 2*2;\n"
				+ "int a = 2%2;\n"
				+ "int b = 2/2;\n"
				;
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	

	@Test
	public void compare() {
		String program = ""
				+ "bool x = 2 >  2;\n"
				+ "bool y = 2 >= 2;\n"
				+ "bool z = 2 == 2;\n"
				+ "bool a = 2 != 2;\n"
				+ "bool b = 2 <= 2;\n"
				+ "bool c = 2 <  2;\n"
				;
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void funDecl() {
		String program = "int x(int s, int y) { return s+y; }";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void varDeclBool() {
		String program = "bool x = true; bool y = false;";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void varDeclStr() {
		String program = "str x = \"Hello, \";\n"
				+ "str y = \"world\";";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void keywords() {
		String program = 
				  "void main() {\n"
				+ "    if(false) {\n"
				+ "        while(true) {\n"
				+ "            int x = read();\n"
				+ "            if(x == 2) {\n"
				+ "                print();\n"
				+ "            } else {\n"
				+ "                x = x-2;\n"
				+ "                print();\n"
				+ "            }\n"
				+ "        }\n"
				+ "    }\n"
				+ "}";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
	
	@Test
	public void funCall() {
		String program = "void main() { print(f(2)); }\n"
				+ "int f(int x) { return x*x; }";
		Parser p = new Parser(program, new Grammar());
		p.parse();
	}
}
