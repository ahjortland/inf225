Here are instructions on building and running this compiler. It will also have
instructions on how to download and run a virtual machine which can be used to 
test the compiler (if you don't have a computer with an ARM cpu running).

BUILDING
  To build this project you can either open up eclipse, and import the project
  and then build it from eclipse, or you can use the provided build.xml ant 
  file. If you use ant, run the command (assuming you have ant and the 
  java sdk installed) from the command line in the folder of build.xml:
      ant jar
  This will create a jar file named "Compiler.jar" in the project root folder.

  If you use eclipse, you can either export it as a jar file, or just run it 
  with the play button in the toolbar. If you run it directly in the IDE, you
  must remember to set the command line arguments in your run configuration.

RUNNING
  To use the program simply run the program on an ARM machine with the input 
  file as the first command line argument, and the output file as the second 
  command line argument. That is if you are running the jar which ant builds
  you use the command (assuming you want to compile the program "prog1.cmin"
      java -jar Compiler.jar prog1.cmin prog1.out
  This will create the executable file prog1.out.

  If you are not on an ARM machine, or simply want to look at the assembly,
  you can specify the command line argument -s which will output assembly to
  the output file instead of an actually executable.

  If you want to know what other command line arguments exists and what they 
  do, just run the compiler with the argument "-h" and it will print the help
  text.

DOWNLOADING AND RUNNING THE VM
  If you find yourself in a position where you want to run this compiler, but
  you don't have an ARM machine to use it on, you can download a virtual 
  machine from the following link
    http://vedlegg.uib.no/?id=67bb11f03792fac53f15b58d326f2e6e
  Unfortunately this is a timed link which will be unavailable from 2014-12-05.
  This is an image of a Raspberry Pi running debian with the compiler 
  preinstalled and in the search path. Just run the command "cmin" and start
  compiling.

  Note that this requires Qemu compiled with ARM support, and it also requires
  a fairly modern CPU (or hardware virtualization) to be able to run acceptably

HELP TEXT
  Usage:
  java Main [-htrv] [-a file] [-c file] input output
  java Main -n [-htrv] [-a file] [-c file] input
  
    -h, --help          Print this help text and exit.
    -v, --verbose       Be verbose. Will print timings and other debug stuff 
                        on stderr.
    -a, --ast           Print the ast to a file. Will print to stdout if a 
                        dash ('-') is given
    -c, --cst           Print the cst to a file. Will print to stdout if a 
                        dash ('-') is given
    -t, --no-typecheck  Disable typechecking
    -n, --no-assembly   Disable code generation. This will disable linking.
    -s, --only-assembly Disable linking
    -r, --no-rewrite    Ignore rewrite rules (not implemented yet)

  input                 This is the input source file which should be compiled.
                        Will read from stdin if input is a dash ('-')

  output                This is the path where the compiler will output the 
                        complete program (or the assembly if -s is set). Will 
                        print to stdout if output is a dash ('-')



