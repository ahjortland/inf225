#!/usr/local/bin/tm
00: LDC     6, 10, 0    # char value to stop reading 10 is newline
01: LDC     4,  1, 0    # current index of char array
02: LDC     1,  1, 0    # increment

03: LDA     2,     0(4) # set r[2] = r[4] (start index of char array)

# "Hello, "
04: LDC     0, 72, 0    # load character H into register 0
05: ST      0,     0(4) # store character into memory
06: ADD     4,  4, 1    # move memory pointer
07: LDC     0,101, 0    # repeat for the rest of the characters in the string "Hello, "
08: ST      0,     0(4)
09: ADD     4,  4, 1
10: LDC     0,108, 0  
11: ST      0,     0(4)
12: ADD     4,  4, 1
13: LDC     0,108, 0  
14: ST      0,     0(4)
15: ADD     4,  4, 1
16: LDC     0,111, 0  
17: ST      0,     0(4)
18: ADD     4,  4, 1
19: LDC     0, 44, 0  
20: ST      0,     0(4)
21: ADD     4,  4, 1
22: LDC     0, 32, 0  
23: ST      0,     0(4)
24: ADD     4,  4, 1

# loop
25: INCHAR  0,  0, 0    # read character into r[0]
26: ST      0,     0(4) # store character in memory at location r[4]
27: ADD     4,  1, 4    # increment memory location
28: SUB     5,  0, 6    # subtract stop value from character for loop invariant
29: JGT     5,    -5(7) # until input == char value to stop reading
# loop done

30: SUB     4,  4, 1    # Subtract increment from register 4
31: LD      3,     0(4) # Load last character into r[3]
32: LDC     0, 33, 0    # Character '!'
33: ST      0,     0(4) # Store '!' in memory
34: ADD     4,  4, 1    # Add increment to register 4
35: ST      3,     0(4) # Store last character in memory
36: ADD     4,  4, 1    # Add increment to register 4

 # loop
37: LD      0,     0(2) # load character from memory
38: OUTCHAR 0,  0, 0    # write character
39: ADD     2,  1, 2    # add increment to index
40: SUB     3,  4, 2    # subtract index against length of list
41: JGT     3,    -5(7) # until we have read the input
# loop done

42: HALT    0,  0, 0    # halt the program
